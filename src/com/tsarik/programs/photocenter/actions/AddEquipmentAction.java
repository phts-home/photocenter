package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddEquipmentDialog;


public class AddEquipmentAction extends MainFrameMenuAction {
	
	public AddEquipmentAction(MainFrame owner) {
		super(owner, "Equipment...");
		putValue(SHORT_DESCRIPTION, "Add Equipment...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_E);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddEquipmentDialog d = new AddEquipmentDialog(owner);
		d.setVisible(true);
	}
}
