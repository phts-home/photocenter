package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowDemandedEquipmentDialog;


public class ShowDemandedEquipmentDialogAction extends MainFrameMenuAction {
	
	public ShowDemandedEquipmentDialogAction(MainFrame owner) {
		super(owner, "Demanded Photo Equipment...");
		putValue(SHORT_DESCRIPTION, "Show Demanded Photo Equipment");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_E);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowDemandedEquipmentDialog d = new ShowDemandedEquipmentDialog(owner);
		d.setVisible(true);
	}
}
