package com.tsarik.programs.photocenter.actions;


import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;

public class ExitAction extends MainFrameMenuAction {
	
	public ExitAction(MainFrame owner) {
		super(owner, "Exit");
		putValue(SHORT_DESCRIPTION, "Exit");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_X);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		
		try {
			owner.exit();
		}
		catch (NullPointerException e) {
		}
	}
}
