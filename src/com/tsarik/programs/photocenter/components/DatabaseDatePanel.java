package com.tsarik.programs.photocenter.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

public class DatabaseDatePanel extends AbstractDatabaseValuesPanel {
	
	public DatabaseDatePanel() {
		super();
		spDate1 = new JSpinner(new SpinnerDateModel());
		spDate2 = new JSpinner(new SpinnerDateModel());
		cbDateSign = new JComboBox();
		
		JButton butNow = new JButton("Now");
		butNow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setNow(1);
			}
		});
		butNow2 = new JButton("Now");
		butNow2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setNow(2);
			}
		});
		cbDateSign.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				spDate2.setEnabled(selectedIndex >= 7);
				butNow2.setEnabled(selectedIndex >= 7);
			}
		});
		cbDateSign.addItem("");
		cbDateSign.addItem("Equal (=)");
		cbDateSign.addItem("Not Equal (<>)");
		cbDateSign.addItem("Less (<)");
		cbDateSign.addItem("Equal or Less (<=)");
		cbDateSign.addItem("Greater (>)");
		cbDateSign.addItem("Equal or Greater (=>)");
		cbDateSign.addItem("Between");
		cbDateSign.addItem("Not Between");
		setNow(1);
		setNow(2);
		
		addComponentToGrid(cbDateSign, 0, 0, 2, 1);
		addComponentToGrid(spDate1, 0, 1, 1, 1);
		addComponentToGrid(spDate2, 1, 1, 1, 1);
		addComponentToGrid(butNow, 0, 2, 1, 1);
		addComponentToGrid(butNow2, 1, 2, 1, 1);
	}
	
	public int getSign() {
		return cbDateSign.getSelectedIndex();
	}
	
	public Date getValue1() {
		return ((SpinnerDateModel)spDate1.getModel()).getDate();
	}
	
	public Date getValue2() {
		return ((SpinnerDateModel)spDate2.getModel()).getDate();
	}
	
	private JSpinner spDate1;
	private JSpinner spDate2;
	private JComboBox cbDateSign;
	private JButton butNow2;
	
	private void setNow(int mode) {
		switch (mode) {
		case 1:
			spDate1.setValue(new Date());
			break;
		case 2:
			spDate2.setValue(new Date());
			break;
		}
	}
	
}
