package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddOrderDialog extends AbstractAddDialog {
	
	public AddOrderDialog(MainFrame owner) {
		super(owner, "Add Order", 450, 400);
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbBranches.removeAllItems();
		cbKiosks.removeAllItems();
		cbDevelopmentTypes.removeAllItems();
		cbDevelopmentTypes.addItem(new ComboBoxDatabaseItem());
		cbPrintingTypes.removeAllItems();
		cbPrintingTypes.addItem(new ComboBoxDatabaseItem());
		cbFormatTypes.removeAllItems();
		cbFormatTypes.addItem(new ComboBoxDatabaseItem());
		cbPaperTypes.removeAllItems();
		cbPaperTypes.addItem(new ComboBoxDatabaseItem());
		cbFrames.removeAllItems();
		cbFrames.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT kiosks.kiosk_id,kiosks.address,branches.name,branches.address FROM kiosks,branches WHERE kiosks.parent_branch_id = branches.branch_id ORDER BY kiosks.kiosk_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbKiosks.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + " - " + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT development_type_id,name,cost FROM development_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbDevelopmentTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT printing_type_id,name,cost_mult FROM printing_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbPrintingTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT format_type_id,name,cost FROM format_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbFormatTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT paper_type_id,name FROM paper_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbPaperTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
			table = dbManager.queryToTable("SELECT frames_id,count_1,count_2,count_3,count_4,count_5,count_6,count_7,count_8,count_9,count_10 FROM frames ORDER BY frames_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbFrames.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,0)+" ("+table.getItem(i,1)+", "+table.getItem(i,2)+", "+table.getItem(i,3)+", "+table.getItem(i,4)+", "+table.getItem(i,5)+", "+table.getItem(i,6)+", "+table.getItem(i,7)+", "+table.getItem(i,8)+", "+table.getItem(i,9)+", "+table.getItem(i,10)+", ...)"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addOrder(((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(), 
					branchEnable.isSelected(), 
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbKiosks.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbDevelopmentTypes.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbPrintingTypes.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbFrames.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbFormatTypes.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbPaperTypes.getSelectedItem()).getId(), 
					cbIsPressing.isSelected(),
					((SpinnerDateModel)spOrderDate.getModel()).getDate(),
					cpDone.isSelected(),
					cpPaid.isSelected()
					);
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		cbBranches = new JComboBox();
		cbKiosks = new JComboBox();
		cbDevelopmentTypes = new JComboBox();
		cbPrintingTypes = new JComboBox();
		cbFormatTypes = new JComboBox();
		cbPaperTypes = new JComboBox();
		cbFrames = new JComboBox();
		cbIsPressing = new JCheckBox("Is Pressing");
		spOrderDate = new JSpinner(new SpinnerDateModel());
		cpDone = new JCheckBox("Done");
		cpPaid = new JCheckBox("Paid");
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spOrderDate.setValue(new Date());
			}
		});
		JButton btAddClient = new JButton("Add");
		btAddClient.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbClients.getItemCount();
				int s = cbClients.getSelectedIndex();
				AddClientDialog d = new AddClientDialog(AddOrderDialog.this);
				d.setVisible(true);
				refresh();
				if (cbClients.getItemCount() != c) {
					cbClients.setSelectedIndex(cbClients.getItemCount()-1);
				} else {
					if (s < cbClients.getItemCount()) {
						cbClients.setSelectedIndex(s);
					}
				}
			}
		});
		JButton btAddFrames = new JButton("Add");
		btAddFrames.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbFrames.getItemCount();
				int s = cbFrames.getSelectedIndex();
				AddFramesDialog d = new AddFramesDialog(AddOrderDialog.this);
				d.setVisible(true);
				refresh();
				if (cbFrames.getItemCount() != c) {
					cbFrames.setSelectedIndex(cbFrames.getItemCount()-1);
				} else {
					if (s < cbFrames.getItemCount()) {
						cbFrames.setSelectedIndex(s);
					}
				}
			}
		});
		
		branchEnable = new JRadioButton("Branch");
		branchEnable.setSelected(true);
		kioskEnable = new JRadioButton("Kiosk");
		ButtonGroup gr = new ButtonGroup();
		gr.add(branchEnable);
		gr.add(kioskEnable);

		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(branchEnable, 0, 1, 1, 1, 1, panel);
		addComponentToGrid(kioskEnable, 0, 2, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Development Type"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Printing Type"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Format"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Paper Type"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Frames"), 0, 7, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Order Date"), 0, 9, 1, 1, 1, panel);
		
		addComponentToGrid(cbClients, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(cbBranches, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(cbKiosks, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(cbDevelopmentTypes, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(cbPrintingTypes, 1, 4, 1, 1, 2, panel);
		addComponentToGrid(cbFormatTypes, 1, 5, 1, 1, 2, panel);
		addComponentToGrid(cbPaperTypes, 1, 6, 1, 1, 2, panel);
		addComponentToGrid(cbFrames, 1, 7, 1, 1, 2, panel);
		addComponentToGrid(cbIsPressing, 1, 8, 1, 1, 2, panel);
		addComponentToGrid(spOrderDate, 1, 9, 1, 1, 2, panel);
		addComponentToGrid(cpDone, 1, 10, 1, 1, 2, panel);
		addComponentToGrid(cpPaid, 1, 11, 1, 1, 2, panel);
		
		addComponentToGrid(btAddClient, 2, 0, 1, 1, 3, panel);
		addComponentToGrid(btAddFrames, 2, 7, 1, 1, 3, panel);
		addComponentToGrid(btNow, 2, 9, 1, 1, 3, panel);
		return panel;
	}
	
	private JComboBox cbClients;
	private JComboBox cbBranches;
	private JComboBox cbKiosks;
	private JComboBox cbDevelopmentTypes;
	private JComboBox cbPrintingTypes;
	private JComboBox cbFormatTypes;
	private JComboBox cbPaperTypes;
	private JComboBox cbFrames;
	private JCheckBox cbIsPressing;
	private JSpinner spOrderDate;
	private JCheckBox cpDone;
	private JCheckBox cpPaid;
	
	JRadioButton kioskEnable;
	JRadioButton branchEnable;
	
}
