package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddEquipmentTypeDialog extends AbstractAddDialog {
	
	public AddEquipmentTypeDialog(MainFrame owner) {
		super(owner, "Add Equipment Type", 400, 150);
	}
	
	public AddEquipmentTypeDialog(AbstractDialog owner) {
		super(owner, "Add Equipment Type", 400, 150);
	}
	
	@Override
	protected void refresh() {
		// Nothing
	}
	
	@Override
	protected int apply() {
		if (tfName.getText().equals("")) {
			JOptionPane.showMessageDialog(owner, Parameters.ERROR_INPUT, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
			return APPLY_CHECK_FAIL;
		}
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addEquipmentType(tfName.getText(), cbForSale.isSelected()); 
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		tfName = new JTextField();
		cbForSale = new JCheckBox("For Sale");
		cbForSale.setSelected(true);
		
		// put components on the panel
		addComponentToGrid(new JLabel("Name"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfName, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(cbForSale, 1, 1, 1, 1, 4, panel);
		
		return panel;
	}
	
	private JTextField tfName;
	private JCheckBox cbForSale;
	
}
