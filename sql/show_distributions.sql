SELECT 
	branches.name AS "Branch Name", 
	branches.address AS "Branch Address", 
	equipment.name AS "Equipment Name", 
	equipment_types.name AS "Equipment Type", 
	distributions.quantity AS "Quantity", 
	distributions.distribution_date AS "Distribution Date" 

FROM branches, equipment, distributions, equipment_types 

WHERE distributions.branch_id = branches.branch_id 
	AND distributions.equipment_id = equipment.equipment_id 
	AND equipment.equipment_type_id = equipment_types.equipment_type_id 

ORDER BY `Distribution Date` 