package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import com.tsarik.dialogs.event.AbstractDialogEvent;
import com.tsarik.dialogs.event.AbstractDialogListener;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;



public class PreferencesDialog extends AbstractPhotocenterProgramDialog {
	
	public PreferencesDialog(MainFrame owner) {
		super(owner, "Preferences", 400, 300);
		addAbstractDialogListener(new AbstractDialogListener(){
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON)|| (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
					apply();
				}
			}
		});
	}

	protected int apply() {
		Parameters.DATABASE_SERVER = tfDbServer.getText();
		Parameters.DATABASE_NAME = tfDbDatabase.getText();
		Parameters.DATABASE_USER = tfDbUserName.getText();
		Parameters.DATABASE_PASSWORD = tfDbPassword.getText();
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected void refresh() {
		tfDbServer.setText(Parameters.DATABASE_SERVER);
		tfDbDatabase.setText(Parameters.DATABASE_NAME);
		tfDbUserName.setText(Parameters.DATABASE_USER);
		tfDbPassword.setText(Parameters.DATABASE_PASSWORD);
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		tabbedPane = new JTabbedPane();
		tabbedPane.add("Connection", createDataBaseTab());
		
		addComponentToGrid(tabbedPane, 0, 0, 1, 1, 0, panel);
		return panel;
	}
	
	private JTabbedPane tabbedPane;
	
	private JTextField tfDbServer;
	private JTextField tfDbDatabase;
	private JTextField tfDbUserName;
	private JTextField tfDbPassword;
	
	private Component createDataBaseTab() {
		JPanel panel = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);
		
		tfDbServer = new JTextField();
		tfDbDatabase = new JTextField();
		tfDbUserName = new JTextField();
		tfDbPassword = new JTextField();
		
		addComponentToGrid(new JLabel("Server:"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfDbServer, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Database:"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfDbDatabase, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Username:"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(tfDbUserName, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Password:"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(tfDbPassword, 1, 3, 1, 1, 4, panel);
		return panel;
	}
	
}
