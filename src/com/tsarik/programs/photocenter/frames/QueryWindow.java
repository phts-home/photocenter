package com.tsarik.programs.photocenter.frames;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.AdvancedTable;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class QueryWindow extends JFrame {
	
	/**
	 * Default window width.
	 */
	public final static int DEFAULT_WIDTH = 400;
	
	/**
	 * Default window height.
	 */
	public final static int DEFAULT_HEIGHT = 250;
	
	public QueryWindow(MainFrame owner) {
		super();
		this.owner = owner;
		setTitle("Select Query Window");
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setLocation(200, 200);
		createComponents();
	}
	
	public String getRequestText() {
		String text = textRequest.getText();
		return text;
	}
	
	public void setResponseText(String text) {
		textResponse.setText(text);
	}
	
	/**
	 * Creates all components on the frame.
	 */
	private void createComponents() {
		textRequest = new JTextArea("SELECT * FROM clients");
		
		Component bottomComponent = null;
		advancedTable = new AdvancedTable();
		JScrollPane scrollPane2 = new JScrollPane(advancedTable);
		
		tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout());
		tablePanel.add(advancedTable.getTableHeader(), BorderLayout.PAGE_START);
		tablePanel.add(scrollPane2, BorderLayout.CENTER);
		bottomComponent = tablePanel;
		
		// create splitPane
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, textRequest, bottomComponent);
		splitPane.setContinuousLayout(true); 
		splitPane.setOneTouchExpandable(false);
		splitPane.setResizeWeight(0.5);
		splitPane.setFocusTraversalKeysEnabled(false);
		
		// create buttons
		JButton butGO = new JButton("Go");
		butGO.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				try {
					PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
					String queryText = getRequestText();
					ResponseTable table = dbManager.queryToTable(queryText);
					advancedTable.setData(table.getData());
					advancedTable.setColumns(table.getColumnLabels());
					advancedTable.refresh();
					dbManager = null;
				}
				catch (SQLException e) {
					Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
				}
				catch (ClassNotFoundException e) {
					Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
				}
			}
		});
		JButton butClose = new JButton("Close");
		butClose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				QueryWindow.this.dispose();
			}
		});
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(butGO);
		buttonsPanel.add(butClose);
		
		// place components on frame
		Container contentPane = getContentPane();
		contentPane.add(splitPane, BorderLayout.CENTER);
		contentPane.add(buttonsPanel, BorderLayout.SOUTH);
	}
	
	private MainFrame owner;
	private JTextArea textRequest;
	private JTextArea textResponse;
	private AdvancedTable advancedTable;
	private JPanel tablePanel;
	private JSplitPane splitPane;
}
