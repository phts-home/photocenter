package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.SQLException;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class ShowBranchesDialog extends AbstractShowDialog {
	
	public ShowBranchesDialog(MainFrame owner) {
		super(owner, "Show Branches/Kiosks", 400, 300);
		tfTabTitle.setText("Branches/Kiosks");
	}
	
	@Override
	protected void refresh() {
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.showBranches(cbType.getSelectedIndex(),
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					(cbBranchNameEnabled.isSelected())?tfBranchName.getText():"",
					(cbBranchAddrEnabled.isSelected())?tfBranchAddr.getText():""
					);
			if (table != null) {
				owner.addTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbBranches = new JComboBox();
		cbBranches.setPreferredSize(new Dimension(200,cbBranches.getPreferredSize().height));
		tfBranchName = new JTextField();
		tfBranchAddr = new JTextField();
		cbType = new JComboBox(new String[]{"Branches","Kiosks","Both"});
		
		cbBranchNameEnabled = new JCheckBox("Branch Name");
		cbBranchAddrEnabled = new JCheckBox("Branch Address");

		constraints.weightx = 1;
		constraints.insets = new Insets(1,1,1,1);
		JPanel branchPanel = new JPanel();
		branchPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbBranchNameEnabled, 0, 0, 1, 1, branchPanel);
		addComponentToGrid(tfBranchName, 1, 0, 1, 1, branchPanel);
		addComponentToGrid(cbBranchAddrEnabled, 0, 1, 1, 1, branchPanel);
		addComponentToGrid(tfBranchAddr, 1, 1, 1, 1, branchPanel);
		
		// put components on the panel
		addComponentToGrid(new JLabel("Show"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbType, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Branch"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(branchPanel, 1, 2, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbBranches;
	private JTextField tfBranchName;
	private JTextField tfBranchAddr;
	private JCheckBox cbBranchNameEnabled;
	private JCheckBox cbBranchAddrEnabled;
	private JComboBox cbType;
	
}
