package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.CalculateOrdersReceiptsDialog;

public class CalculateOrdersReceiptsAction extends MainFrameMenuAction {
	
	public CalculateOrdersReceiptsAction(MainFrame owner) {
		super(owner, "Orders Receipts...");
		putValue(SHORT_DESCRIPTION, "Calculate Orders Receipts...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		CalculateOrdersReceiptsDialog d = new CalculateOrdersReceiptsDialog(owner);
		d.setVisible(true);
	}
}
