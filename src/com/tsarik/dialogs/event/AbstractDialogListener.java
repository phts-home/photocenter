package com.tsarik.dialogs.event;

import java.util.EventListener;


/**
 * Listener for an abstract dialog which provides to do some actions when
 * "Ok", "Cancel" or "Apply" button is pressed.
 * 
 * @author Phil Tsarik
 *
 */
public interface AbstractDialogListener extends EventListener {
	
	/**
	 * Invokes when some dialog button has been pressed.
	 * 
	 * @param evt Dialog event
	 */
	public void buttonPressed(AbstractDialogEvent evt);
	
}
