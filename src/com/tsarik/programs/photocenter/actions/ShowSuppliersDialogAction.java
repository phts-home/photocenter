package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowSuppliersDialog;


public class ShowSuppliersDialogAction extends MainFrameMenuAction {
	
	public ShowSuppliersDialogAction(MainFrame owner) {
		super(owner, "Suppliers...");
		putValue(SHORT_DESCRIPTION, "Show Suppliers");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowSuppliersDialog d = new ShowSuppliersDialog(owner);
		d.setVisible(true);
	}
}
