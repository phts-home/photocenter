SELECT 
	CONCAT(suppliers.name, " (", suppliers.address, ")") AS "Supplier", 
	CONCAT(equipment.name, " (", equipment_types.name, ")") AS "Equipment", 
	deliverable_equipment.cost AS "Quotation", 
	supplies.quantity AS "Quantity", 
	supplies.quantity*deliverable_equipment.cost	AS "Supply Price", 
	supplies.supply_date AS "Supply Date" 
	
FROM equipment, equipment_types, supplies, suppliers, deliverable_equipment 

WHERE deliverable_equipment.equipment_id = equipment.equipment_id 
	AND deliverable_equipment.supplier_id = suppliers.supplier_id 
	AND supplies.deliverable_equipment_id = deliverable_equipment.deliverable_equipment_id 
	AND equipment.equipment_type_id = equipment_types.equipment_type_id 

ORDER BY `Supply Date`, `Equipment` 
