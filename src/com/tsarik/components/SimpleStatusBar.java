package com.tsarik.components;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;


/**
 * Simple status bar with panels as labels.
 * 
 * @see SimpleStatusBarPanel
 * 
 * @author Phil Tsarik
 * @version 1.0.2
 *
 */
public class SimpleStatusBar extends JPanel {
	
	public final static int ALIGNMENT_LEFT = SwingConstants.LEFT;
	public final static int ALIGNMENT_RIGHT = SwingConstants.RIGHT;
	public final static int ALIGNMENT_CENTER = SwingConstants.CENTER;
	
	public final static int BORDER_RAISED = EtchedBorder.RAISED;
	public final static int BORDER_LOWERED = EtchedBorder.LOWERED;
	public final static int BORDER_NONE = -1;
	
	/**
	 * Constructs a new instance of the <code>SimpleStatusBar</code> class.
	 * 
	 * @param height Status bar height
	 */
	public SimpleStatusBar(int height) {
		super();
		init(height, BORDER_LOWERED);
	}
	
	/**
	 * Constructs a new <code>SimpleStatusBar</code> instance.
	 * 
	 * @param height Status bar height
	 * @param border Border type
	 */
	public SimpleStatusBar(int height, int border) {
		super();
		init(height, border);
	}
	
	/**
	 * Adds a panel to the bar.
	 * 
	 * @param text Panel text
	 */
	public void addPanel(String text) {
		addPanel(text, 0, 100, ALIGNMENT_LEFT, BORDER_LOWERED);
	}
	
	/**
	 * Adds a panel to the bar.
	 * 
	 * @param text Panel text
	 * @param alignment Text alignment
	 * @param border Panel border
	 */
	public void addPanel(String text, int alignment, int border) {
		addPanel(text, 0, 100, alignment, border);
	}
	
	/**
	 * Adds a panel to the bar.
	 * 
	 * @param text Panel text
	 * @param weightx Share in the bar (in percentage)
	 * @param width Panel width (only if weightx == 0)
	 * @param alignment Text alignment
	 * @param border Panel border
	 */
	public void addPanel(String text, int weightx, int width, int alignment, int border) {
		SimpleStatusBarPanel panel = new SimpleStatusBarPanel(text, width, height, alignment, border);
		
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = weightx;
		constraints.gridy = 0;
		layout.setConstraints(panel, constraints);
		
		super.add(panel);
		panels.add(panel);
	}
	
	/**
	 * Gets panel with the specified index.
	 * 
	 * @param index Panel index.
	 * @return A <code>SimpleStatusBarPanel</code>'s instance
	 */
	public SimpleStatusBarPanel getPanel(int index) {
		return panels.get(index);
	}
	
	/**
	 * Gets panels height.
	 * 
	 * @return Panels height
	 */
	public int getPanelsHeight() {
		return height;
	}
	
	/**
	 * Sets panels height.
	 * 
	 * @param height Panels height.
	 */
	public void setPanelsHeight(int height) {
		this.height = height;
		for (int i = 0; i < panels.size(); i++) {
			panels.get(i).setPanelHeight(height);
		}
	}
	
	
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	private ArrayList <SimpleStatusBarPanel> panels;
	private int height;
	
	
	/**
	 * Initializes the instance.
	 * 
	 * @param height Bar height
	 * @param border Bar border
	 */
	private void init(int height, int border) {
		layout = new GridBagLayout();
		setLayout(layout);
		
		constraints = new GridBagConstraints();
		panels = new ArrayList<SimpleStatusBarPanel>();
		this.height = height;
		setPanelsHeight(height);
		if (border != BORDER_NONE) {
			setBorder(BorderFactory.createEtchedBorder(border));
		}
	}
	
}

