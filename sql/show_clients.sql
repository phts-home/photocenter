-- branch AND discount AND photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name" , 
	clients.address AS "Client's Address" , 
	CONCAT(branches.name," (", branches.address, ")") AS "Branch", 
	(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) AS "Number Of Photos", 
	discounts.rate AS "Discount Rate", 
	discounts.exp_date AS "Discount Expiration Date"  

FROM orders, clients, branches, discounts, frames 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id = frames.frames_id 
	AND orders.done = 0 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	
	AND branches.name LIKE "%"


UNION 



-- branch AND no discount AND photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address, 
	CONCAT(branches.name," (", branches.address, ")"), 
	(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36), 
	"-", 
	"-"  

FROM orders, clients, branches, discounts, frames 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id = frames.frames_id 
	AND orders.done = 0 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	
	AND branches.name LIKE "%"



UNION 



-- kiosk AND discount AND photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address, 
	CONCAT(branches.name," (", branches.address, ")"), 
	(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36), 
	discounts.rate, 
	discounts.exp_date  

FROM orders, clients, branches, kiosks, discounts, frames 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id = frames.frames_id 
	AND orders.done = 0 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	
	AND branches.name LIKE "%"



UNION 



-- kiosk AND no discount AND photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address, 
	CONCAT(branches.name," (", branches.address, ")"), 
	(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36), 
	"-", 
	"-"  

FROM orders, clients, branches, kiosks, discounts, frames 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id = frames.frames_id 
	AND orders.done = 0 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	
	AND branches.name LIKE "%"



UNION


-- branch AND discount AND no photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address, 
	CONCAT(branches.name," (", branches.address, ")"), 
	0, 
	discounts.rate, 
	discounts.exp_date  

FROM orders, clients, branches, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id IS NULL 
	AND orders.done = 0 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	
	AND branches.name LIKE "%"



UNION 



-- branch AND no discount AND no photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address AS "Client's Address" , 
	CONCAT(branches.name," (", branches.address, ")"), 
	0, 
	"-", 
	"-"  

FROM orders, clients, branches, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id IS NULL 
	AND orders.done = 0 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	
	AND branches.name LIKE "%"



UNION 



-- kiosk AND discount AND no photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address, 
	CONCAT(branches.name," (", branches.address, ")"), 
	0, 
	discounts.rate, 
	discounts.exp_date  

FROM orders, clients, branches, kiosks, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id IS NULL 
	AND orders.done = 0 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	
	AND branches.name LIKE "%"



UNION 



-- kiosk AND no discount AND no photos
SELECT 
	CONCAT(clients.firstname, " ", clients.lastname), 
	clients.address, 
	CONCAT(branches.name," (", branches.address, ")"), 
	0, 
	"-", 
	"-"  

FROM orders, clients, branches, kiosks, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id IS NULL 
	AND orders.done = 0 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	
	AND branches.name LIKE "%"

