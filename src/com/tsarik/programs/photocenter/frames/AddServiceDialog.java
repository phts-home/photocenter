package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;


public class AddServiceDialog extends AbstractAddDialog {
	
	public AddServiceDialog(MainFrame owner) {
		super(owner, "Add Service", 450, 450);
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbBranches.removeAllItems();
		cbServiceTypes.removeAllItems();
		cbEquipment.removeAllItems();
		cbEquipment.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT service_type_id,name,cost FROM service_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbServiceTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT equipment.equipment_id,equipment.name FROM equipment,equipment_types WHERE equipment.equipment_type_id=equipment_types.equipment_type_id AND equipment_types.for_sale=1 ORDER BY equipment.equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addService(((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbServiceTypes.getSelectedItem()).getId(), 
					((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getId(), 
					((SpinnerDateModel)spServiceDate.getModel()).getDate(), 
					((SpinnerDateModel)spExpDate.getModel()).getDate(), 
					cpDone.isSelected(), 
					cpPaid.isSelected());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		cbBranches = new JComboBox();
		cbServiceTypes = new JComboBox();
		cbEquipment = new JComboBox();
		spServiceDate = new JSpinner(new SpinnerDateModel());
		spExpDate = new JSpinner(new SpinnerDateModel());
		cpDone = new JCheckBox("Done");
		cpPaid = new JCheckBox("Paid");
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spServiceDate.setValue(new Date());
			}
		});
		JButton btNow2 = new JButton("Now");
		btNow2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spExpDate.setValue(new Date());
			}
		});
		JButton btAddClient = new JButton("Add");
		btAddClient.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbClients.getItemCount();
				int s = cbClients.getSelectedIndex();
				AddClientDialog addClientWindow = new AddClientDialog(AddServiceDialog.this);
				addClientWindow.setVisible(true);
				refresh();
				if (cbClients.getItemCount() != c) {
					cbClients.setSelectedIndex(cbClients.getItemCount()-1);
				} else {
					if (s < cbClients.getItemCount()) {
						cbClients.setSelectedIndex(s);
					}
				}
			}
		});

		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Branch"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Service Type"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Equipment"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Booking Date"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(new JLabel("Expiration Date"), 0, 5, 1, 1, 1, panel);
		
		addComponentToGrid(cbClients, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(cbBranches, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(cbServiceTypes, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(cbEquipment, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(spServiceDate, 1, 4, 1, 1, 2, panel);
		addComponentToGrid(spExpDate, 1, 5, 1, 1, 2, panel);
		addComponentToGrid(cpDone, 1, 6, 1, 1, 2, panel);
		addComponentToGrid(cpPaid, 1, 7, 1, 1, 2, panel);
		
		addComponentToGrid(btAddClient, 2, 0, 1, 1, 3, panel);
		addComponentToGrid(btNow, 2, 4, 1, 1, 3, panel);
		addComponentToGrid(btNow2, 2, 5, 1, 1, 3, panel);
		
		return panel;
	}
	
	private JComboBox cbClients;
	private JComboBox cbBranches;
	private JComboBox cbServiceTypes;
	private JComboBox cbEquipment;
	private JSpinner spServiceDate;
	private JSpinner spExpDate;
	private JCheckBox cpDone;
	private JCheckBox cpPaid;
	
}
