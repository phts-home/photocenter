package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowSalesDialog;


public class ShowSalesDialogAction extends MainFrameMenuAction {
	
	public ShowSalesDialogAction(MainFrame owner) {
		super(owner, "Sales...");
		putValue(SHORT_DESCRIPTION, "Show Sales");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowSalesDialog d = new ShowSalesDialog(owner);
		d.setVisible(true);
	}
}
