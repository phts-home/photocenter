package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddSaleDialog;


public class AddSaleAction extends MainFrameMenuAction {
	
	public AddSaleAction(MainFrame owner) {
		super(owner, "Sale...");
		putValue(SHORT_DESCRIPTION, "Add Sale...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddSaleDialog d = new AddSaleDialog(owner);
		d.setVisible(true);
	}
}
