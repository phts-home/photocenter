package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddDistributionDialog;


public class AddDistributionAction extends MainFrameMenuAction {
	
	public AddDistributionAction(MainFrame owner) {
		super(owner, "Distribution...");
		putValue(SHORT_DESCRIPTION, "Add Distribution...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_D);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddDistributionDialog d = new AddDistributionDialog(owner);
		d.setVisible(true);
	}
}
