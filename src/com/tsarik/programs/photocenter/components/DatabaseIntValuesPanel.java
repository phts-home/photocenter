package com.tsarik.programs.photocenter.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class DatabaseIntValuesPanel extends AbstractDatabaseValuesPanel {
	
	public DatabaseIntValuesPanel() {
		super();
		spValue1 = new JSpinner(new SpinnerNumberModel(1,0,Integer.MAX_VALUE,1));
		spValue2 = new JSpinner(new SpinnerNumberModel(10,0,Integer.MAX_VALUE,1));
		cbValuesSign = new JComboBox();
		
		cbValuesSign.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				spValue2.setEnabled(selectedIndex >= 7);
			}
		});
		cbValuesSign.addItem("");
		cbValuesSign.addItem("Equal (=)");
		cbValuesSign.addItem("Not Equal (<>)");
		cbValuesSign.addItem("Less (<)");
		cbValuesSign.addItem("Equal or Less (<=)");
		cbValuesSign.addItem("Greater (>)");
		cbValuesSign.addItem("Equal or Greater (=>)");
		cbValuesSign.addItem("Between");
		cbValuesSign.addItem("Not Between");
		
		addComponentToGrid(cbValuesSign, 0, 0, 2, 1);
		addComponentToGrid(spValue1, 0, 1, 1, 1);
		addComponentToGrid(spValue2, 1, 1, 1, 1);
	}
	
	public int getSign() {
		return cbValuesSign.getSelectedIndex();
	}
	
	public Number getValue1() {
		return ((SpinnerNumberModel)spValue1.getModel()).getNumber().intValue();
	}
	
	public Number getValue2() {
		return ((SpinnerNumberModel)spValue2.getModel()).getNumber().intValue();
	}
	
	private JSpinner spValue1;
	private JSpinner spValue2;
	private JComboBox cbValuesSign;
	
}
