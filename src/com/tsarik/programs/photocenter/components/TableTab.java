package com.tsarik.programs.photocenter.components;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;
import com.tsarik.programs.photocenter.frames.AbstractEditDialog;
import com.tsarik.programs.photocenter.frames.EditClientDialog;
import com.tsarik.programs.photocenter.frames.EditOrderDialog;
import com.tsarik.programs.photocenter.frames.EditServiceDialog;

public class TableTab extends AbstractTableTab {
	
	public TableTab(MainFrame owner, String title, ResponseTable table, boolean editButton, boolean deleteButton, boolean refreshButton) {
		super(owner, title, table, editButton, deleteButton, refreshButton);
		type = 0;
	}
	
	public TableTab(MainFrame owner, String title, ResponseTable table) {
		super(owner, title, table);
		type = 0;
	}
	
	public TableTab(MainFrame owner, String title, ResponseTable table, int type) {
		super(owner, title, table);
		this.type = type;
	}
	
	protected void edit() {
		if (table.getSelectedRow() == -1) {
			return;
		}
		AbstractEditDialog d;
		int id = ((Integer)table.getModel().getValueAt(table.getSelectedRow(), 0)).intValue();
		switch (type) {
		case 1:
			d = new EditOrderDialog(owner, id);
			d.setVisible(true);
			break;
		case 2:
			d = new EditServiceDialog(owner, id);
			d.setVisible(true);
			break;
		case 3:
			d = new EditClientDialog(owner, id);
			d.setVisible(true);
			break;
		}
		refresh();
	}
	
	protected void delete() {
		if (table.getSelectedRow() == -1) {
			return;
		}
		if (JOptionPane.showConfirmDialog(TableTab.this, Parameters.QUESTION_DELETE, Parameters.PROGRAM_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
			return;
		}
		int id = ((Integer)table.getModel().getValueAt(table.getSelectedRow(), 0)).intValue();
		PhotocenterDBManager dbManager;
		try {
			switch (type) {
			case 1:
				dbManager = PhotocenterDBManager.getInstance();
				dbManager.deleteOrder(id);
				break;
			case 2:
				dbManager = PhotocenterDBManager.getInstance();
				dbManager.deleteService(id);
				break;
			case 3:
				dbManager = PhotocenterDBManager.getInstance();
				dbManager.deleteClient(id);
				break;
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		refresh();
	}
	
	protected void refresh() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable responseTable = dbManager.executePreparedStatement(pstmt);
			table.setData(responseTable.getData());
			table.setColumns(responseTable.getColumnLabels());
			table.refresh();
			responseTable = null;
			owner.updateStatus(TableTab.this);
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	private int type;
	
}
