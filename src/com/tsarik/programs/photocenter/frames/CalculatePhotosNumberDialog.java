package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class CalculatePhotosNumberDialog extends AbstractCalculationDialog {
	
	public CalculatePhotosNumberDialog(MainFrame owner) {
		super(owner, "Calculate Number Of Photos/Films", 400, 400);
		tfTabTitle.setText("Number Of Photos/Films");
	}
	
	@Override
	protected void refresh() {
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		cbKiosks.removeAllItems();
		cbKiosks.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT kiosks.kiosk_id,kiosks.address,branches.name,branches.address FROM kiosks,branches WHERE kiosks.parent_branch_id = branches.branch_id ORDER BY kiosks.kiosk_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbKiosks.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + " - " + table.getItem(i,3) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.calculatePhotosNumber(
					cbMethod.getSelectedIndex(),
					cbBranchEnabled.isSelected(),
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					(cbBranchNameEnabled.isSelected())?tfBranchName.getText():"",
					(cbBranchAddrEnabled.isSelected())?tfBranchAddr.getText():"",
					cbKiosksEnabled.isSelected(),
					((ComboBoxDatabaseItem)cbKiosks.getSelectedItem()).getId(),
					cbIsPressing.getSelectedIndex()-1,
					dpOrderDatePanel.getSign(),
					dpOrderDatePanel.getValue1(),
					dpOrderDatePanel.getValue2()
					);
			if (table != null) {
				owner.addTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbBranches = new JComboBox();
		cbKiosks = new JComboBox();
		tfBranchName = new JTextField();
		tfBranchAddr = new JTextField();
		cbBranchEnabled = new JRadioButton("Branch");
		cbBranchEnabled.setSelected(true);
		cbKiosksEnabled = new JRadioButton("Kiosk");
		ButtonGroup gr = new ButtonGroup();
		gr.add(cbBranchEnabled);
		gr.add(cbKiosksEnabled);
		cbMethod = new JComboBox(new String[]{"Printed Photos","Developed Films"});
		cbIsPressing = new JComboBox(new String[]{"","Ordinary","Pressing"});
		dpOrderDatePanel = new DatabaseDatePanel();
		
		cbBranchNameEnabled = new JCheckBox("Branch Name");
		cbBranchAddrEnabled = new JCheckBox("Branch Address");

		// create constraints
		constraints.weightx = 100;
		constraints.insets = new Insets(1,1,1,1);
		
		JPanel branchPanel = new JPanel();
		branchPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbBranchNameEnabled, 0, 0, 1, 1, branchPanel);
		addComponentToGrid(tfBranchName, 1, 0, 1, 1, branchPanel);
		addComponentToGrid(cbBranchAddrEnabled, 0, 1, 1, 1, branchPanel);
		addComponentToGrid(tfBranchAddr, 1, 1, 1, 1, branchPanel);
		
		// put components on the panel
		addComponentToGrid(cbBranchEnabled, 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(branchPanel, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(cbKiosksEnabled, 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbKiosks, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("What"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(cbMethod, 1, 3, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Is Pressing"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(cbIsPressing, 1, 4, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Booking Date"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(dpOrderDatePanel, 1, 5, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbBranches;
	private JTextField tfBranchName;
	private JTextField tfBranchAddr;
	private JCheckBox cbBranchNameEnabled;
	private JCheckBox cbBranchAddrEnabled;
	private JComboBox cbKiosks;
	private JRadioButton cbBranchEnabled;
	private JRadioButton cbKiosksEnabled;
	private JComboBox cbMethod;
	private JComboBox cbIsPressing;
	private DatabaseDatePanel dpOrderDatePanel;
	
}
