package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.tsarik.dialogs.event.AbstractDialogEvent;
import com.tsarik.dialogs.event.AbstractDialogListener;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;




public abstract class AbstractEditDialog extends AbstractPhotocenterProgramDialog {
	
	public AbstractEditDialog(MainFrame owner, String title, int w, int h, int id) {
		super(owner, title, w, h, new String[]{"Update", "Cancel"});
		this.id = id;
		init();
	}
	
	protected int id;
	protected JTextField tfId;
	
	@Override
	protected Component createComponentPanel() {
		Component panel = super.createComponentPanel();
		return panel;
	}
	
	protected void selectComboBoxDatabaseItemById(JComboBox cb, int id) {
		for (int i = 0; i < cb.getItemCount(); i++) {
			if (((ComboBoxDatabaseItem)cb.getItemAt(i)).getId() == id) {
				cb.setSelectedIndex(i);
				break;
			}
		}
	}

	private void init() {
		buttonPanel.add(new JLabel("ID "),0);
		tfId = new JTextField("");
		tfId.setPreferredSize(new Dimension(50, (int)tfId.getPreferredSize().getHeight()));
		tfId.setText(String.valueOf(id));
		tfId.setEnabled(false);
		buttonPanel.add(tfId,1);
		addAbstractDialogListener(new AbstractDialogListener(){
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				switch (evt.getButton()) {
				case 0:
					if (apply() == APPLY_CHECK_OK) {
						AbstractEditDialog.this.dispose();
					}
					break;
				case 1:
					AbstractEditDialog.this.dispose();
					break;
				}
			}
		});
	}
}
