package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;

public class OpenScriptAction extends MainFrameMenuAction {
	
	public OpenScriptAction(MainFrame owner) {
		super(owner, "Open Script...");
		putValue(SHORT_DESCRIPTION, "Open Script");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.openScript();
	}
	
}
