SELECT 
	"Branch" AS "Branch / Kiosk", 
	branches.name AS "Branch Name / Kiosk Address", 
	branches.address AS "Branch Address / Parent Branch", 
	COUNT(orders.order_id) AS "Developed Film Count" 

FROM orders, branches 

WHERE orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.development_type_id IS NOT NULL 
	AND orders.done = 1 

GROUP BY orders.branch_id 




UNION 




SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	COUNT(orders.order_id) 

FROM orders, branches, kiosks 

WHERE orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND kiosks.parent_branch_id = branches.branch_id  
	AND orders.development_type_id IS NOT NULL 
	AND orders.done = 1 

GROUP BY orders.branch_id 



