package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddEquipmentDialog extends AbstractAddDialog {
	
	public AddEquipmentDialog(MainFrame owner) {
		super(owner, "Add Equipment", 450, 250);
	}
	
	public AddEquipmentDialog(AbstractDialog owner) {
		super(owner, "Add Equipment", 450, 250);
	}
	
	@Override
	protected void refresh() {
		cbEquipmentTypes.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT equipment_types.equipment_type_id,equipment_types.name FROM equipment_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipmentTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		if ( (tfName.getText().equals("")) || (tfProducer.getText().equals("")) ) {
			JOptionPane.showMessageDialog(owner, Parameters.ERROR_INPUT, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
			return APPLY_CHECK_FAIL;
		}
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addEquipment(
					((ComboBoxDatabaseItem)cbEquipmentTypes.getSelectedItem()).getId(),
					tfName.getText(),
					((SpinnerNumberModel)spCost.getModel()).getNumber().doubleValue(),
					tfProducer.getText()); 
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbEquipmentTypes = new JComboBox();
		tfName = new JTextField();
		spCost = new JSpinner(new SpinnerNumberModel(10.0,0.0,999.9,0.1));
		tfProducer = new JTextField();
		
		JButton btAddEquipmentType = new JButton("Add");
		btAddEquipmentType.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbEquipmentTypes.getItemCount();
				int s = cbEquipmentTypes.getSelectedIndex();
				AddEquipmentTypeDialog d = new AddEquipmentTypeDialog(AddEquipmentDialog.this);
				d.setVisible(true);
				refresh();
				if (cbEquipmentTypes.getItemCount() != c) {
					cbEquipmentTypes.setSelectedIndex(cbEquipmentTypes.getItemCount()-1);
				} else {
					if (s < cbEquipmentTypes.getItemCount()) {
						cbEquipmentTypes.setSelectedIndex(s);
					}
				}
			}
		});
		
		// put components on the panel
		addComponentToGrid(new JLabel("Equipment Type"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbEquipmentTypes, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(btAddEquipmentType, 2, 0, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Name"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfName, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Cost"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(spCost, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Producer"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(tfProducer, 1, 3, 1, 1, 2, panel);
		
		return panel;
	}
	
	private JComboBox cbEquipmentTypes;
	private JTextField tfName;
	private JSpinner spCost;
	private JTextField tfProducer;
	
}
