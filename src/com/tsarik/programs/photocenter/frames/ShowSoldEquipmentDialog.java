package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class ShowSoldEquipmentDialog extends AbstractShowDialog {
	
	public ShowSoldEquipmentDialog(MainFrame owner) {
		super(owner, "Show Sold Equipment", 400, 300);
		tfTabTitle.setText("Sold Equipment");
	}
	
	@Override
	protected void refresh() {
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.showSoldEquipment(
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					(cbBranchNameEnabled.isSelected())?tfBranchName.getText():"",
					(cbBranchAddrEnabled.isSelected())?tfBranchAddr.getText():"",
					dpSaleDatePanel.getSign(),
					dpSaleDatePanel.getValue1(),
					dpSaleDatePanel.getValue2()
					);
			if (table != null) {
				owner.addTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbBranches = new JComboBox();
		tfBranchName = new JTextField();
		tfBranchAddr = new JTextField();
		dpSaleDatePanel = new DatabaseDatePanel();
		cbBranchNameEnabled = new JCheckBox("Branch Name");
		cbBranchAddrEnabled = new JCheckBox("Branch Address");

		constraints.weightx = 1;
		constraints.insets = new Insets(1,1,1,1);
		JPanel branchPanel = new JPanel();
		branchPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbBranchNameEnabled, 0, 0, 1, 1, branchPanel);
		addComponentToGrid(tfBranchName, 1, 0, 1, 1, branchPanel);
		addComponentToGrid(cbBranchAddrEnabled, 0, 1, 1, 1, branchPanel);
		addComponentToGrid(tfBranchAddr, 1, 1, 1, 1, branchPanel);
		
		// put components on the panel
		addComponentToGrid(new JLabel("Branch"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(branchPanel, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Sale Date"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(dpSaleDatePanel, 1, 2, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbBranches;
	private JTextField tfBranchName;
	private JTextField tfBranchAddr;
	private JCheckBox cbBranchNameEnabled;
	private JCheckBox cbBranchAddrEnabled;
	private DatabaseDatePanel dpSaleDatePanel;
	
}
