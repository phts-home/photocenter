package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddOrderDialog;


public class AddOrderAction extends MainFrameMenuAction {
	
	public AddOrderAction(MainFrame owner) {
		super(owner, "Order...");
		putValue(SHORT_DESCRIPTION, "Add Order...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddOrderDialog d = new AddOrderDialog(owner);
		d.setVisible(true);
	}
}
