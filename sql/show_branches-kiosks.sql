SELECT 
	"Branch" AS "Branch / Kiosk", 
	branches.name AS "Branch Name / Kiosk Address", 
	branches.address AS "Branch Address / Parent Branch", 
	branches.main AS "Main Office", 
	NULL AS "Install Date", 
	NULL AS "Expiration Date" 

FROM branches 



UNION


SELECT 
	"Kiosk" AS "Branch / Kiosk", 
	kiosks.address AS "Branch Name / Kiosk Address", 
	CONCAT(branches.name," (",branches.address,")") AS "Branch Address / Parent Branch", 
	NULL AS "Main Office", 
	kiosks.install_date AS "Install Date", 
	kiosks.exp_date AS "Expiration Date" 

FROM kiosks, branches 

WHERE branches.branch_id = kiosks.parent_branch_id 
	AND (kiosks.exp_date IS NULL OR kiosks.exp_date > NOW())


