package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class EditClientDialog extends AbstractEditDialog {
	
	public EditClientDialog(MainFrame owner, int id) {
		super(owner, "Edit Order", 450, 300, id);
	}
	
	@Override
	protected void refresh() {
		String firstname = "";
		String lastname = "";
		String address = "";
		boolean professional = false;
		
		boolean discount = false;
		double rate = 0;
		java.sql.Timestamp exp_date = null;
		
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT firstname,lastname,address,professional FROM clients WHERE client_id = " + String.valueOf(id));
			if (table.getRecordCount() != 0) {
				if (table.getItem(0,0) != null) {
					firstname = (String)table.getItem(0,0);
				}
				if (table.getItem(0,1) != null) {
					lastname = (String)table.getItem(0,1);
				}
				if (table.getItem(0,2) != null) {
					address = (String)table.getItem(0,2);
				}
				if (table.getItem(0,3) != null) {
					professional = ((Boolean)table.getItem(0,3)).booleanValue();
				}
			}
			table = dbManager.queryToTable("SELECT rate,exp_date FROM discounts WHERE client_id = " + String.valueOf(id));
			if (table.getRecordCount() != 0) {
				if (table.getItem(0,0) != null) {
					rate = ((Double)table.getItem(0,0)).doubleValue();
				}
				if (table.getItem(0,1) != null) {
					exp_date = (java.sql.Timestamp)table.getItem(0,1);
				}
				discount = true;
			}
			
			tfFirstname.setText(firstname);
			tfLastname.setText(lastname);
			tfAddress.setText(address);
			cbProfessional.setSelected(professional);
			cbDiscountEnabled.setSelected(discount);
			if (discount) {
				spRate.setValue(rate);
				if (exp_date != null) {
					((SpinnerDateModel)spExpDate.getModel()).setValue(new java.util.Date(exp_date.getTime()));
				}
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		if ( (tfFirstname.getText().equals("")) || (tfLastname.getText().equals("")) || (tfAddress.getText().equals("")) ) {
			JOptionPane.showMessageDialog(owner, Parameters.ERROR_INPUT, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
			return APPLY_CHECK_FAIL;
		}
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.editClient(id,
					tfFirstname.getText(),
					tfLastname.getText(),
					tfAddress.getText(),
					cbProfessional.isSelected(),
					cbDiscountEnabled.isSelected(),
					((SpinnerNumberModel)spRate.getModel()).getNumber().doubleValue(),
					((SpinnerDateModel)spExpDate.getModel()).getDate());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		tfFirstname = new JTextField();
		tfLastname = new JTextField();
		tfAddress = new JTextField();
		cbProfessional = new JCheckBox("Professional");
		cbDiscountEnabled = new JCheckBox("Discount");;
		spRate = new JSpinner(new SpinnerNumberModel(0.0,0.0,1.0,0.01));
		spExpDate = new JSpinner(new SpinnerDateModel());
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spExpDate.setValue(new Date());
			}
		});
		
		// put components on the panel
		addComponentToGrid(new JLabel("Last Name"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfLastname, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("First Name"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfFirstname, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Address"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(tfAddress, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(cbProfessional, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(cbDiscountEnabled, 1, 4, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Discount Rate"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(spRate, 1, 5, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Expiration Date"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(spExpDate, 1, 6, 1, 1, 2, panel);
		addComponentToGrid(btNow, 2, 6, 1, 1, 3, panel);
		return panel;
	}
	
	private JTextField tfFirstname;
	private JTextField tfLastname;
	private JTextField tfAddress;
	private JCheckBox cbProfessional;
	private JCheckBox cbDiscountEnabled;
	private JSpinner spRate;
	private JSpinner spExpDate;
	
	
}
