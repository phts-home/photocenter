package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.CalculatePhotosNumberDialog;


public class CalculatePhotosNumberAction extends MainFrameMenuAction {
	
	public CalculatePhotosNumberAction(MainFrame owner) {
		super(owner, "Number Of Photos/Films...");
		putValue(SHORT_DESCRIPTION, "Calculate Number Of Photos/Films...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		CalculatePhotosNumberDialog d = new CalculatePhotosNumberDialog(owner);
		d.setVisible(true);
	}
}
