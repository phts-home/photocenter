SELECT 
	service_id AS "Service ID", 
	service_types.name AS "Service Type", 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name" , 
	clients.address AS "Client's Address" , 
	CONCAT(branches.name," (", branches.address, ")") AS "Branch" , 
	photo_equipment.name AS "Equipment", 
	services.service_date AS "Booking Date", 
	services.exp_date AS "Expiration Date", 
	services.done AS "Is Done", 
	services.paid AS "Is Paid" 

FROM services, clients, branches, service_types, photo_equipment 

WHERE services.client_id = clients.client_id 
	AND services.branch_id = branches.branch_id 
	AND services.service_type_id = service_types.service_type_id 
	AND services.photo_equipment_id = photo_equipment.photo_equipment_id


UNION


SELECT 
	service_id AS "Service ID", 
	service_types.name AS "Service Type", 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name" , 
	clients.address AS "Client's Address" , 
	CONCAT(branches.name," (", branches.address, ")") AS "Branch" , 
	"-" AS "Equipment", 
	services.service_date AS "Booking Date", 
	services.exp_date AS "Expiration Date", 
	services.done AS "Is Done", 
	services.paid AS "Is Paid" 

FROM services, clients, branches, service_types

WHERE services.client_id = clients.client_id 
	AND services.branch_id = branches.branch_id 
	AND services.service_type_id = service_types.service_type_id 
	AND services.photo_equipment_id IS NULL

ORDER BY "Booking Date" 

