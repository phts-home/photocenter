package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddSupplyDialog;


public class AddSupplyAction extends MainFrameMenuAction {
	
	public AddSupplyAction(MainFrame owner) {
		super(owner, "Supply...");
		putValue(SHORT_DESCRIPTION, "Add Supply...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddSupplyDialog d = new AddSupplyDialog(owner);
		d.setVisible(true);
	}
}
