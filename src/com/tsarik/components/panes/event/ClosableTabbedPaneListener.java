package com.tsarik.components.panes.event;

import java.util.EventListener;



/**
 * The listener interface for receiving <code>ClosableTabbedPane</code> events.<br><br>
 * 
 * Example:<br>
 * <code><pre>
 * ClosableTabbedPane pane = new ClosableTabbedPane();
 * pane.setClosableTabbedPaneListener(new MyPaneHandler());
 * </pre></code>
 * <code>...</code>
 * <code><pre>
 * private class MyPaneHandler implements ClosableTabbedPaneListener {
 *      public void tabCreated(evt) {
 *          // do something
 *      }
 *      public void tabClosing(evt) {
 *          // do something
 *      }
 *      public void tabClosed(evt) {
 *          // do something
 *      }
 * }
 * </pre></code>
 * 
 * @author Phil Tsarik
 *
 */
public interface ClosableTabbedPaneListener extends EventListener {
	
	/**
	 * Invoked when a tab has been created as the result of calling method <code>addTab</code>.
	 * 
	 * @param evt <code>ClosableTabbedPaneEvent</code>
	 */
	public void tabCreated(ClosableTabbedPaneEvent evt);
	
	/**
	 * Invoked when the user attempts to close the tab.
	 * 
	 * @param evt <code>ClosableTabbedPaneEvent</code>
	 */
	public void tabClosing(ClosableTabbedPaneEvent evt);
	
	/**
	 * Invoked when a tab has been closed as the result of calling method <code>closeTab</code>.
	 * 
	 * @param evt <code>ClosableTabbedPaneEvent</code>
	 */
	public void tabClosed(ClosableTabbedPaneEvent evt);
	
}
