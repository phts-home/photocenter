/* branch AND development AND printing AND discount */
SELECT orders.order_id AS "Order ID", 
	CONCAT(development_types.name,", ",printing_types.name) AS "Order Type", 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Branch / Kiosk Address", 
	"-" AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, development_types, printing_types, frames, paper_types, format_types, discounts 

WHERE clients.client_id = orders.client_id 
AND branches.branch_id = orders.branch_id 
AND orders.kiosk_id IS NULL 
AND development_types.development_type_id = orders.development_type_id 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
AND discounts.client_id = orders.client_id 


UNION 


/* kiosk AND development AND printing AND discount */
SELECT orders.order_id AS "Order ID", 
	CONCAT(development_types.name,", ",printing_types.name) AS "Order Type", 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(kiosks.address) AS "Branch / Kiosk Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, kiosks, development_types, printing_types, frames, paper_types, format_types, discounts 

WHERE clients.client_id = orders.client_id 
AND orders.branch_id IS NULL 
AND orders.kiosk_id = kiosks.kiosk_id 
AND branches.branch_id = kiosks.parent_branch_id 
AND development_types.development_type_id = orders.development_type_id 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
AND discounts.client_id = orders.client_id 


UNION 


/* branch AND development AND not printing AND discount */
SELECT orders.order_id AS "Order ID", 
	development_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Branch / Kiosk Address", 
	"-" AS "Parent Branch", 
	0 AS "Number Of Photos", 
	"-" AS "Paper Type", 
	"-" AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	development_types.cost*(orders.pressing+1)*(1-discounts.rate) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, development_types, discounts 

WHERE clients.client_id = orders.client_id 
AND branches.branch_id = orders.branch_id 
AND orders.kiosk_id IS NULL 
AND orders.printing_type_id IS NULL 
AND development_types.development_type_id = orders.development_type_id 
AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
AND discounts.client_id = orders.client_id 


UNION 


/* kiosk AND development AND not printing AND discount */
SELECT orders.order_id AS "Order ID", 
	development_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(kiosks.address) AS "Branch / Kiosk Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Parent Branch", 
	0 AS "Number Of Photos", 
	"-" AS "Paper Type", 
	"-" AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	development_types.cost*(orders.pressing+1)*(1-discounts.rate) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, kiosks, development_types, discounts 

WHERE clients.client_id = orders.client_id 
AND orders.branch_id IS NULL 
AND orders.kiosk_id = kiosks.kiosk_id 
AND branches.branch_id = kiosks.parent_branch_id 
AND orders.printing_type_id IS NULL 
AND development_types.development_type_id = orders.development_type_id 
AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
AND discounts.client_id = orders.client_id 


UNION 


/* branch AND not development AND printing AND discount */
SELECT orders.order_id AS "Order ID", 
	printing_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Branch / Kiosk Address", 
	"-" AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid"

FROM orders, clients, branches, printing_types, paper_types, format_types, frames, discounts 

WHERE clients.client_id = orders.client_id 
AND branches.branch_id = orders.branch_id 
AND orders.kiosk_id IS NULL 
AND orders.development_type_id IS NULL 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
AND discounts.client_id = orders.client_id 


UNION


/* kiosk AND not development AND printing AND discount */
SELECT orders.order_id AS "Order ID", 
	printing_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(kiosks.address) AS "Branch / Kiosk Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid"

FROM orders, clients, branches, kiosks, printing_types, paper_types, format_types, frames, discounts 

WHERE clients.client_id = orders.client_id 
AND orders.branch_id IS NULL 
AND orders.kiosk_id = kiosks.kiosk_id 
AND branches.branch_id = kiosks.parent_branch_id 
AND orders.development_type_id IS NULL 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
AND discounts.client_id = orders.client_id 


UNION


/* branch AND development AND printing AND no discount */
SELECT orders.order_id AS "Order ID", 
	CONCAT(development_types.name,", ",printing_types.name) AS "Order Type", 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Branch / Kiosk Address", 
	"-" AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, development_types, printing_types, frames, paper_types, format_types 

WHERE clients.client_id = orders.client_id 
AND branches.branch_id = orders.branch_id 
AND orders.kiosk_id IS NULL 
AND development_types.development_type_id = orders.development_type_id 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 


UNION


/* kiosk AND development AND printing AND no discount */
SELECT orders.order_id AS "Order ID", 
	CONCAT(development_types.name,", ",printing_types.name) AS "Order Type", 
	CONCAT(clients.firstname, " ", clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(kiosks.address) AS "Branch / Kiosk Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, kiosks, development_types, printing_types, frames, paper_types, format_types 

WHERE clients.client_id = orders.client_id 
AND orders.branch_id IS NULL 
AND orders.kiosk_id = kiosks.kiosk_id 
AND branches.branch_id = kiosks.parent_branch_id 
AND development_types.development_type_id = orders.development_type_id 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 


UNION 


/* branch AND development AND not printing AND no discount */
SELECT orders.order_id AS "Order ID", 
	development_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Branch / Kiosk Address", 
	"-" AS "Parent Branch", 
	0 AS "Number Of Photos", 
	"-" AS "Paper Type", 
	"-" AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	development_types.cost*(orders.pressing+1) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, development_types 

WHERE clients.client_id = orders.client_id 
AND branches.branch_id = orders.branch_id 
AND orders.kiosk_id IS NULL 
AND orders.printing_type_id IS NULL 
AND development_types.development_type_id = orders.development_type_id 
AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 


UNION 


/* kiosk AND development AND not printing AND no discount */
SELECT orders.order_id AS "Order ID", 
	development_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(kiosks.address) AS "Branch / Kiosk Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Parent Branch", 
	0 AS "Number Of Photos", 
	"-" AS "Paper Type", 
	"-" AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	development_types.cost*(orders.pressing+1) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid" 

FROM orders, clients, branches, kiosks, development_types 

WHERE clients.client_id = orders.client_id 
AND orders.branch_id IS NULL 
AND orders.kiosk_id = kiosks.kiosk_id 
AND branches.branch_id = kiosks.parent_branch_id 
AND orders.printing_type_id IS NULL 
AND development_types.development_type_id = orders.development_type_id 
AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 


UNION 


/* branch AND not development AND printing AND no discount */
SELECT orders.order_id AS "Order ID", 
	printing_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Branch / Kiosk Address", 
	"-" AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid"

FROM orders, clients, branches, printing_types, paper_types, format_types, frames 

WHERE clients.client_id = orders.client_id 
AND branches.branch_id = orders.branch_id 
AND orders.kiosk_id IS NULL 
AND orders.development_type_id IS NULL 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 


UNION 


/* kiosk AND not development AND printing AND no discount */
SELECT orders.order_id AS "Order ID", 
	printing_types.name AS "Order Type", 
	CONCAT(clients.firstname, " ", 
	clients.lastname) AS "Client's Full Name", 
	clients.address AS "Client's Address", 
	CONCAT(kiosks.address) AS "Branch / Kiosk Address", 
	CONCAT(branches.name," (" ,branches.address, ")") AS "Parent Branch", 
	frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS "Number Of Photos", 
	paper_types.name AS "Paper Type", 
	format_types.name AS "Format", 
	orders.pressing AS "Is Pressing", 
	orders.order_date AS "Booking Date", 
	((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1) AS "Price", 
	orders.done AS "Is Done", 
	orders.paid AS "Is Paid"

FROM orders, clients, branches, kiosks, printing_types, paper_types, format_types, frames 

WHERE clients.client_id = orders.client_id 
AND orders.branch_id IS NULL 
AND orders.kiosk_id = kiosks.kiosk_id 
AND orders.development_type_id IS NULL 
AND printing_types.printing_type_id = orders.printing_type_id 
AND frames.frames_id = orders.frames_id 
AND paper_types.paper_type_id = orders.paper_type_id 
AND format_types.format_type_id = orders.format_type_id 
AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 



ORDER BY `Order ID`
