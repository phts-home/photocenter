package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddDiscountDialog extends AbstractAddDialog {
	
	public AddDiscountDialog(MainFrame owner) {
		super(owner, "Add Discount", 450, 250);
	}
	
	public AddDiscountDialog(AbstractDialog owner) {
		super(owner, "Add Discount", 450, 250);
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addDiscount(((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(), 
					((SpinnerNumberModel)spDiscountRate.getModel()).getNumber().doubleValue(), 
					((SpinnerDateModel)spExpDate.getModel()).getDate());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		spDiscountRate = new JSpinner(new SpinnerNumberModel(0.0, 0.0, 1.0, 0.01));
		spExpDate = new JSpinner(new SpinnerDateModel());
		
		JButton btNow2 = new JButton("Now");
		btNow2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spExpDate.setValue(new Date());
			}
		});
		
		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbClients, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Discount Rate"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(spDiscountRate, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Expiration Date"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(spExpDate, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(btNow2, 2, 2, 1, 1, 3, panel);
		
		return panel;
	}
	
	private JComboBox cbClients;
	private JSpinner spDiscountRate;
	private JSpinner spExpDate;
	
}
