package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddDeliverableEquipmentDialog extends AbstractAddDialog {
	
	public AddDeliverableEquipmentDialog(MainFrame owner) {
		super(owner, "Add Distribution", 450, 300);
	}
	
	public AddDeliverableEquipmentDialog(AbstractDialog owner) {
		super(owner, "Add Distribution", 450, 300);
	}
	
	@Override
	protected void refresh() {
		cbSuppliers.removeAllItems();
		cbEquipment.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT supplier_id,name,address FROM suppliers");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbSuppliers.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT equipment.equipment_id,equipment.name,equipment_types.name FROM equipment,equipment_types WHERE equipment.equipment_type_id=equipment_types.equipment_type_id ORDER BY equipment.equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addDeliverableEquipment(
					((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbSuppliers.getSelectedItem()).getId(),
					((SpinnerNumberModel)spQuotation.getModel()).getNumber().doubleValue()); 
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbEquipment = new JComboBox();
		cbSuppliers = new JComboBox();
		spQuotation = new JSpinner(new SpinnerNumberModel(10.0,0.0,999.0,0.1));
		spDistributionDate = new JSpinner(new SpinnerDateModel());
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spDistributionDate.setValue(new Date());
			}
		});
		
		JButton btAddEquipment = new JButton("Add");
		btAddEquipment.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbEquipment.getItemCount();
				int s = cbEquipment.getSelectedIndex();
				AddEquipmentDialog d = new AddEquipmentDialog(AddDeliverableEquipmentDialog.this);
				d.setVisible(true);
				refresh();
				if (cbEquipment.getItemCount() != c) {
					cbEquipment.setSelectedIndex(cbEquipment.getItemCount()-1);
				} else {
					if (s < cbEquipment.getItemCount()) {
						cbEquipment.setSelectedIndex(s);
					}
				}
			}
		});

		// put components on the panel
		addComponentToGrid(new JLabel("Equipment"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbEquipment, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(btAddEquipment, 2, 0, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Supplier"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(cbSuppliers, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Quotation"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(spQuotation, 1, 2, 1, 1, 2, panel);
		
		return panel;
	}
	
	private JComboBox cbEquipment;
	private JComboBox cbSuppliers;
	private JSpinner spQuotation;
	private JSpinner spDistributionDate;
	
}
