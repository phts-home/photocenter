package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddKioskDialog extends AbstractAddDialog {
	
	public AddKioskDialog(MainFrame owner) {
		super(owner, "Add Kiosk", 450, 300);
	}
	
	public AddKioskDialog(AbstractDialog owner) {
		super(owner, "Add Kiosk", 450, 300);
	}
	
	@Override
	protected void refresh() {
		cbBranches.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		if (tfAddr.getText().equals("")) {
			JOptionPane.showMessageDialog(owner, Parameters.ERROR_INPUT, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
			return APPLY_CHECK_FAIL;
		}
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addKiosk(((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(), 
					tfAddr.getText(), 
					((SpinnerDateModel)spInstallDate.getModel()).getDate(), 
					cpExpDateEnable.isSelected(), 
					((SpinnerDateModel)spExpDate.getModel()).getDate(),
					((SpinnerNumberModel)spWorkplaces.getModel()).getNumber().intValue());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbBranches = new JComboBox();
		tfAddr = new JTextField();
		spInstallDate = new JSpinner(new SpinnerDateModel());
		cpExpDateEnable = new JCheckBox("Expiration Date");
		spExpDate = new JSpinner(new SpinnerDateModel());
		spWorkplaces = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spInstallDate.setValue(new Date());
			}
		});
		JButton btNow2 = new JButton("Now");
		btNow2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spExpDate.setValue(new Date());
			}
		});

		// put components on the panel
		addComponentToGrid(new JLabel("Parent Branch"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Address"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfAddr, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Install Date"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(spInstallDate, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(btNow, 2, 2, 1, 1, 3, panel);
		addComponentToGrid(cpExpDateEnable, 0, 3, 1, 1, 1, panel);
		addComponentToGrid(spExpDate, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(btNow2, 2, 3, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Workplaces"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(spWorkplaces, 1, 4, 1, 1, 2, panel);
		
		return panel;
	}
	
	private JComboBox cbBranches;
	private JTextField tfAddr;
	private JSpinner spInstallDate;
	private JCheckBox cpExpDateEnable;
	private JSpinner spExpDate;
	private JSpinner spWorkplaces;
	
}
