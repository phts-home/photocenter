package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowClientsDialog;

public class ShowClientsDialogAction extends MainFrameMenuAction {
	
	public ShowClientsDialogAction(MainFrame owner) {
		super(owner, "Clients...");
		putValue(SHORT_DESCRIPTION, "Show Clients");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowClientsDialog d = new ShowClientsDialog(owner);
		d.setVisible(true);
	}
}
