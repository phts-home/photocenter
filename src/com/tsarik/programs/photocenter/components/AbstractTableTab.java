package com.tsarik.programs.photocenter.components;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;

public abstract class AbstractTableTab extends JPanel {
	
	public AbstractTableTab(MainFrame owner, String title, Object[][] data, Object[] columnNames, Object[] columnLabels, PreparedStatement pstmt) {
		this(owner, title, data, columnNames, columnLabels, pstmt, true, true, true);
	}
	
	public AbstractTableTab(MainFrame owner, String title, ResponseTable table) {
		this(owner, title, table.getData(), table.getColumnNames(), table.getColumnLabels(), table.getStatement(), true, true, true);
	}
	
	public AbstractTableTab(MainFrame owner, String title, Object[][] data, Object[] columnNames, Object[] columnLabels, PreparedStatement pstmt, boolean editButton, boolean deleteButton, boolean refreshButton) {
		this.owner = owner;
		this.data = data;
		this.columnNames = columnNames;
		this.columnLabels = columnLabels;
		this.pstmt = pstmt;
		this.editButton = editButton;
		this.deleteButton = deleteButton;
		this.refreshButton = refreshButton;
		createComponents();
	}
	
	public AbstractTableTab(MainFrame owner, String title, ResponseTable table, boolean editButton, boolean deleteButton, boolean refreshButton) {
		this(owner, title, table.getData(), table.getColumnNames(), table.getColumnLabels(), table.getStatement(), editButton, deleteButton, refreshButton);
	}
	
	public void closeResourses() {
		try {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLOSERESOURCES, e.getMessage());
		}
	}
	
	public int getRecordCount() {
		if (data != null) {
			return data.length;
		}
		return 0;
	}
	
	protected MainFrame owner;
	protected Object[][] data;
	protected Object[] columnLabels;
	protected Object[] columnNames;
	protected PreparedStatement pstmt;
	protected AdvancedTable table;
	
	protected abstract void edit();
	protected abstract void delete();
	protected abstract void refresh();
	
	private boolean editButton;
	private boolean deleteButton;
	private boolean refreshButton;
	
	private void createComponents() {
		// create components panel
		Component compPanel = createComponentPanel();
		Component buttonPanel = createButtonPanel();
		
		// place components on frame
		this.setLayout(new BorderLayout());
		this.add(compPanel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	private Component createComponentPanel() {
		JPanel panel = new JPanel();
		
		table = new AdvancedTable(data, columnLabels);
		JScrollPane scrollPane = new JScrollPane(table);
		
		panel.setLayout(new BorderLayout());
		panel.add(table.getTableHeader(), BorderLayout.PAGE_START);
		panel.add(scrollPane, BorderLayout.CENTER);
		
		return panel;
	}
	
	/**
	 * Creates button panel.
	 * 
	 * @return Panel
	 */
	private Component createButtonPanel() {
		JPanel buttonsPanel = new JPanel();
		if (editButton) {
			JButton butEdit = new JButton("Edit");
			butEdit.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt) {
					edit();
				}
			});
			buttonsPanel.add(butEdit);
		}
		
		if (deleteButton) {
			JButton butDelete = new JButton("Delete");
			butDelete.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt) {
					delete();
				}
			});
			buttonsPanel.add(butDelete);
		}
		
		if (refreshButton) {
			JButton butClose = new JButton("Refresh");
			butClose.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt) {
					refresh();
				}
			});
			buttonsPanel.add(butClose);
		}
		
		return buttonsPanel;
	}
}
