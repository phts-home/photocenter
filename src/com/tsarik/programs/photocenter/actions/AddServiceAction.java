package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddServiceDialog;


public class AddServiceAction extends MainFrameMenuAction {
	
	public AddServiceAction(MainFrame owner) {
		super(owner, "Service...");
		putValue(SHORT_DESCRIPTION, "Add Service...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_S);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddServiceDialog d = new AddServiceDialog(owner);
		d.setVisible(true);
	}
}
