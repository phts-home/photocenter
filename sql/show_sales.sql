SELECT 
	branches.name AS "Branch Name", 
	branches.address AS "Branch Address", 
	SUM(equipment.cost) AS "Sale Price", 
	sales.sale_date AS "Sale Date" 

FROM branches, equipment, sales, sold_equipment 

WHERE sales.branch_id = branches.branch_id 
	AND sold_equipment.sale_id = sales.sale_id 
	AND sold_equipment.equipment_id = equipment.equipment_id 

GROUP BY sales.sale_id 

ORDER BY `Sale Date` 