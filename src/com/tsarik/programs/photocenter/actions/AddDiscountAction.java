package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddDiscountDialog;


public class AddDiscountAction extends MainFrameMenuAction {
	
	public AddDiscountAction(MainFrame owner) {
		super(owner, "Discount...");
		putValue(SHORT_DESCRIPTION, "Add Discount...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_D);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddDiscountDialog d = new AddDiscountDialog(owner);
		d.setVisible(true);
	}
}
