package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowSuppliesDialog;


public class ShowSuppliesDialogAction extends MainFrameMenuAction {
	
	public ShowSuppliesDialogAction(MainFrame owner) {
		super(owner, "Supplies...");
		putValue(SHORT_DESCRIPTION, "Show Supplies");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowSuppliesDialog d = new ShowSuppliesDialog(owner);
		d.setVisible(true);
	}
}
