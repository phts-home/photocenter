SELECT 
	"Branches" AS "Branches / Kiosks", 
	COUNT(branches.branch_id) AS "Number" 

FROM branches 

GROUP BY `Branch / Kiosk` 


UNION


SELECT 
	"Kiosks", 
	COUNT(kiosks.kiosk_id) 

FROM branches, kiosks 

WHERE kiosks.parent_branch_id = branches.branch_id
	AND (kiosks.exp_date IS NULL OR kiosks.exp_date > NOW())

GROUP BY `Branch / Kiosk` 



UNION


SELECT 
	"Total", 
	(SELECT COUNT(branches.branch_id) FROM branches GROUP BY "Branch / Kiosk") 
		+ (SELECT COUNT(kiosks.kiosk_id) FROM kiosks WHERE (kiosks.exp_date IS NULL OR kiosks.exp_date > NOW())) 

