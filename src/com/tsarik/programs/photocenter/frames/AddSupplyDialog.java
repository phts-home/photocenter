package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddSupplyDialog extends AbstractAddDialog {
	
	public AddSupplyDialog(MainFrame owner) {
		super(owner, "Add Supply", 450, 200);
	}
	
	public AddSupplyDialog(AbstractDialog owner) {
		super(owner, "Add Supply", 450, 200);
	}
	
	@Override
	protected void refresh() {
		cbDeliverableEquipment.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT deliverable_equipment.deliverable_equipment_id,equipment.name,suppliers.name FROM deliverable_equipment,equipment,suppliers WHERE deliverable_equipment.equipment_id = equipment.equipment_id AND deliverable_equipment.supplier_id = suppliers.supplier_id ORDER BY deliverable_equipment.deliverable_equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbDeliverableEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " - " + table.getItem(i,2)));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addSupply(
					((ComboBoxDatabaseItem)cbDeliverableEquipment.getSelectedItem()).getId(),
					((SpinnerNumberModel)spQuantity.getModel()).getNumber().intValue(),
					((SpinnerDateModel)spSupplyDate.getModel()).getDate()); 
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbDeliverableEquipment = new JComboBox();
		spQuantity = new JSpinner(new SpinnerNumberModel(1,0,999,1));
		spSupplyDate = new JSpinner(new SpinnerDateModel());
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spSupplyDate.setValue(new Date());
			}
		});
		
		JButton btAddDeliverableEquipment = new JButton("Add");
		btAddDeliverableEquipment.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbDeliverableEquipment.getItemCount();
				int s = cbDeliverableEquipment.getSelectedIndex();
				AddDeliverableEquipmentDialog d = new AddDeliverableEquipmentDialog(AddSupplyDialog.this);
				d.setVisible(true);
				refresh();
				if (cbDeliverableEquipment.getItemCount() != c) {
					cbDeliverableEquipment.setSelectedIndex(cbDeliverableEquipment.getItemCount()-1);
				} else {
					if (s < cbDeliverableEquipment.getItemCount()) {
						cbDeliverableEquipment.setSelectedIndex(s);
					}
				}
			}
		});

		// put components on the panel
		addComponentToGrid(new JLabel("Equipment-Supplier"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbDeliverableEquipment, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(btAddDeliverableEquipment, 2, 0, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Quantity"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(spQuantity, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Supply Date"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(spSupplyDate, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(btNow, 2, 3, 1, 1, 3, panel);
		
		return panel;
	}
	
	private JComboBox cbDeliverableEquipment;
	private JSpinner spQuantity;
	private JSpinner spSupplyDate;
	
}
