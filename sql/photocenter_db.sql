-- phpMyAdmin SQL Dump
-- version 2.6.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Dec 15, 2009 at 01:06 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.4
-- 
-- Database: `photocenter`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `branches`
-- 

CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `cashiers` int(11) NOT NULL,
  `photographers` int(11) NOT NULL,
  `managers` int(11) NOT NULL,
  `service_staff` int(11) NOT NULL,
  `other` int(11) NOT NULL,
  PRIMARY KEY  (`branch_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `branches`
-- 

INSERT INTO `branches` VALUES (1, '�������', '���������� 12', 1, 5, 10, 2, 10, 0);
INSERT INTO `branches` VALUES (2, '������', '�������� 15', 0, 3, 5, 1, 5, 0);
INSERT INTO `branches` VALUES (3, 'Kodak', '��������� 20', 0, 2, 4, 1, 7, 0);
INSERT INTO `branches` VALUES (4, '������', '���������� 140', 0, 1, 3, 1, 6, 0);
INSERT INTO `branches` VALUES (5, '������', '���������� 2', 0, 1, 3, 1, 4, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `clients`
-- 

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL auto_increment,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `professional` tinyint(1) NOT NULL,
  PRIMARY KEY  (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `clients`
-- 

INSERT INTO `clients` VALUES (1, '����', '������', '���������� 11, 3', 1);
INSERT INTO `clients` VALUES (2, '������', '������', '�������� 10, 55', 0);
INSERT INTO `clients` VALUES (3, '�������', '�������', '�������� 20, 10', 0);
INSERT INTO `clients` VALUES (4, '���������', '�����������', '��������� 13, 14', 1);
INSERT INTO `clients` VALUES (5, '�������', '��������', '�������� 13', 1);
INSERT INTO `clients` VALUES (6, '�������', '����������', '������ 40, 58', 1);
INSERT INTO `clients` VALUES (7, '������', '���������', '������ 16, 2', 0);
INSERT INTO `clients` VALUES (8, '�����', '�������', '��������� 10, 15', 0);
INSERT INTO `clients` VALUES (9, '������', '�����������', '�������� 7, 106', 1);
INSERT INTO `clients` VALUES (10, '�����', '������', '��������� 2, 2', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `deliverable_equipment`
-- 

CREATE TABLE `deliverable_equipment` (
  `deliverable_equipment_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `cost` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- 
-- Dumping data for table `deliverable_equipment`
-- 

INSERT INTO `deliverable_equipment` VALUES (1, 2, 2, 0);
INSERT INTO `deliverable_equipment` VALUES (2, 1, 2, 9000);
INSERT INTO `deliverable_equipment` VALUES (3, 5, 4, 0);
INSERT INTO `deliverable_equipment` VALUES (4, 7, 4, 0);
INSERT INTO `deliverable_equipment` VALUES (5, 3, 1, 0);
INSERT INTO `deliverable_equipment` VALUES (6, 4, 1, 0);
INSERT INTO `deliverable_equipment` VALUES (7, 6, 3, 0);
INSERT INTO `deliverable_equipment` VALUES (8, 1, 5, 10000);

-- --------------------------------------------------------

-- 
-- Table structure for table `development_types`
-- 

CREATE TABLE `development_types` (
  `development_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY  (`development_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `development_types`
-- 

INSERT INTO `development_types` VALUES (1, 'Development', 1000);

-- --------------------------------------------------------

-- 
-- Table structure for table `discounts`
-- 

CREATE TABLE `discounts` (
  `discount_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL,
  `rate` double NOT NULL,
  `exp_date` datetime NOT NULL,
  PRIMARY KEY  (`discount_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `discounts`
-- 

INSERT INTO `discounts` VALUES (5, 1, 0.06, '2010-12-02 20:06:00');
INSERT INTO `discounts` VALUES (2, 2, 0.5, '2010-12-14 00:00:00');
INSERT INTO `discounts` VALUES (8, 4, 0.02, '2009-12-16 00:14:00');
INSERT INTO `discounts` VALUES (7, 6, 0.04, '2010-05-17 00:08:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `distributions`
-- 

CREATE TABLE `distributions` (
  `equipment_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `distribution_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- 
-- Dumping data for table `distributions`
-- 

INSERT INTO `distributions` VALUES (5, 5, 100, '2009-12-02 01:44:33');

-- --------------------------------------------------------

-- 
-- Table structure for table `equipment`
-- 

CREATE TABLE `equipment` (
  `equipment_id` int(11) NOT NULL auto_increment,
  `equipment_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `producer` varchar(50) NOT NULL,
  PRIMARY KEY  (`equipment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `equipment`
-- 

INSERT INTO `equipment` VALUES (1, 2, 'canon cn123', 300000, 'Canon Inc.');
INSERT INTO `equipment` VALUES (2, 2, 'canon cn1567', 350000, 'Canon Inc.');
INSERT INTO `equipment` VALUES (3, 1, 'kodak bw 36', 15000, 'Eastman Kodak Company');
INSERT INTO `equipment` VALUES (4, 1, 'kodak col 24', 16000, 'Eastman Kodak Company');
INSERT INTO `equipment` VALUES (5, 3, 'photo album for 30 photos', 4000, 'Lomond Trading Ltd.');
INSERT INTO `equipment` VALUES (6, 4, 'Epson C91', 0, 'Seiko Epson Corporation');
INSERT INTO `equipment` VALUES (7, 6, 'matt paper A4 500', 15000, 'Lomond Trading Ltd.');

-- --------------------------------------------------------

-- 
-- Table structure for table `equipment_types`
-- 

CREATE TABLE `equipment_types` (
  `equipment_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `for_sale` tinyint(1) NOT NULL,
  PRIMARY KEY  (`equipment_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `equipment_types`
-- 

INSERT INTO `equipment_types` VALUES (1, 'Film', 1);
INSERT INTO `equipment_types` VALUES (2, 'Camera', 1);
INSERT INTO `equipment_types` VALUES (3, 'Photo album', 1);
INSERT INTO `equipment_types` VALUES (4, 'Printer', 0);
INSERT INTO `equipment_types` VALUES (5, 'Development equipment', 0);
INSERT INTO `equipment_types` VALUES (6, 'Paper', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `format_types`
-- 

CREATE TABLE `format_types` (
  `format_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY  (`format_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `format_types`
-- 

INSERT INTO `format_types` VALUES (1, '3*4', 500);
INSERT INTO `format_types` VALUES (2, '10*15', 700);
INSERT INTO `format_types` VALUES (3, '18*25', 1000);
INSERT INTO `format_types` VALUES (4, '20*30', 1500);
INSERT INTO `format_types` VALUES (5, '30*45', 2000);

-- --------------------------------------------------------

-- 
-- Table structure for table `frames`
-- 

CREATE TABLE `frames` (
  `frames_id` int(11) NOT NULL auto_increment,
  `count_1` int(11) default '0',
  `count_2` int(11) default '0',
  `count_3` int(11) default '0',
  `count_4` int(11) default '0',
  `count_5` int(11) default '0',
  `count_6` int(11) default '0',
  `count_7` int(11) default '0',
  `count_8` int(11) default '0',
  `count_9` int(11) default '0',
  `count_10` int(11) default '0',
  `count_11` int(11) default '0',
  `count_12` int(11) default '0',
  `count_13` int(11) default '0',
  `count_14` int(11) default '0',
  `count_15` int(11) default '0',
  `count_16` int(11) default '0',
  `count_17` int(11) default '0',
  `count_18` int(11) default '0',
  `count_19` int(11) default '0',
  `count_20` int(11) default '0',
  `count_21` int(11) default '0',
  `count_22` int(11) default '0',
  `count_23` int(11) default '0',
  `count_24` int(11) default '0',
  `count_25` int(11) default '0',
  `count_26` int(11) default '0',
  `count_27` int(11) default '0',
  `count_28` int(11) default '0',
  `count_29` int(11) default '0',
  `count_30` int(11) default '0',
  `count_31` int(11) default '0',
  `count_32` int(11) default '0',
  `count_33` int(11) default '0',
  `count_34` int(11) default '0',
  `count_35` int(11) default '0',
  `count_36` int(11) default '0',
  PRIMARY KEY  (`frames_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `frames`
-- 

INSERT INTO `frames` VALUES (1, 1, 0, 0, 0, 4, 0, 0, 6, 0, 3, 0, 0, 0, 0, 0, 5, 0, 0, 6, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `frames` VALUES (2, 0, 3, 0, 0, 4, 0, 5, 0, 6, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 2, 0, 2, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `frames` VALUES (3, 1, 1, 0, 1, 1, 0, 1, 0, 2, 2, 0, 1, 0, 1, 1, 0, 3, 3, 3, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `frames` VALUES (4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `frames` VALUES (5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `frames` VALUES (6, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);
INSERT INTO `frames` VALUES (10, 6, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `installed_equipment`
-- 

CREATE TABLE `installed_equipment` (
  `equipment_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `install_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- 
-- Dumping data for table `installed_equipment`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `kiosks`
-- 

CREATE TABLE `kiosks` (
  `kiosk_id` int(11) NOT NULL auto_increment,
  `parent_branch_id` int(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  `install_date` datetime NOT NULL,
  `exp_date` datetime default NULL,
  `workplaces` int(11) NOT NULL,
  PRIMARY KEY  (`kiosk_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `kiosks`
-- 

INSERT INTO `kiosks` VALUES (1, 2, '���������� 154', '2009-10-06 20:41:21', '2010-10-20 20:41:28', 1);
INSERT INTO `kiosks` VALUES (2, 2, '������ 10', '2009-10-01 20:41:44', '2009-11-24 20:41:49', 1);
INSERT INTO `kiosks` VALUES (3, 1, '�������� 15', '2009-11-01 19:13:59', '2010-02-01 19:13:00', 1);
INSERT INTO `kiosks` VALUES (4, 4, '�. ������ 2', '2009-11-01 19:20:37', NULL, 1);
INSERT INTO `kiosks` VALUES (6, 2, '��������� 5', '2009-11-01 19:21:04', '2009-11-02 19:22:00', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL,
  `branch_id` int(11) default NULL,
  `kiosk_id` int(11) default NULL,
  `development_type_id` int(11) default NULL,
  `printing_type_id` int(11) default NULL,
  `frames_id` int(11) default NULL,
  `format_type_id` int(11) default NULL,
  `paper_type_id` int(11) default NULL,
  `pressing` tinyint(1) NOT NULL default '0',
  `order_date` datetime NOT NULL,
  `done` tinyint(1) NOT NULL default '0',
  `paid` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=17 ;

-- 
-- Dumping data for table `orders`
-- 

INSERT INTO `orders` VALUES (1, 1, 2, NULL, 1, 1, 1, 2, 1, 0, '2009-10-20 18:30:00', 1, 0);
INSERT INTO `orders` VALUES (2, 2, 1, NULL, 1, NULL, NULL, NULL, NULL, 1, '2009-10-21 11:02:00', 1, 1);
INSERT INTO `orders` VALUES (3, 4, NULL, 1, 1, 2, 2, 3, 2, 1, '2009-10-22 16:44:02', 1, 1);
INSERT INTO `orders` VALUES (4, 3, 3, NULL, 1, NULL, NULL, NULL, NULL, 1, '2009-12-02 03:53:26', 1, 1);
INSERT INTO `orders` VALUES (5, 5, 3, NULL, NULL, 2, 3, 3, 3, 1, '2009-10-28 21:37:22', 1, 1);
INSERT INTO `orders` VALUES (16, 3, 5, NULL, 1, 1, 10, 5, 2, 0, '2009-12-02 21:52:47', 0, 0);
INSERT INTO `orders` VALUES (8, 2, 2, NULL, 1, 2, 3, 2, 1, 0, '2009-10-21 11:02:00', 1, 1);
INSERT INTO `orders` VALUES (9, 2, NULL, 1, 1, 2, 2, 3, 2, 0, '2009-10-22 16:44:02', 1, 1);
INSERT INTO `orders` VALUES (10, 1, NULL, 4, 1, 1, 5, 1, 1, 0, '2009-11-14 16:18:10', 0, 0);
INSERT INTO `orders` VALUES (11, 1, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, '2009-12-01 10:55:49', 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `paper_types`
-- 

CREATE TABLE `paper_types` (
  `paper_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`paper_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `paper_types`
-- 

INSERT INTO `paper_types` VALUES (1, 'Glossy');
INSERT INTO `paper_types` VALUES (2, 'Matt');
INSERT INTO `paper_types` VALUES (3, 'Glossy-matt');

-- --------------------------------------------------------

-- 
-- Table structure for table `printing_types`
-- 

CREATE TABLE `printing_types` (
  `printing_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `cost_mult` double NOT NULL,
  PRIMARY KEY  (`printing_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `printing_types`
-- 

INSERT INTO `printing_types` VALUES (1, 'Ordinary Printing', 1);
INSERT INTO `printing_types` VALUES (2, 'HQ Printing', 1.5);

-- --------------------------------------------------------

-- 
-- Table structure for table `sales`
-- 

CREATE TABLE `sales` (
  `sale_id` int(11) NOT NULL auto_increment,
  `branch_id` int(11) NOT NULL,
  `sale_date` datetime NOT NULL,
  PRIMARY KEY  (`sale_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `sales`
-- 

INSERT INTO `sales` VALUES (1, 1, '2009-11-01 15:51:26');
INSERT INTO `sales` VALUES (2, 1, '2009-11-03 15:51:31');
INSERT INTO `sales` VALUES (3, 2, '2009-11-04 15:51:49');
INSERT INTO `sales` VALUES (4, 1, '2009-11-02 15:51:51');
INSERT INTO `sales` VALUES (5, 3, '2009-11-06 15:52:01');
INSERT INTO `sales` VALUES (6, 2, '2009-11-24 15:52:03');
INSERT INTO `sales` VALUES (7, 2, '2009-11-26 15:52:17');
INSERT INTO `sales` VALUES (8, 3, '2009-11-28 15:52:20');
INSERT INTO `sales` VALUES (10, 2, '2009-12-08 21:58:03');

-- --------------------------------------------------------

-- 
-- Table structure for table `service_types`
-- 

CREATE TABLE `service_types` (
  `service_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY  (`service_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `service_types`
-- 

INSERT INTO `service_types` VALUES (1, 'Photo for documents', 1500);
INSERT INTO `service_types` VALUES (2, 'Restoration', 5000);
INSERT INTO `service_types` VALUES (3, 'Camera hire', 10000);
INSERT INTO `service_types` VALUES (4, 'Art photography', 5000);
INSERT INTO `service_types` VALUES (5, 'Proffesional photographer service', 10000);

-- --------------------------------------------------------

-- 
-- Table structure for table `services`
-- 

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `service_type_id` int(11) NOT NULL,
  `equipment_id` int(11) default NULL,
  `service_date` datetime NOT NULL,
  `exp_date` datetime NOT NULL,
  `done` tinyint(1) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  PRIMARY KEY  (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `services`
-- 

INSERT INTO `services` VALUES (1, 3, 3, 3, 1, '2009-10-20 21:18:52', '2009-10-27 21:19:16', 0, 0);
INSERT INTO `services` VALUES (2, 2, 2, 4, NULL, '2009-10-19 21:19:46', '2009-10-22 21:19:50', 0, 0);
INSERT INTO `services` VALUES (3, 3, 1, 1, 1, '2009-11-01 20:20:20', '2009-11-01 20:20:20', 1, 0);
INSERT INTO `services` VALUES (4, 1, 1, 1, 5, '2009-11-17 01:24:05', '2009-11-17 01:24:05', 0, 0);
INSERT INTO `services` VALUES (5, 1, 1, 1, 7, '2009-11-17 01:24:05', '2009-11-17 01:24:05', 0, 0);
INSERT INTO `services` VALUES (6, 1, 1, 1, 7, '2009-11-17 01:49:19', '2009-11-17 01:49:19', 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `sold_equipment`
-- 

CREATE TABLE `sold_equipment` (
  `sale_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- 
-- Dumping data for table `sold_equipment`
-- 

INSERT INTO `sold_equipment` VALUES (1, 1, 1);
INSERT INTO `sold_equipment` VALUES (1, 3, 2);
INSERT INTO `sold_equipment` VALUES (1, 5, 1);
INSERT INTO `sold_equipment` VALUES (2, 2, 1);
INSERT INTO `sold_equipment` VALUES (2, 4, 1);
INSERT INTO `sold_equipment` VALUES (3, 4, 2);
INSERT INTO `sold_equipment` VALUES (4, 1, 1);
INSERT INTO `sold_equipment` VALUES (5, 2, 2);
INSERT INTO `sold_equipment` VALUES (6, 3, 1);
INSERT INTO `sold_equipment` VALUES (7, 4, 1);
INSERT INTO `sold_equipment` VALUES (8, 5, 1);
INSERT INTO `sold_equipment` VALUES (10, 1, 1);
INSERT INTO `sold_equipment` VALUES (10, 7, 4);

-- --------------------------------------------------------

-- 
-- Table structure for table `suppliers`
-- 

CREATE TABLE `suppliers` (
  `supplier_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  PRIMARY KEY  (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `suppliers`
-- 

INSERT INTO `suppliers` VALUES (1, 'Kodak', '�������� 32');
INSERT INTO `suppliers` VALUES (2, 'Canon', '�������� 20');
INSERT INTO `suppliers` VALUES (3, 'Epson', '���������� 34');
INSERT INTO `suppliers` VALUES (4, 'Lomond', '������ 50');
INSERT INTO `suppliers` VALUES (5, 'Kodak5', '�������� 45');

-- --------------------------------------------------------

-- 
-- Table structure for table `supplies`
-- 

CREATE TABLE `supplies` (
  `deliverable_equipment_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `supply_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- 
-- Dumping data for table `supplies`
-- 

INSERT INTO `supplies` VALUES (1, 3, '2009-11-01 15:29:49');
INSERT INTO `supplies` VALUES (1, 1, '2009-11-03 15:29:59');
INSERT INTO `supplies` VALUES (1, 2, '2009-11-02 15:30:11');
INSERT INTO `supplies` VALUES (2, 1, '2009-11-06 15:30:23');
INSERT INTO `supplies` VALUES (2, 2, '2009-11-08 15:30:47');
INSERT INTO `supplies` VALUES (3, 5, '2009-11-04 15:30:51');
INSERT INTO `supplies` VALUES (8, 1, '2009-12-02 01:38:27');
        