package com.tsarik.programs.photocenter.components;

public class ComboBoxDatabaseItem {
	
	public ComboBoxDatabaseItem() {
		this.id = 0;
		this.item = "";
	}
	
	public ComboBoxDatabaseItem(int id, Object item) {
		this.id = id;
		this.item = item;
	}
	
	public int getId() {
		return id;
	}
	
	public Object getItem() {
		return item;
	}
	
	@Override
	public String toString() {
		if (id == 0) {
			return item.toString();
		}
		return id + " - " + item.toString();
	}
	
	private int id;
	private Object item;
}
