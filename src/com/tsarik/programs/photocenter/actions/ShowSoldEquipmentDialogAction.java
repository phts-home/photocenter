package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowSoldEquipmentDialog;


public class ShowSoldEquipmentDialogAction extends MainFrameMenuAction {
	
	public ShowSoldEquipmentDialogAction(MainFrame owner) {
		super(owner, "Sold Equipment...");
		putValue(SHORT_DESCRIPTION, "Show Sold Equipment");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_Q);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowSoldEquipmentDialog d = new ShowSoldEquipmentDialog(owner);
		d.setVisible(true);
	}
}
