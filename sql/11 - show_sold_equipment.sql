SELECT 
	branches.name AS "Branch Name", 
	branches.address AS "Branch Address", 
	equipment.name AS "Equipment Name", 
	equipment_types.name AS "Equipment Type", 
	SUM(sold_equipment.quantity) AS "Quantity", 
	SUM(equipment.cost) AS "Price"/*, 
	sales.sale_date AS "Sale Date" */
	
FROM branches, equipment, sales, sold_equipment, equipment_types 

WHERE 
	sales.branch_id = branches.branch_id 
	AND sold_equipment.sale_id = sales.sale_id 
	AND sold_equipment.equipment_id = equipment.equipment_id 
	AND equipment.equipment_type_id = equipment_types.equipment_type_id 
	
	
	
GROUP BY equipment.equipment_id, branches.branch_id 
	
ORDER BY /*`Sale Date`, */`Equipment Name` 
