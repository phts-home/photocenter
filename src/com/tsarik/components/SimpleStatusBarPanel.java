package com.tsarik.components;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;


/**
 * A panel which is used by the <code>SimpleStatusBar</code>.
 * 
 * @see SimpleStatusBar
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class SimpleStatusBarPanel extends JLabel {
	
	/**
	 * Constructs a new instance of the <code>SimpleStatusBarPanel</code> class.
	 * 
	 * @param text Text
	 * @param width Width of the this panel
	 * @param height Height of the this panel
	 * @param alignment Text alignment
	 * @param border The border type
	 */
	public SimpleStatusBarPanel(String text, int width, int height, int alignment, int border) {
		super(text);
		this.width = width;
		this.height = height;
		setPanelSize(new Dimension(width, height));
		if (border != -1) {
			setBorder(BorderFactory.createEtchedBorder(border));
		}
		setHorizontalAlignment(alignment);
	}
	
	/**
	 * Gets panel height.
	 * 
	 * @return Panel height
	 */
	public int getPanelHeight() {
		return height;
	}
	
	/**
	 * Sets panel height.
	 * 
	 * @param height Panel height
	 */
	public void setPanelHeight(int height) {
		this.height = height;
		setPanelSize(new Dimension(width, height));
	}
	
	/**
	 * Gets panel width.
	 * 
	 * @return Panel width
	 */
	public int getPanelWidth() {
		return super.getWidth();
	}
	
	/**
	 * Sets panel width.
	 * 
	 * @param width Panel width
	 */
	public void setPanelWidth(int width) {
		this.width = width;
		setPanelSize(new Dimension(width, height));
	}
	
	/**
	 * Sets text alignment.
	 * 
	 * @param alignment Text alignment
	 */
	public void setAlignment(int alignment) {
		setHorizontalAlignment(alignment);
	}
	
	/**
	 * Sets panel border.
	 * 
	 * @param border Panel border
	 */
	public void setBorder(int border) {
		setBorder(BorderFactory.createEtchedBorder(border));
	}
	
	private int width;
	private int height;
	
	/**
	 * Sets panel size.
	 * 
	 * @param d Panel dimension
	 */
	private void setPanelSize(Dimension d) {
		setPreferredSize(d);
		setMaximumSize(d);
		setMinimumSize(d);
	}
	
}
