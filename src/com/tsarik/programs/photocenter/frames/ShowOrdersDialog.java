package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class ShowOrdersDialog extends AbstractShowDialog {
	
	public ShowOrdersDialog(MainFrame owner) {
		super(owner, "Show Orders", 400, 550);
		tfTabTitle.setText("Orders");
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbClients.addItem(new ComboBoxDatabaseItem());
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		cbKiosks.removeAllItems();
		cbKiosks.addItem(new ComboBoxDatabaseItem());
		cbDevelopmentTypes.removeAllItems();
		cbDevelopmentTypes.addItem(new ComboBoxDatabaseItem());
		cbPrintingTypes.removeAllItems();
		cbPrintingTypes.addItem(new ComboBoxDatabaseItem());
		cbFormatTypes.removeAllItems();
		cbFormatTypes.addItem(new ComboBoxDatabaseItem());
		cbPaperTypes.removeAllItems();
		cbPaperTypes.addItem(new ComboBoxDatabaseItem());
		
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT kiosks.kiosk_id,kiosks.address,branches.name,branches.address FROM kiosks,branches WHERE kiosks.parent_branch_id = branches.branch_id ORDER BY kiosks.kiosk_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbKiosks.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + " - " + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT development_type_id,name,cost FROM development_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbDevelopmentTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT printing_type_id,name,cost_mult FROM printing_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbPrintingTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT format_type_id,name,cost FROM format_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbFormatTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT paper_type_id,name FROM paper_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbPaperTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.showOrders(((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(),
					(cbClientFirstnameEnabled.isSelected())?tfClientFirstname.getText():"",
					(cbClientLastnameEnabled.isSelected())?tfClientLastname.getText():"",
					(cbClientAddressEnabled.isSelected())?tfClientAddr.getText():"",
					cbBranchEnabled.isSelected(),
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					(cbBranchNameEnabled.isSelected())?tfBranchName.getText():"",
					(cbBranchAddrEnabled.isSelected())?tfBranchAddr.getText():"",
					cbKiosksEnabled.isSelected(),
					((ComboBoxDatabaseItem)cbKiosks.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbDevelopmentTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbPrintingTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbFormatTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbPaperTypes.getSelectedItem()).getId(),
					cbIsPressing.getSelectedIndex()-1,
					dpOrderDatePanel.getSign(),
					dpOrderDatePanel.getValue1(),
					dpOrderDatePanel.getValue2(),
					cbIsDone.getSelectedIndex()-1, 
					cbIsPaid.getSelectedIndex()-1);
			if (table != null) {
				owner.addOrdersTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		tfClientFirstname = new JTextField();
		tfClientLastname = new JTextField();
		tfClientAddr = new JTextField();
		cbBranches = new JComboBox();
		cbKiosks = new JComboBox();
		tfBranchName = new JTextField();
		tfBranchAddr = new JTextField();
		cbBranchEnabled = new JRadioButton("Branch");
		cbBranchEnabled.setSelected(true);
		cbKiosksEnabled = new JRadioButton("Kiosk");
		ButtonGroup gr = new ButtonGroup();
		gr.add(cbBranchEnabled);
		gr.add(cbKiosksEnabled);
		cbIsPressing = new JComboBox(new String[]{"","Ordinary","Pressing"});
		cbIsDone = new JComboBox(new String[]{"","Not Done","Done"});
		cbIsPaid = new JComboBox(new String[]{"","Not Paid","Paid"});
		cbDevelopmentTypes = new JComboBox();
		cbPrintingTypes = new JComboBox();
		cbFormatTypes = new JComboBox();
		cbPaperTypes = new JComboBox();
		dpOrderDatePanel = new DatabaseDatePanel();
		
		cbClientFirstnameEnabled = new JCheckBox("First Name");
		cbClientLastnameEnabled = new JCheckBox("Last Name");
		cbClientAddressEnabled = new JCheckBox("Address");
		
		cbBranchNameEnabled = new JCheckBox("Branch Name");
		cbBranchAddrEnabled = new JCheckBox("Branch Address");

		constraints.weightx = 1;
		constraints.insets = new Insets(1,1,1,1);
		JPanel clientPanel = new JPanel();
		clientPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbClientFirstnameEnabled, 0, 0, 1, 1, clientPanel);
		addComponentToGrid(tfClientFirstname, 1, 0, 1, 1, clientPanel);
		addComponentToGrid(cbClientLastnameEnabled, 0, 1, 1, 1, clientPanel);
		addComponentToGrid(tfClientLastname, 1, 1, 1, 1, clientPanel);
		addComponentToGrid(cbClientAddressEnabled, 0, 2, 1, 1, clientPanel);
		addComponentToGrid(tfClientAddr, 1, 2, 1, 1, clientPanel);
		
		JPanel branchPanel = new JPanel();
		branchPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbBranchNameEnabled, 0, 0, 1, 1, branchPanel);
		addComponentToGrid(tfBranchName, 1, 0, 1, 1, branchPanel);
		addComponentToGrid(cbBranchAddrEnabled, 0, 1, 1, 1, branchPanel);
		addComponentToGrid(tfBranchAddr, 1, 1, 1, 1, branchPanel);
		
		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbClients, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(clientPanel, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(cbBranchEnabled, 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(branchPanel, 1, 3, 1, 1, 4, panel);
		addComponentToGrid(cbKiosksEnabled, 0, 4, 1, 1, 1, panel);
		addComponentToGrid(cbKiosks, 1, 4, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Development Type"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(cbDevelopmentTypes, 1, 5, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Printing Type"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(cbPrintingTypes, 1, 6, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Format"), 0, 7, 1, 1, 1, panel);
		addComponentToGrid(cbFormatTypes, 1, 7, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Paper Type"), 0, 8, 1, 1, 1, panel);
		addComponentToGrid(cbPaperTypes, 1, 8, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Is Pressing"), 0, 9, 1, 1, 1, panel);
		addComponentToGrid(cbIsPressing, 1, 9, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Order Date"), 0, 10, 1, 1, 1, panel);
		addComponentToGrid(dpOrderDatePanel, 1, 10, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Is Done"), 0, 11, 1, 1, 1, panel);
		addComponentToGrid(cbIsDone, 1, 11, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Is Paid"), 0, 12, 1, 1, 1, panel);
		addComponentToGrid(cbIsPaid, 1, 12, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbClients;
	private JTextField tfClientFirstname;
	private JTextField tfClientLastname;
	private JTextField tfClientAddr;
	private JCheckBox cbClientFirstnameEnabled;
	private JCheckBox cbClientLastnameEnabled;
	private JCheckBox cbClientAddressEnabled;
	private JComboBox cbBranches;
	private JTextField tfBranchName;
	private JTextField tfBranchAddr;
	private JCheckBox cbBranchNameEnabled;
	private JCheckBox cbBranchAddrEnabled;
	private JComboBox cbKiosks;
	private JRadioButton cbBranchEnabled;
	private JRadioButton cbKiosksEnabled;
	private JComboBox cbDevelopmentTypes;
	private JComboBox cbPrintingTypes;
	private JComboBox cbFormatTypes;
	private JComboBox cbPaperTypes;
	private JComboBox cbIsPressing;
	private DatabaseDatePanel dpOrderDatePanel;
	private JComboBox cbIsDone;
	private JComboBox cbIsPaid;
	
}
