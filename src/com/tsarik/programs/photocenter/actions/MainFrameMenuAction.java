package com.tsarik.programs.photocenter.actions;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tsarik.programs.photocenter.MainFrame;


public abstract class MainFrameMenuAction extends AbstractAction {
	
	public MainFrameMenuAction(String name) {
		super(name);
		this.owner = null;
	}
	
	public MainFrameMenuAction(MainFrame owner, String name) {
		super(name);
		this.owner = owner;
	}
	
	public MainFrameMenuAction(MainFrame owner, String name, Icon icon) {
		super(name, icon);
		this.owner = owner;
	}
	
	protected MainFrame owner;
	
}

