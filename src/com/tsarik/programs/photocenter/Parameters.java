package com.tsarik.programs.photocenter;

import java.awt.Component;

import javax.swing.JOptionPane;

public final class Parameters {
	
	public static final String PROGRAM_NAME = "Photocenter";
	public static final String PROGRAM_AUTHOR = "Phil Tsarik";
	public static final String PROGRAM_CONTACTS = "philip-s@yandex.ru";
	public static final String PROGRAM_VERSION = "0.9.3";
	public static final String PROGRAM_BUILD = "30";
	public static final String PROGRAM_YEAR = "2009";
	
	public static final String PREFERENCES_PATH = "preferences/preferences.xml";
	
	public static final String ERROR_CONNECTION = "Unable to execute the statement";
	public static final String ERROR_CLASSNOTFOUND = "Unable to find the required class";
	public static final String ERROR_FILENOTFOUND = "Unable to open the file";
	public static final String ERROR_LOOKANDFEEL = "Unable to apply LookAndFeel";
	public static final String ERROR_CLOSERESOURCES = "Unable to close connection resources";
	public static final String ERROR_UNEXPECTED = "Unexpected error";
	public static final String ERROR_INPUT = "Some fields have incorrect data";
	
	public static final String QUESTION_DELETE = "Delete this record from database?";
	
	public static final String SUCCESS_OPEN = "Script has been executed successfully";
	
	public static int MAINFRAME_WIDTH;
	public static int MAINFRAME_HEIGHT;
	public static int MAINFRAME_X;
	public static int MAINFRAME_Y;
	static public int MAINFRAME_STATE;
	
	public static String DATABASE_SERVER;
	public static String DATABASE_NAME;
	public static String DATABASE_USER;
	public static String DATABASE_PASSWORD;
	
	public static void showErrorMessage(Component parentComponent, String message, String errorMessage) {
		JOptionPane.showMessageDialog(parentComponent, message+"\n\nError message:\n\n"+"<html><textarea cols=40 rows=2>"+errorMessage.replaceAll("[\n\r]", " ")+"</textarea></html>", Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
	}
	
}
