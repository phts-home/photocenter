package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.components.DatabaseIntValuesPanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class ShowSuppliesDialog extends AbstractShowDialog {
	
	public ShowSuppliesDialog(MainFrame owner) {
		super(owner, "Show Supplies", 400, 300);
		tfTabTitle.setText("Supplies");
	}
	
	@Override
	protected void refresh() {
		cbEquipmentType.removeAllItems();
		cbEquipmentType.addItem(new ComboBoxDatabaseItem());
		cbSupplier.removeAllItems();
		cbSupplier.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT equipment_type_id,name FROM equipment_types WHERE for_sale = 1");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipmentType.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
			table = dbManager.queryToTable("SELECT supplier_id,name FROM suppliers");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbSupplier.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.showSupplies(
					((ComboBoxDatabaseItem)cbEquipmentType.getSelectedItem()).getId(),
					dpQuantityPanel.getSign(),
					dpQuantityPanel.getValue1().intValue(),
					dpQuantityPanel.getValue2().intValue(),
					((ComboBoxDatabaseItem)cbSupplier.getSelectedItem()).getId(),
					dpSupplyDatePanel.getSign(),
					dpSupplyDatePanel.getValue1(),
					dpSupplyDatePanel.getValue2()
					);
			if (table != null) {
				owner.addTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbEquipmentType = new JComboBox();
		cbSupplier = new JComboBox();
		dpQuantityPanel = new DatabaseIntValuesPanel();
		dpSupplyDatePanel = new DatabaseDatePanel();
		
		// put components on the panel
		addComponentToGrid(new JLabel("Equipment Type"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbEquipmentType, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Equipment Quantity"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(dpQuantityPanel, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Supplier"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbSupplier, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Supplies Date"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(dpSupplyDatePanel, 1, 3, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbEquipmentType;
	private JComboBox cbSupplier;
	private DatabaseIntValuesPanel dpQuantityPanel;
	private DatabaseDatePanel dpSupplyDatePanel;
	
}
