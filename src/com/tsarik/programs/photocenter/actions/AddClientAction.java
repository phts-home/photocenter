package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddClientDialog;


public class AddClientAction extends MainFrameMenuAction {
	
	public AddClientAction(MainFrame owner) {
		super(owner, "Client...");
		putValue(SHORT_DESCRIPTION, "Add Client...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddClientDialog d = new AddClientDialog(owner);
		d.setVisible(true);
	}
}
