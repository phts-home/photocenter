SELECT 
	branches.name AS "Branch Name", 
	branches.address AS "Branch Address", 
	SUM(photo_equipment.cost*sold_equipment.quantity) AS "Sales Receipts" 
	
FROM branches, photo_equipment, sales, sold_equipment 

WHERE 
	sales.branch_id = branches.branch_id 
	AND sold_equipment.sale_id = sales.sale_id 
	AND sold_equipment.photo_equipment_id = photo_equipment.photo_equipment_id 
	
GROUP BY branches.branch_id 

ORDER BY `Branch Name` 
