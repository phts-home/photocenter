package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class EditServiceDialog extends AbstractEditDialog {
	
	public EditServiceDialog(MainFrame owner, int id) {
		super(owner, "Edit Service", 450, 350, id);
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbBranches.removeAllItems();
		cbServiceTypes.removeAllItems();
		cbEquipment.removeAllItems();
		cbEquipment.addItem(new ComboBoxDatabaseItem());
		
		int client_id = 0;
		int branch_id = 0;
		int service_type_id = 0;
		int equipment_id = 0;
		java.sql.Timestamp service_date = null;
		java.sql.Timestamp exp_date = null;
		boolean done = false;
		boolean paid = false;
		
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,branch_id,service_type_id,equipment_id,service_date,exp_date,done,paid FROM services WHERE service_id = " + String.valueOf(id));
			if (table.getRecordCount() != 0) {
				if (table.getItem(0,0) != null) {
					client_id = ((Integer)table.getItem(0,0)).intValue();
				}
				if (table.getItem(0,1) != null) {
					branch_id = ((Integer)table.getItem(0,1)).intValue();
				}
				if (table.getItem(0,2) != null) {
					service_type_id = ((Integer)table.getItem(0,2)).intValue();
				}
				if (table.getItem(0,3) != null) {
					equipment_id = ((Integer)table.getItem(0,3)).intValue();
				}
				if (table.getItem(0,4) != null) {
					service_date = (java.sql.Timestamp)table.getItem(0,4);
				}
				if (table.getItem(0,5) != null) {
					exp_date = (java.sql.Timestamp)table.getItem(0,5);
				}
				if (table.getItem(0,6) != null) {
					done = ((Boolean)table.getItem(0,6)).booleanValue();
				}
				if (table.getItem(0,7) != null) {
					paid = ((Boolean)table.getItem(0,7)).booleanValue();
				}
			}
			
			table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			selectComboBoxDatabaseItemById(cbClients,client_id);
			
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			selectComboBoxDatabaseItemById(cbBranches,branch_id);
			
			table = dbManager.queryToTable("SELECT service_type_id,name,cost FROM service_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbServiceTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			selectComboBoxDatabaseItemById(cbServiceTypes,service_type_id);
			
			table = dbManager.queryToTable("SELECT equipment.equipment_id,equipment.name FROM equipment,equipment_types WHERE equipment.equipment_type_id=equipment_types.equipment_type_id AND equipment_types.for_sale=1 ORDER BY equipment.equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
			selectComboBoxDatabaseItemById(cbEquipment,equipment_id);
			
			cbDone.setSelected(done);
			cbPaid.setSelected(paid);
			if (service_date != null) {
				((SpinnerDateModel)spServiceDate.getModel()).setValue(new java.util.Date(service_date.getTime()));
			}
			if (exp_date != null) {
				((SpinnerDateModel)spExpDate.getModel()).setValue(new java.util.Date(exp_date.getTime()));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.editService(id,
					((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbServiceTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getId(),
					((SpinnerDateModel)spServiceDate.getModel()).getDate(),
					((SpinnerDateModel)spExpDate.getModel()).getDate(),
					cbDone.isSelected(), 
					cbPaid.isSelected());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		cbBranches = new JComboBox();
		cbServiceTypes = new JComboBox();
		cbEquipment = new JComboBox();
		spServiceDate = new JSpinner(new SpinnerDateModel());
		spExpDate = new JSpinner(new SpinnerDateModel());
		cbDone = new JCheckBox("Done");
		cbPaid = new JCheckBox("Paid");
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spServiceDate.setValue(new Date());
			}
		});
		JButton btNow2 = new JButton("Now");
		btNow2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spExpDate.setValue(new Date());
			}
		});
		
		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbClients, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Branch"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Service Type"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbServiceTypes, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Equipment"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(cbEquipment, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Service Date"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(spServiceDate, 1, 4, 1, 1, 2, panel);
		addComponentToGrid(btNow, 2, 4, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Expiration Date"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(spExpDate, 1, 5, 1, 1, 2, panel);
		addComponentToGrid(btNow2, 2, 5, 1, 1, 3, panel);
		addComponentToGrid(cbDone, 1, 6, 1, 1, 2, panel);
		addComponentToGrid(cbPaid, 1, 7, 1, 1, 2, panel);
		return panel;
	}
	
	private JComboBox cbClients;
	private JComboBox cbBranches;
	private JComboBox cbServiceTypes;
	private JComboBox cbEquipment;
	private JSpinner spServiceDate;
	private JSpinner spExpDate;
	private JCheckBox cbDone;
	private JCheckBox cbPaid;
	
	
}
