package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddKioskDialog;


public class AddKioskAction extends MainFrameMenuAction {
	
	public AddKioskAction(MainFrame owner) {
		super(owner, "Kiosk...");
		putValue(SHORT_DESCRIPTION, "Add Kiosk...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_K);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddKioskDialog d = new AddKioskDialog(owner);
		d.setVisible(true);
	}
}
