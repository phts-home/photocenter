package com.tsarik.programs.photocenter;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.tsarik.application.preferences.SimpleProgramPreferences;



/**
 * Main entry program class.
 * 
 * @author Phil Tsarik
 *
 */
public class Photocenter {
	
	/**
	 * Program entry point.
	 * 
	 * @param args Command line arguments
	 */
	public static void main(String args[]) {
		// create preferences
		SimpleProgramPreferences preferences = new SimpleProgramPreferences(Parameters.PREFERENCES_PATH, "preferences");
		
		// create main frame
		MainFrame mainFrame = new MainFrame(preferences);
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			mainFrame.updateComponentUI();
		}
		catch (Exception e) {
			Parameters.showErrorMessage(mainFrame, Parameters.ERROR_LOOKANDFEEL, e.getMessage());
		}
		mainFrame.setVisible(true);
	}
	
}
