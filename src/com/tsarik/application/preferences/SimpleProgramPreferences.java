package com.tsarik.application.preferences;

import java.io.File;
import java.util.ArrayList;

import com.tsarik.documents.SimpleXMLDocument;



/**
 * Simple program preferences which provides to load and save preferences to XML file.
 * XML document powered by <code>SimpleXMLDocument</code>.<br>
 * This class works with <code>PreferencesCustomizable</code> and <code>PreferencesApplicable</code> interfaces.
 * 
 * @author Phil Tsarik
 * @version 1.0.4
 *
 */
public class SimpleProgramPreferences {
	
	/**
	 * Constructs a new <code>SimpleProgramPreferences</code>.
	 * 
	 * @param path Path of the XML document file
	 * @param rootName Root name of the XML document
	 */
	public SimpleProgramPreferences(String path, String rootName) {
		customizables = new ArrayList <PreferencesCustomizable> ();
		applicables = new ArrayList <PreferencesApplicable> ();
		file = new File(path);
		this.rootName = rootName;
	}
	
	/**
	 * Adds object as Customizable to list.
	 * 
	 * @param customizable Object to add
	 */
	public void addCustomizable(PreferencesCustomizable customizable) {
		if (!customizables.contains(customizable)) {
			customizables.add(customizable);
		}
	}
	
	/**
	 * Adds object as Applicable to list and applies its preferences.
	 * 
	 * @param applicable Object to add
	 */
	public void addApplicable(PreferencesApplicable applicable) {
		if (!applicables.contains(applicable)) {
			applicables.add(applicable);
		}
	}
	
	/**
	 * Adds object as Customizable to list.
	 * 
	 * @param customizable Object to add
	 */
	public void addCustomizableAndLoad(PreferencesCustomizable customizable) {
		if (!customizables.contains(customizable)) {
			customizables.add(customizable);
			load(customizable);
		}
	}
	
	/**
	 * Adds object as Applicable to list.
	 * 
	 * @param applicable Object to add
	 */
	public void addApplicableAndApply(PreferencesApplicable applicable) {
		if (!applicables.contains(applicable)) {
			applicables.add(applicable);
		}
		apply(applicable);
	}
	
	/**
	 * Executes "savePreferences" method in all Customizable objects 
	 * which had been added as Customizable by <code>addCustomizable</code> method.
	 * Then writes XML file.
	 */
	public void save() {
		SimpleXMLDocument preferences = new SimpleXMLDocument(rootName);
		for (int i = 0; i < customizables.size(); i++) {
			customizables.get(i).savePreferences(preferences);
		}
		preferences.writeXML(file);
	}
	
	/**
	 * Executes "loadPreferences" method in all objects 
	 * which had been added as Customizable by <code>addCustomizable</code> method.
	 */
	public void load() {
		SimpleXMLDocument preferences = new SimpleXMLDocument(file);
		for (int i = 0; i < customizables.size(); i++) {
			customizables.get(i).loadPreferences(preferences);
		}
	}
	
	/**
	 * Executes "applyPreferences" method in all objects 
	 * which had been added as Applicable by <code>addApplicable</code> method.
	 */
	public void apply() {
		for (int i = 0; i < applicables.size(); i++) {
			applicables.get(i).applyPreferences();
		}
	}
	
	
	protected File file;
	protected String rootName;
	protected ArrayList <PreferencesCustomizable> customizables;
	protected ArrayList <PreferencesApplicable> applicables;
	
	
	protected void load(PreferencesCustomizable customizable) {
		SimpleXMLDocument preferences = new SimpleXMLDocument(file);
		if (customizables.contains(customizable)) {
			customizables.get(customizables.indexOf(customizable)).loadPreferences(preferences);
		}
	}
	
	protected void apply(PreferencesApplicable applicable) {
		if (applicables.contains(applicable)) {
			applicables.get(applicables.indexOf(applicable)).applyPreferences();
		}
	}
	
}
