package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowServicesDialog;


public class ShowServicesDialogAction extends MainFrameMenuAction {
	
	public ShowServicesDialogAction(MainFrame owner) {
		super(owner, "Services...");
		putValue(SHORT_DESCRIPTION, "Show Services");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_S);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowServicesDialog d = new ShowServicesDialog(owner);
		d.setVisible(true);
	}
}
