package com.tsarik.programs.photocenter;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.w3c.dom.Element;

import com.tsarik.application.preferences.PreferencesCustomizable;
import com.tsarik.application.preferences.SimpleProgramPreferences;
import com.tsarik.components.SimpleStatusBar;
import com.tsarik.components.panes.ClosableTabbedPane;
import com.tsarik.components.panes.event.ClosableTabbedPaneEvent;
import com.tsarik.components.panes.event.ClosableTabbedPaneListener;
import com.tsarik.documents.SimpleXMLDocument;
import com.tsarik.programs.photocenter.actions.AboutAction;
import com.tsarik.programs.photocenter.actions.AddBranchAction;
import com.tsarik.programs.photocenter.actions.AddClientAction;
import com.tsarik.programs.photocenter.actions.AddDiscountAction;
import com.tsarik.programs.photocenter.actions.AddDistributionAction;
import com.tsarik.programs.photocenter.actions.AddEquipmentAction;
import com.tsarik.programs.photocenter.actions.AddKioskAction;
import com.tsarik.programs.photocenter.actions.AddOrderAction;
import com.tsarik.programs.photocenter.actions.AddSaleAction;
import com.tsarik.programs.photocenter.actions.AddServiceAction;
import com.tsarik.programs.photocenter.actions.AddSupplyAction;
import com.tsarik.programs.photocenter.actions.CalculateOrdersReceiptsAction;
import com.tsarik.programs.photocenter.actions.CalculatePhotosNumberAction;
import com.tsarik.programs.photocenter.actions.CalculateSalesReceiptsAction;
import com.tsarik.programs.photocenter.actions.ExitAction;
import com.tsarik.programs.photocenter.actions.MainFrameMenuAction;
import com.tsarik.programs.photocenter.actions.OpenScriptAction;
import com.tsarik.programs.photocenter.actions.PreferencesAction;
import com.tsarik.programs.photocenter.actions.PrintAction;
import com.tsarik.programs.photocenter.actions.SelectQueryAction;
import com.tsarik.programs.photocenter.actions.ShowBranchesDialogAction;
import com.tsarik.programs.photocenter.actions.ShowClientsDialogAction;
import com.tsarik.programs.photocenter.actions.ShowDemandedEquipmentDialogAction;
import com.tsarik.programs.photocenter.actions.ShowDistributionsDialogAction;
import com.tsarik.programs.photocenter.actions.ShowOrdersDialogAction;
import com.tsarik.programs.photocenter.actions.ShowSalesDialogAction;
import com.tsarik.programs.photocenter.actions.ShowServicesDialogAction;
import com.tsarik.programs.photocenter.actions.ShowSoldEquipmentDialogAction;
import com.tsarik.programs.photocenter.actions.ShowSuppliersDialogAction;
import com.tsarik.programs.photocenter.actions.ShowSuppliesDialogAction;
import com.tsarik.programs.photocenter.actions.ShowWorkplacesDialogAction;
import com.tsarik.programs.photocenter.components.AbstractTableTab;
import com.tsarik.programs.photocenter.components.TableTab;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;
import com.tsarik.programs.photocenter.frames.PreferencesDialog;


public class MainFrame extends JFrame implements PreferencesCustomizable {
	
	public MainFrame(SimpleProgramPreferences preferences) {
		super();
		this.preferences = preferences;
		preferences.addCustomizableAndLoad(this);
		createComponents();
		
		addWindowListener(new WindowEventsHandler());
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	@Override
	public void loadPreferences(SimpleXMLDocument doc) {
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		Element elem = doc.getElement(null, "main_frame");
		Parameters.MAINFRAME_WIDTH = doc.readIntegerValue(elem, "width", 700);
		Parameters.MAINFRAME_HEIGHT = doc.readIntegerValue(elem, "height", 600);
		Parameters.MAINFRAME_X = doc.readIntegerValue(elem, "x", scrSize.width/2 - Parameters.MAINFRAME_WIDTH/2);
		Parameters.MAINFRAME_Y = doc.readIntegerValue(elem, "y", scrSize.height/2 - Parameters.MAINFRAME_HEIGHT/2);
		Parameters.MAINFRAME_STATE = doc.readIntegerValue(elem, "state", Frame.NORMAL);
		elem = doc.getElement(null, "database");
		Parameters.DATABASE_SERVER = doc.readStringValue(elem, "dbserver", "");
		Parameters.DATABASE_NAME = doc.readStringValue(elem, "dbname", "");
		Parameters.DATABASE_USER = doc.readStringValue(elem, "username", "");
		Parameters.DATABASE_PASSWORD = doc.readStringValue(elem, "password", "");
		
		setTitle(Parameters.PROGRAM_NAME);
		setSize(Parameters.MAINFRAME_WIDTH, Parameters.MAINFRAME_HEIGHT);
		setLocation(Parameters.MAINFRAME_X, Parameters.MAINFRAME_Y);
		setExtendedState(Parameters.MAINFRAME_STATE);
	}
	
	@Override
	public void savePreferences(SimpleXMLDocument doc) {
		Element elem = doc.getElement(null, "main_frame");
		doc.addProperty(elem, "width", this.getWidth());
		doc.addProperty(elem, "height", this.getHeight());
		doc.addProperty(elem, "x", this.getX());
		doc.addProperty(elem, "y", this.getY());
		doc.addProperty(elem, "state", this.getState());
		elem = doc.getElement(null, "database");
		doc.addProperty(elem, "dbserver", Parameters.DATABASE_SERVER);
		doc.addProperty(elem, "dbname", Parameters.DATABASE_NAME);
		doc.addProperty(elem, "username", Parameters.DATABASE_USER);
		doc.addProperty(elem, "password", Parameters.DATABASE_PASSWORD);
	}
	
	public void showPreferences() {
		preferencesDialog.setVisible(true);
	}
	
	public void addTableTab(String title, ResponseTable responseTable) {
		tabbedPane.addTab(title, new TableTab(this, title, responseTable, false, false, true));
	}
	
	public void addOrdersTableTab(String title, ResponseTable responseTable) {
		tabbedPane.addTab(title, new TableTab(this, title, responseTable, 1));
	}
	
	public void addServicesTableTab(String title, ResponseTable responseTable) {
		tabbedPane.addTab(title, new TableTab(this, title, responseTable, 2));
	}
	
	public void addClientsTableTab(String title, ResponseTable responseTable) {
		tabbedPane.addTab(title, new TableTab(this, title, responseTable, 3));
	}
	
	
	public void openScript() {
		fileChooser.setMultiSelectionEnabled(true);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			File[] files = fileChooser.getSelectedFiles();
			for (int i = 0; i < files.length; i++) {
				openScript(files[i]);
			}
		}
	}
	
	public void openScript(File f) {
		try {
			PhotocenterDBManager bdManager = PhotocenterDBManager.getInstance();
			ResponseTable table = bdManager.queryToTable(f);
			if (table != null) {
				addTableTab(f.getName(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(this, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(this, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		catch (IOException e) {
			Parameters.showErrorMessage(this, Parameters.ERROR_FILENOTFOUND, e.getMessage());
		}
		catch (Exception e) {
			Parameters.showErrorMessage(this, Parameters.ERROR_UNEXPECTED, e.getMessage());
		}
		finally {
			JOptionPane.showMessageDialog(this, Parameters.SUCCESS_OPEN, Parameters.PROGRAM_NAME, JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public void print() {
		// TODO: printing
	}
	
	public void exit() {
		tabbedPane.closeAllTabs();
		preferences.save();
		this.dispose();
	}
	
	/**
	 * Updates components' UI
	 */
	public void updateComponentUI() {
		SwingUtilities.updateComponentTreeUI(this);
		SwingUtilities.updateComponentTreeUI(preferencesDialog);
		SwingUtilities.updateComponentTreeUI(tabPopup);
	}
	
	public void updateStatus(AbstractTableTab tab) {
		if (tab != null) {
			statusBar.getPanel(0).setText("Record Count: "+tab.getRecordCount());
		} else {
			statusBar.getPanel(0).setText("Ready");
		}
	}
	
	
	private SimpleProgramPreferences preferences;
	private PreferencesDialog preferencesDialog;
	private JFileChooser fileChooser;
	private JPopupMenu tabPopup;
	private SimpleStatusBar statusBar;
	private ClosableTabbedPane tabbedPane;
	
	/**
	 * Creates all components on the frame.
	 */
	private void createComponents() {
		// File menu
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		fileMenu.add(new OpenScriptAction(this));
		fileMenu.addSeparator();
		fileMenu.add(new PrintAction(this));
		fileMenu.addSeparator();
		fileMenu.add(new ExitAction(this));
		
		// Show menu
		JMenu showMenu = new JMenu("Show");
		showMenu.setMnemonic('H');
		showMenu.add(new ShowOrdersDialogAction(this));
		showMenu.add(new ShowServicesDialogAction(this));
		showMenu.add(new ShowClientsDialogAction(this));
		showMenu.addSeparator();
		showMenu.add(new ShowBranchesDialogAction(this));
		showMenu.add(new ShowWorkplacesDialogAction(this));
		showMenu.addSeparator();
		showMenu.add(new ShowSalesDialogAction(this));
		showMenu.add(new ShowSoldEquipmentDialogAction(this));
		showMenu.add(new ShowDemandedEquipmentDialogAction(this));
		showMenu.addSeparator();
		showMenu.add(new ShowSuppliesDialogAction(this));
		showMenu.add(new ShowSuppliersDialogAction(this));
		showMenu.addSeparator();
		showMenu.add(new ShowDistributionsDialogAction(this));
		
		// Add menu
		JMenu addMenu = new JMenu("Add");
		addMenu.setMnemonic('A');
		addMenu.add(new AddOrderAction(this));
		addMenu.add(new AddServiceAction(this));
		addMenu.addSeparator();
		addMenu.add(new AddClientAction(this));
		addMenu.add(new AddDiscountAction(this));
		addMenu.addSeparator();
		addMenu.add(new AddBranchAction(this));
		addMenu.add(new AddKioskAction(this));
		addMenu.addSeparator();
		addMenu.add(new AddEquipmentAction(this));
		addMenu.add(new AddSaleAction(this));
		addMenu.add(new AddDistributionAction(this));
		addMenu.add(new AddSupplyAction(this));
		
		// Calculate menu
		JMenu calculateMenu = new JMenu("Calculate");
		calculateMenu.setMnemonic('L');
		calculateMenu.add(new CalculateOrdersReceiptsAction(this));
		calculateMenu.add(new CalculateSalesReceiptsAction(this));
		calculateMenu.add(new CalculatePhotosNumberAction(this));
		
		// Tools menu
		JMenu toolsMenu = new JMenu("Tools");
		toolsMenu.setMnemonic('T');
		toolsMenu.add(new SelectQueryAction(this));
		toolsMenu.addSeparator();
		toolsMenu.add(new PreferencesAction(this));
		
		// Help menu
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');
		helpMenu.add(new AboutAction(this));
		
		// place menu bar
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(showMenu);
		menuBar.add(addMenu);
		menuBar.add(calculateMenu);
		menuBar.add(toolsMenu);
		menuBar.add(helpMenu);
		menuBar.setFocusTraversalKeysEnabled(false);
		
		// create pop-up menu
		tabPopup = new JPopupMenu();
		tabPopup.add(new MainFrameMenuAction("Rename"){
			@Override
			public void actionPerformed(ActionEvent e) {
				renameTab();
			}
		});
		tabPopup.add(new MainFrameMenuAction("Close"){
			@Override
			public void actionPerformed(ActionEvent e) {
				tabbedPane.closeTab();
			}
		});
		
		// create dialogs and windows
		preferencesDialog = new PreferencesDialog(this);
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));
		
		// create components
		tabbedPane = new ClosableTabbedPane();
		tabbedPane.addClosableTabbedPaneListener(new TabEventsHandler());
		tabbedPane.addChangeListener(new TabSelectionHandler());
		tabbedPane.setPopupMenu(tabPopup);
		
		statusBar = new SimpleStatusBar(20, SimpleStatusBar.BORDER_NONE);
		statusBar.addPanel("Ready", 0, 150, SimpleStatusBar.ALIGNMENT_LEFT, SimpleStatusBar.BORDER_RAISED);
		statusBar.addPanel("", 100, 100, SimpleStatusBar.ALIGNMENT_LEFT, SimpleStatusBar.BORDER_RAISED);
		
		// place components on the frame
		this.add(tabbedPane, BorderLayout.CENTER);
		this.add(statusBar, BorderLayout.SOUTH);
	}
	
	private void renameTab() {
		if (tabbedPane.getSelectedIndex() > -1) {
			String newName = JOptionPane.showInputDialog("New Name", tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
			if (newName != null) {
				tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), newName);
			}
		}
	}
	
	
	private class WindowEventsHandler extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent evt) {
			MainFrame.this.exit();
		}
	}
	
	private class TabEventsHandler implements ClosableTabbedPaneListener {
		@Override
		public void tabClosing(ClosableTabbedPaneEvent evt) {
			AbstractTableTab tab = (AbstractTableTab)evt.getComponent();
			tab.closeResourses();
		}
		@Override
		public void tabCreated(ClosableTabbedPaneEvent evt) {
		}
		@Override
		public void tabClosed(ClosableTabbedPaneEvent evt) {
		}
	}
	
	private class TabSelectionHandler implements ChangeListener {
		@Override
		public void stateChanged(ChangeEvent evt) {
			AbstractTableTab tab = (AbstractTableTab)((ClosableTabbedPane)evt.getSource()).getSelectedComponent();
			updateStatus(tab);
		}
	}
	
}
