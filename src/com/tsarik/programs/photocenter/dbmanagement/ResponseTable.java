package com.tsarik.programs.photocenter.dbmanagement;

import java.sql.PreparedStatement;

public class ResponseTable {
	
	public ResponseTable(Object[][] data, Object[] columnNames, Object[] columnLabels, PreparedStatement pstmt) {
		this.data = data;
		this.columnNames = columnNames;
		this.columnLabels = columnLabels;
		this.pstmt = pstmt;
	}
	
	public Object[] getRow(int n) {
		return data[n];
	}
	
	public Object getItem(int row, int column) {
		return data[row][column];
	}
	
	public Object[][] getData() {
		return data;
	}
	
	public Object[] getColumnNames() {
		return columnNames;
	}
	
	public Object[] getColumnLabels() {
		return columnLabels;
	}
	
	public PreparedStatement getStatement() {
		return pstmt;
	}
	
	public int getRecordCount() {
		return data.length;
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}
	
	private Object[][] data;
	private Object[] columnLabels;
	private Object[] columnNames;
	private PreparedStatement pstmt;
	
}
