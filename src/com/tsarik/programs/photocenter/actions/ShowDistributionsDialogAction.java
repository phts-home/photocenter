package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowDistributionsDialog;


public class ShowDistributionsDialogAction extends MainFrameMenuAction {
	
	public ShowDistributionsDialogAction(MainFrame owner) {
		super(owner, "Distributions...");
		putValue(SHORT_DESCRIPTION, "Show Distributions");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_D);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowDistributionsDialog d = new ShowDistributionsDialog(owner);
		d.setVisible(true);
	}
}
