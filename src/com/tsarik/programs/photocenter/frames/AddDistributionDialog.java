package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddDistributionDialog extends AbstractAddDialog {
	
	public AddDistributionDialog(MainFrame owner) {
		super(owner, "Add Distribution", 450, 250);
	}
	
	@Override
	protected void refresh() {
		cbBranches.removeAllItems();
		cbEquipment.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT equipment.equipment_id,equipment.name,equipment_types.name FROM equipment,equipment_types WHERE equipment.equipment_type_id=equipment_types.equipment_type_id ORDER BY equipment.equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addDistribution(
					((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					((SpinnerNumberModel)spQuantity.getModel()).getNumber().intValue(),
					((SpinnerDateModel)spDistributionDate.getModel()).getDate()); 
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbEquipment = new JComboBox();
		cbBranches = new JComboBox();
		spQuantity = new JSpinner(new SpinnerNumberModel(1,0,999,1));
		spDistributionDate = new JSpinner(new SpinnerDateModel());
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spDistributionDate.setValue(new Date());
			}
		});
		
		JButton btAddEquipment = new JButton("Add");
		btAddEquipment.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbEquipment.getItemCount();
				int s = cbEquipment.getSelectedIndex();
				AddEquipmentDialog d = new AddEquipmentDialog(AddDistributionDialog.this);
				d.setVisible(true);
				refresh();
				if (cbEquipment.getItemCount() != c) {
					cbEquipment.setSelectedIndex(cbEquipment.getItemCount()-1);
				} else {
					if (s < cbEquipment.getItemCount()) {
						cbEquipment.setSelectedIndex(s);
					}
				}
			}
		});

		// put components on the panel
		addComponentToGrid(new JLabel("Equipment"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbEquipment, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(btAddEquipment, 2, 0, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Destination"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Quantity"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(spQuantity, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Distribution Date"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(spDistributionDate, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(btNow, 2, 3, 1, 1, 3, panel);
		
		return panel;
	}
	
	private JComboBox cbEquipment;
	private JComboBox cbBranches;
	private JSpinner spQuantity;
	private JSpinner spDistributionDate;
	
}
