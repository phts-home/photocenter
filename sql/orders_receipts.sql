/* branch AND development AND printing AND discount */
SELECT 
	"Branch" AS "Branch / Kiosk", 
	branches.name AS "Branch Name / Kiosk Address", 
	branches.address AS "Branch Address / Parent Branch", 
	CONCAT(development_types.name,", ",printing_types.name) AS "Order Type", 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate)) AS "Orders Receipts" 

FROM orders, clients, branches, frames, format_types, printing_types, development_types, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id = development_types.development_type_id 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	AND orders.paid = 1 

GROUP BY orders.branch_id 


UNION 


/* kiosk AND development AND printing AND discount */
SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	CONCAT(development_types.name,", ",printing_types.name), 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate)) 

FROM orders, clients, branches, kiosks, frames, format_types, printing_types, development_types, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id = development_types.development_type_id 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	AND orders.paid = 1 

GROUP BY orders.kiosk_id 


UNION 


/* branch AND development AND not printing AND discount */
SELECT 
	"Branch", 
	branches.name, 
	branches.address, 
	development_types.name, 
	SUM(development_types.cost*(orders.pressing+1)*(1-discounts.rate)) 

FROM orders, clients, branches, development_types, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.printing_type_id IS NULL
	AND orders.development_type_id = development_types.development_type_id 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	AND orders.paid = 1 

GROUP BY orders.branch_id 


UNION 


/* kiosk AND development AND not printing AND discount */
SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	development_types.name, 
	SUM(development_types.cost*(orders.pressing+1)*(1-discounts.rate)) 

FROM orders, clients, branches, kiosks, development_types, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.printing_type_id IS NULL
	AND orders.development_type_id = development_types.development_type_id 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	AND orders.paid = 1 

GROUP BY orders.kiosk_id 


UNION 


/* branch AND not development AND printing AND discount */
SELECT 
	"Branch", 
	branches.name, 
	branches.address, 
	printing_types.name, 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate)) 

FROM orders, clients, branches, frames, format_types, printing_types, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id IS NULL 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	AND orders.paid = 1 

GROUP BY orders.branch_id 


UNION 


/* kiosk AND not development AND printing AND discount */
SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	printing_types.name, 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate)) 

FROM orders, clients, branches, kiosks, frames, format_types, printing_types, discounts 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id IS NULL 
	AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND discounts.client_id = orders.client_id 
	AND orders.paid = 1 

GROUP BY orders.kiosk_id 


UNION


/* branch AND development AND printing AND no discount */
SELECT 
	"Branch", 
	branches.name, 
	branches.address, 
	CONCAT(development_types.name,", ",printing_types.name), 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)) 

FROM orders, clients, branches, frames, format_types, printing_types, development_types 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id = development_types.development_type_id 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND orders.paid = 1 

GROUP BY orders.branch_id 


UNION 


/* kiosk AND development AND printing AND no discount */
SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	CONCAT(development_types.name,", ",printing_types.name), 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)) 

FROM orders, clients, branches, kiosks, frames, format_types, printing_types, development_types 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id = development_types.development_type_id 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND orders.paid = 1 

GROUP BY orders.kiosk_id 


UNION 


/* branch AND development AND not printing AND no discount */
SELECT 
	"Branch", 
	branches.name, 
	branches.address, 
	development_types.name, 
	SUM(development_types.cost*(orders.pressing+1)) 

FROM orders, clients, branches, development_types 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.printing_type_id IS NULL
	AND orders.development_type_id = development_types.development_type_id 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND orders.paid = 1 

GROUP BY orders.branch_id 


UNION 


/* kiosk AND development AND not printing AND no discount */
SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	development_types.name, 
	SUM(development_types.cost*(orders.pressing+1)) 

FROM orders, clients, branches, kiosks, development_types 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.printing_type_id IS NULL
	AND orders.development_type_id = development_types.development_type_id 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND orders.paid = 1 

GROUP BY orders.kiosk_id 


UNION 


/* branch AND not development AND printing AND no discount */
SELECT 
	"Branch", 
	branches.name, 
	branches.address, 
	printing_types.name, 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)) 

FROM orders, clients, branches, frames, format_types, printing_types 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id IS NULL 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND orders.paid = 1 

GROUP BY orders.branch_id 


UNION 


/* kiosk AND not development AND printing AND no discount */
SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	printing_types.name, 
	SUM(((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)) 

FROM orders, clients, branches, kiosks, frames, format_types, printing_types 

WHERE orders.client_id = clients.client_id 
	AND orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND branches.branch_id = kiosks.parent_branch_id 
	AND orders.frames_id = frames.frames_id 
	AND orders.format_type_id = format_types.format_type_id 
	AND orders.printing_type_id = printing_types.printing_type_id 
	AND orders.development_type_id IS NULL 
	AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) 
	AND orders.paid = 1 

GROUP BY orders.kiosk_id 
