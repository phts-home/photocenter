package com.tsarik.programs.photocenter.frames;

import java.awt.Component;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.dialogs.event.AbstractDialogEvent;
import com.tsarik.dialogs.event.AbstractDialogListener;
import com.tsarik.programs.photocenter.MainFrame;



public abstract class AbstractAddDialog extends AbstractPhotocenterProgramDialog {
	
	public AbstractAddDialog(MainFrame owner, String title, int w, int h) {
		super(owner, title, w, h, new String[] {"OK", "Cancel", "Apply", "Refresh"});
		init();
	}
	
	public AbstractAddDialog(AbstractDialog owner, String title, int w, int h) {
		super(owner, title, w, h, new String[] {"OK", "Cancel", "Apply", "Refresh"});
		init();
	}
	
	@Override
	protected Component createComponentPanel() {
		Component panel = super.createComponentPanel();
		return panel;
	}
	
	private void init() {
		addAbstractDialogListener(new AbstractDialogListener(){
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				switch (evt.getButton()) {
				case 0:
					if (apply() == APPLY_CHECK_OK) {
						AbstractAddDialog.this.dispose();
					}
					break;
				case 1:
					AbstractAddDialog.this.dispose();
					break;
				case 2:
					apply();
					break;
				case 3:
					refresh();
					break;
				}
			}
		});
	}
	
}
