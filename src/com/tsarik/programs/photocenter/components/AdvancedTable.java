package com.tsarik.programs.photocenter.components;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.ListSelectionModel;

/**
 * 
 * @author Phil Tsarik
 *
 */
public class AdvancedTable extends JTable {
	
	public AdvancedTable() {
		this(null,null);
	}
	
	public AdvancedTable(Object[][] data, Object[] columnNames) {
		super();
		this.data = data;
		this.columnNames = columnNames;
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		refresh();
	}
	
	public AdvancedTable(AdvancedTable table) {
		super();
		this.data = table.getData();
		this.columnNames = table.getColumns();
		refresh();
	}
	
	public void setData(Object[][] data) {
		this.data = data;
	}
	
	public void setColumns(Object[] columnNames) {
		this.columnNames = columnNames;
	}
	
	public Object[][] getData() {
		return data;
	}
	
	public Object[] getColumns() {
		return columnNames;
	}
	
	public void refresh() {
		setModel(new DataTableModel(data, columnNames));
	}
	
	private Object[][] data;
	private Object[] columnNames;
	
	private class DataTableModel extends AbstractTableModel {
		public DataTableModel(Object[][] data, Object[] columnNames) {
			this.data = data;
			this.columnNames = columnNames;
		}
		
		@Override
		public int getColumnCount() {
			if (columnNames == null) {
				return 0;
			}
			return columnNames.length;
		}
		
		@Override
		public int getRowCount() {
			if (data == null) {
				return 0;
			}
			return data.length;
		}
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (data == null) {
				return null;
			}
			return data[rowIndex][columnIndex];
		}
		
		@Override
		public String getColumnName(int column) {
			if (columnNames == null) {
				return null;
			}
			return (String)columnNames[column];
		}
		
		@Override
		public void setValueAt(Object value, int rowIndex, int columnIndex) {
			if (data != null) {
				data[rowIndex][columnIndex] = value;
			}
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			int i = getRowCount();
			do {
				i--;
				if (i < 0) {
					i = 0;
					break;
				}
			} while (getValueAt(i, columnIndex) == null);
			
			if (getValueAt(i, columnIndex) == null) {
				return Object.class;
			}
			if (getValueAt(i, columnIndex).getClass().getName() == "java.sql.Timestamp") {
				return Object.class;
			}
			return getValueAt(i, columnIndex).getClass();
		}
		
		private Object[][] data;
		private Object[] columnNames;
	}
	
}
