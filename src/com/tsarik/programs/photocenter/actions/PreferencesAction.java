package com.tsarik.programs.photocenter.actions;


import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;

public class PreferencesAction extends MainFrameMenuAction {
	
	public PreferencesAction(MainFrame owner) {
		super(owner, "Preferences...");
		putValue(SHORT_DESCRIPTION, "Preferences");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		
		owner.showPreferences();
	}
}
