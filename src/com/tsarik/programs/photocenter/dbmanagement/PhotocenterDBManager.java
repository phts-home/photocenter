package com.tsarik.programs.photocenter.dbmanagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.SoldEquipmentItem;
import com.tsarik.util.mysql.MySQLConnectionManager;


/**
 * 
 * @author Phil Tsarik
 *
 */
public class PhotocenterDBManager extends MySQLConnectionManager {
	
	private PhotocenterDBManager() throws SQLException, ClassNotFoundException {
		super();
	}
	
	private static PhotocenterDBManager instance;
	
	public static synchronized PhotocenterDBManager getInstance() throws SQLException, ClassNotFoundException {
		if (instance == null) {
			instance = new PhotocenterDBManager();
		}
		createConnection(Parameters.DATABASE_SERVER, Parameters.DATABASE_NAME, Parameters.DATABASE_USER, Parameters.DATABASE_PASSWORD);
		return instance;
	}
	
	
	
	public ResponseTable queryToTable(String queryText) throws SQLException {
		ResponseTable table = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(queryText);
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
		}
		return table;
	}
	
	public ResponseTable queryToTable(File file) throws SQLException, IOException {
		String queryText = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			while (reader.ready()) {
				queryText += (reader.readLine() + " ");
			}
		}
		finally {
			if (reader != null) {
				reader.close();
			}
		}
		return queryToTable(queryText);
	}
	
	public ResponseTable executePreparedStatement(PreparedStatement pstmt) throws SQLException {
		ResponseTable table = null;
		try {
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable getResponseTable(PreparedStatement pstmt) throws SQLException {
		ResponseTable table = null;
		if (rs != null) {
			Object[] columnNames = new Object[rs.getMetaData().getColumnCount()];
			for (int i = 0; i < rs.getMetaData().getColumnCount(); i++){
				columnNames[i] = rs.getMetaData().getColumnName(i+1);
			}
			Object[] columnLabels = new Object[rs.getMetaData().getColumnCount()];
			for (int i = 0; i < rs.getMetaData().getColumnCount(); i++){
				columnLabels[i] = rs.getMetaData().getColumnLabel(i+1);
			}
			Object[][] data = new Object[getResultSetRowCount()][rs.getMetaData().getColumnCount()];
			rs.beforeFirst();
			int rownum = 0;
			while (rs.next()) {
				for (int i = 0; i < rs.getMetaData().getColumnCount(); i++){
					data[rownum][i] = rs.getObject(i+1);
				}
				rownum++;
			}
			table = new ResponseTable(data, columnNames, columnLabels, pstmt);
		}
		return table;
	}
	
	public void addClient(String firstname, String lastname, String address, Boolean isProfessional) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO clients (firstname, lastname, address, professional) VALUES (?, ?, ?, ?)");
			pstmt.setString(1,firstname);
			pstmt.setString(2,lastname);
			pstmt.setString(3,address);
			pstmt.setBoolean(4,isProfessional);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addOrder(int clientId, boolean isBranch, int branchId, int kioskId, int developmentTypeId, int printingTypeId, 
			int framesId, int formatTypeId, int paperTypesId, boolean isPressing, 
			java.util.Date orderDate, boolean isDone, boolean isPaid) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO orders (client_id, " +
					((isBranch)?"branch_id, ":"kiosk_id, ") +
					((developmentTypeId!=0)?"development_type_id, ":"") +
					((printingTypeId!=0)?"printing_type_id, frames_id, format_type_id, paper_type_id, ":"") +
					"pressing, order_date, done, paid) " +
					"VALUES (?, ?, " +
					((developmentTypeId!=0)?"?, ":"") +
					((printingTypeId!=0)?"?, ?, ?, ?, ":"") +
					"?, ?, ?, ?)"
			);
			int param = 0;
			pstmt.setInt(++param,clientId);
			pstmt.setInt(++param,((isBranch)?branchId:kioskId));
			if (developmentTypeId!=0) {
				pstmt.setInt(++param,developmentTypeId);
			}
			if (printingTypeId!=0) {
				pstmt.setInt(++param,printingTypeId);
				pstmt.setInt(++param,framesId);
				pstmt.setInt(++param,formatTypeId);
				pstmt.setInt(++param,paperTypesId);
			}
			
			pstmt.setBoolean(++param,isPressing);
			pstmt.setTimestamp(++param,new Timestamp(orderDate.getTime()));
			pstmt.setBoolean(++param,isDone);
			pstmt.setBoolean(++param,isPaid);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addService(int clientId, int branchId, int serviceTypeId, 
			int photoEquipmentId, java.util.Date serviceDate, java.util.Date expDate, 
			boolean isDone, boolean isPaid) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO services (client_id, branch_id, service_type_id, " +
					((photoEquipmentId!=0)?"equipment_id, ":"") +
					"service_date, exp_date, done, paid) " +
					"VALUES (?, ?, ?, " +
					((photoEquipmentId!=0)?"?, ":"") +
					"?, ?, ?, ?)"
			);
			int param = 0;
			pstmt.setInt(++param,clientId);
			pstmt.setInt(++param,branchId);
			pstmt.setInt(++param,serviceTypeId);
			if (photoEquipmentId != 0) {
				pstmt.setInt(++param,photoEquipmentId);
			}
			pstmt.setTimestamp(++param,new Timestamp(serviceDate.getTime()));
			pstmt.setTimestamp(++param,new Timestamp(expDate.getTime()));
			pstmt.setBoolean(++param,isDone);
			pstmt.setBoolean(++param,isPaid);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addBranch(String name, String address, Boolean isMain, int cashiers, int photographers, int managers, int serviceStaff, int other) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO branches (name, address, main, cashiers, photographers, managers, service_staff,  other) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, name);
			pstmt.setString(2, address);
			pstmt.setBoolean(3, isMain);
			pstmt.setInt(4, cashiers);
			pstmt.setInt(5, photographers);
			pstmt.setInt(6, managers);
			pstmt.setInt(7, serviceStaff);
			pstmt.setInt(8, other);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addKiosk(int branchId, String address, java.util.Date installDate, boolean isExpDateEnable, java.util.Date expDate, int workplaces) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO kiosks (parent_branch_id, address, install_date, workplaces" +
					((isExpDateEnable)?" ,exp_date":"") +
					") VALUES (?, ?, ?, ?" +
					((isExpDateEnable)?", ?":"") +
					")");
			int param = 0;
			pstmt.setInt(++param, branchId);
			pstmt.setString(++param, address);
			pstmt.setTimestamp(++param, new Timestamp(installDate.getTime()));
			pstmt.setInt(++param, workplaces);
			if (isExpDateEnable) {
				pstmt.setTimestamp(++param, new Timestamp(expDate.getTime()));
			}
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addFrames(int[] nums) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String q = "INSERT INTO frames (";
			for (int i = 0; i < nums.length-1; i++) {
				q += "count_" + (i+1) + ",";
			}
			q += "count_" + nums.length;
			q += ") VALUES (";
			for (int i = 0; i < nums.length-1; i++) {
				q += "?, ";
			}
			q += "?";
			q += ")";
			System.out.println(q);
			pstmt = con.prepareStatement(q);
			
			for (int i = 0; i < nums.length; i++) {
				pstmt.setInt(i+1, nums[i]);
			}
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addDiscount(int clientId, double discountRate, java.util.Date expDate) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("UPDATE discounts SET rate = ?, exp_date = ? WHERE client_id = ?");
			int param = 0;
			pstmt.setDouble(++param, discountRate);
			pstmt.setTimestamp(++param, new Timestamp(expDate.getTime()));
			pstmt.setInt(++param, clientId);
			pstmt.executeUpdate();
			int updated = pstmt.getUpdateCount();
			if (updated == 0) {
				pstmt = con.prepareStatement("INSERT INTO discounts (client_id, rate, exp_date) VALUES (?, ?, ?)");
				param = 0;
				pstmt.setInt(++param, clientId);
				pstmt.setDouble(++param, discountRate);
				pstmt.setTimestamp(++param, new Timestamp(expDate.getTime()));
				pstmt.execute();
			}
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addEquipment(int equipmentTypeId, String name, double cost, String producer) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO equipment (equipment_type_id, name, cost, producer) VALUES (?, ?, ?, ?)");
			int param = 0;
			pstmt.setInt(++param, equipmentTypeId);
			pstmt.setString(++param, name);
			pstmt.setDouble(++param, cost);
			pstmt.setString(++param, producer);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addEquipmentType(String name, boolean forSale) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO equipment_types (name, forSale) VALUES (?, ?)");
			int param = 0;
			pstmt.setString(++param, name);
			pstmt.setBoolean(++param, forSale);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addDistribution(int equipmentId, int branchId, int quantity, java.util.Date distributionDate) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO distributions (equipment_id, branch_id, quantity, distribution_date) VALUES (?, ?, ?, ?)");
			int param = 0;
			pstmt.setInt(++param, equipmentId);
			pstmt.setInt(++param, branchId);
			pstmt.setInt(++param, quantity);
			pstmt.setTimestamp(++param, new Timestamp(distributionDate.getTime()));
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addSale(int branchId, java.util.Date saleDate, List<SoldEquipmentItem> seList) throws SQLException {
		PreparedStatement pstmt = null;
		if (seList.size() == 0) 
			return;
		try {
			pstmt = con.prepareStatement("INSERT INTO sales (branch_id, sale_date) VALUES (?, ?)");
			pstmt.setInt(1, branchId);
			pstmt.setTimestamp(2, new Timestamp(saleDate.getTime()));
			pstmt.execute();
			
			pstmt = con.prepareStatement("SELECT sale_id FROM sales WHERE branch_id = ? AND sale_date = ?");
			pstmt.setInt(1, branchId);
			pstmt.setTimestamp(2,new Timestamp(saleDate.getTime()));
			pstmt.execute();
			rs = pstmt.getResultSet();
			int sale_id = 0;
			if (rs != null) {
				if (rs.first()) {
					sale_id = rs.getInt(1);
				}
			}
			System.out.println(sale_id);
			for (int i = 0; i < seList.size(); i++) {
				pstmt = con.prepareStatement("INSERT INTO sold_equipment (sale_id, equipment_id, quantity) VALUES (?, ?, ?)");
				pstmt.setInt(1, sale_id);
				pstmt.setInt(2, seList.get(i).getId());
				pstmt.setInt(3, seList.get(i).getQuantity());
				pstmt.execute();
			}
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addSupply(int deliverableEquipmentId, int quantity, java.util.Date supplyDate) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO supplies (deliverable_equipment_id, quantity, supply_date) VALUES (?, ?, ?)");
			int param = 0;
			pstmt.setInt(++param, deliverableEquipmentId);
			pstmt.setInt(++param, quantity);
			pstmt.setTimestamp(++param, new Timestamp(supplyDate.getTime()));
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public void addDeliverableEquipment(int equipmentId, int supplierId, double quotation) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("INSERT INTO deliverable_equipment (equipment_id, supplier_id, cost) VALUES (?, ?, ?, ?)");
			int param = 0;
			pstmt.setInt(++param, equipmentId);
			pstmt.setInt(++param, supplierId);
			pstmt.setDouble(++param, quotation);
			pstmt.execute();
		}
		finally {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
	}
	
	public ResponseTable calculatePhotosNumber(int method,
			boolean isBranchEnabled, int branchId, String branchName, String branchAddress,
			boolean isKiosksEnabled, int kioskId, 
			int isPressing, 
			int orderDateSign, java.util.Date orderDate1, java.util.Date orderDate2) throws SQLException {
		ResponseTable table = null;
		rs = null;
		PreparedStatement pstmt = null;
		try {
			String odSign = "=";
			if (orderDateSign != 0) {
				switch (orderDateSign) {
				case 1: odSign = "="; break;
				case 2: odSign = "<>"; break;
				case 3: odSign = "<"; break;
				case 4: odSign = "<="; break;
				case 5: odSign = ">"; break;
				case 6: odSign = ">="; break;
				case 7: odSign = "BETWEEN ? AND"; break;
				case 8: odSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String branchQueryString = "";
			if (isBranchEnabled) {
				if ( (branchName != "")||(branchAddress != "") ) {
					branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
					branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
				} else {
					branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
				}
			}
			String kioskQueryString = "";
			if (isKiosksEnabled) {
				if (kioskId != 0) {
					kioskQueryString += "AND orders.kiosk_id = ? ";
				}
			}
			String orderTypeQueryString = "";
			orderTypeQueryString += ((isPressing!=-1)?"AND orders.pressing = ? ":"");
			orderTypeQueryString += ((orderDateSign!=0)?"AND orders.order_date "+odSign+" ? ":"");
			
			switch (method) {
			case 0:
				pstmt = con.prepareStatement("SELECT \"Branch\" AS \"Branch / Kiosk\", " +
						"branches.name AS \"Branch Name / Kiosk Address\", " +
						"branches.address AS \"Branch Address / Parent Branch\", " +
						"SUM(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) AS \"Printed Photos Count\" " +
						"FROM orders, branches, frames " +
						"WHERE orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.frames_id IS NOT NULL " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.done = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"GROUP BY branches.branch_id " +
						"UNION " +
						"SELECT \"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"SUM(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) " +
						"FROM orders, branches, kiosks, frames " +
						"WHERE orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND kiosks.parent_branch_id = branches.branch_id " +
						"AND orders.frames_id IS NOT NULL " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.done = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"GROUP BY kiosks.kiosk_id " +
						"");
				break;
			case 1:
				pstmt = con.prepareStatement("SELECT \"Branch\" AS \"Branch / Kiosk\", " +
						"branches.name AS \"Branch Name / Kiosk Address\", " +
						"branches.address AS \"Branch Address / Parent Branch\", " +
						"COUNT(orders.order_id) AS \"Developed Film Count\" " +
						"FROM orders, branches " +
						"WHERE orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.development_type_id IS NOT NULL " +
						"AND orders.done = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"GROUP BY orders.branch_id " +
						"UNION " +
						"SELECT \"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"COUNT(orders.order_id) " +
						"FROM orders, branches, kiosks " +
						"WHERE orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND kiosks.parent_branch_id = branches.branch_id " +
						"AND orders.development_type_id IS NOT NULL " +
						"AND orders.done = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"GROUP BY kiosks.kiosk_id " +
						"");
				break;
			}
			
			int param = 0;
			for (int i = 0; i < 2; i++) {
				if (isBranchEnabled) {
					if ( (branchName != "")||(branchAddress != "") ) {
						if (branchName != "") {
							pstmt.setString(++param, branchName);
						}
						if (branchAddress != "") {
							pstmt.setString(++param, branchAddress);
						}
					} else {
						if (branchId != 0) {
							pstmt.setInt(++param, branchId);
						}
					}
				}
				if (isKiosksEnabled) {
					if (kioskId != 0) {
						pstmt.setInt(++param, kioskId);
					}
				}
				if (isPressing != -1) {
					pstmt.setInt(++param, isPressing);
				}
				if (orderDateSign != 0) {
					pstmt.setTimestamp(++param,new Timestamp(orderDate1.getTime()));
					if (orderDateSign >= 7) {
						pstmt.setTimestamp(++param,new Timestamp(orderDate2.getTime()));
					}
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		
		return table;
	}
	
	public ResponseTable calculateOrdersReceipts(boolean isBranchEnabled, int branchId, String branchName, String branchAddress,
			boolean isKiosksEnabled, int kioskId, 
			int type, int isPressing, 
			int orderDateSign, java.util.Date orderDate1, java.util.Date orderDate2) throws SQLException {
		ResponseTable table = null;
		rs = null;
		PreparedStatement pstmt = null;
		try {
			String odSign = "=";
			if (orderDateSign != 0) {
				switch (orderDateSign) {
				case 1: odSign = "="; break;
				case 2: odSign = "<>"; break;
				case 3: odSign = "<"; break;
				case 4: odSign = "<="; break;
				case 5: odSign = ">"; break;
				case 6: odSign = ">="; break;
				case 7: odSign = "BETWEEN ? AND"; break;
				case 8: odSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String branchQueryString = "";
			if (isBranchEnabled) {
				if ( (branchName != "")||(branchAddress != "") ) {
					branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
					branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
				} else {
					branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
				}
			}
			String kioskQueryString = "";
			if (isKiosksEnabled) {
				if (kioskId != 0) {
					kioskQueryString += "AND orders.kiosk_id = ? ";
				}
			}
			String orderTypeQueryString = "";
			orderTypeQueryString += ((isPressing!=-1)?"AND orders.pressing = ? ":"");
			orderTypeQueryString += ((orderDateSign!=0)?"AND orders.order_date "+odSign+" ? ":"");
			
			String queryPart1 = "SELECT a.a AS \"Branch / Kiosk\", " +
						"a.b AS \"Branch Name / Kiosk Address\", " +
						"a.c AS \"Branch Address / Parent Branch\", " +
						"\"Development\" AS \"Order Type\", " +
						"a.d AS \"Is Pressing\", " +
						"SUM(a.e) AS \"Orders Receipts\" " +
					"FROM ( " +
						"SELECT branches.branch_id AS i, " +
						"\"Branch\" AS a, " +
						"branches.name AS b, " +
						"branches.address AS c, " +
						"orders.pressing AS d, " +
						"development_types.cost*(orders.pressing+1)*(1-discounts.rate) AS e " +
						"FROM orders, clients, branches, development_types, discounts " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.printing_type_id IS NULL " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND discounts.client_id = orders.client_id " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT branches.branch_id, " +
						"\"Branch\", " +
						"branches.name, " +
						"branches.address, " +
						"orders.pressing, " +
						"development_types.cost*(orders.pressing+1) " +
						"FROM orders, clients, branches, development_types " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.printing_type_id IS NULL " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT kiosks.kiosk_id, " +
						"\"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"orders.pressing, " +
						"development_types.cost*(orders.pressing+1)*(1-discounts.rate) " +
						"FROM orders, clients, branches, kiosks, development_types, discounts " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND branches.branch_id = kiosks.parent_branch_id " +
						"AND orders.printing_type_id IS NULL " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND discounts.client_id = orders.client_id " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT kiosks.kiosk_id, " +
						"\"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"orders.pressing, " +
						"development_types.cost*(orders.pressing+1) " +
						"FROM orders, clients, branches, kiosks, development_types " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND branches.branch_id = kiosks.parent_branch_id " +
						"AND orders.printing_type_id IS NULL " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
					") AS a " +
					"GROUP BY a.i, a.a, a.d ";
			String queryPart2 = "SELECT a.a AS \"Branch / Kiosk\", " +
						"a.b AS \"Branch Name / Kiosk Address\", " +
						"a.c AS \"Branch Address / Parent Branch\", " +
						"\"Printing\" AS \"Order Type\", " +
						"a.d AS \"Is Pressing\", " +
						"SUM(a.e) AS \"Orders Receipts\" " +
					"FROM ( " +
						"SELECT branches.branch_id AS i, " +
						"\"Branch\" AS a, " +
						"branches.name AS b, " +
						"branches.address AS c, " +
						"orders.pressing AS d, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate) AS e " +
						"FROM orders, clients, branches, frames, format_types, printing_types, discounts " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id IS NULL " +
						"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND discounts.client_id = orders.client_id " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT branches.branch_id, " +
						"\"Branch\", " +
						"branches.name, " +
						"branches.address, " +
						"orders.pressing, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1) " +
						"FROM orders, clients, branches, frames, format_types, printing_types " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id IS NULL " +
						"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT kiosks.kiosk_id, " +
						"\"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"orders.pressing, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate) " +
						"FROM orders, clients, branches, kiosks, frames, format_types, printing_types, discounts " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND branches.branch_id = kiosks.parent_branch_id " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id IS NULL " +
						"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND discounts.client_id = orders.client_id " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT kiosks.kiosk_id, " +
						"\"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"orders.pressing, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1) " +
						"FROM orders, clients, branches, kiosks, frames, format_types, printing_types " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND branches.branch_id = kiosks.parent_branch_id " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id IS NULL " +
						"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
					") AS a " +
					"GROUP BY a.i, a.a, a.d ";
			String queryPart3 = "SELECT a.a AS \"Branch / Kiosk\", " +
						"a.b AS \"Branch Name / Kiosk Address\", " +
						"a.c AS \"Branch Address / Parent Branch\", " +
						"\"Printing And Development\" AS \"Order Type\", " +
						"a.d AS \"Is Pressing\", " +
						"SUM(a.e) AS \"Orders Receipts\" " +
					"FROM ( " +
						"SELECT branches.branch_id AS i, " +
						"\"Branch\" AS a, " +
						"branches.name AS b, " +
						"branches.address AS c, " +
						"orders.pressing AS d, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate) AS e " +
						"FROM orders, clients, branches, frames, format_types, printing_types, development_types, discounts " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND discounts.client_id = orders.client_id " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT branches.branch_id, " +
						"\"Branch\", " +
						"branches.name, " +
						"branches.address, " +
						"orders.pressing, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1) " +
						"FROM orders, clients, branches, frames, format_types, printing_types, development_types " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id = branches.branch_id " +
						"AND orders.kiosk_id IS NULL " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION "+
						"SELECT kiosks.kiosk_id, " +
						"\"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"orders.pressing, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate) " +
						"FROM orders, clients, branches, kiosks, frames, format_types, printing_types, development_types, discounts " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND branches.branch_id = kiosks.parent_branch_id " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND discounts.client_id = orders.client_id " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
						"UNION " +
						"SELECT kiosks.kiosk_id, " +
						"\"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"orders.pressing, " +
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1) " +
						"FROM orders, clients, branches, kiosks, frames, format_types, printing_types, development_types " +
						"WHERE orders.client_id = clients.client_id " +
						"AND orders.branch_id IS NULL " +
						"AND orders.kiosk_id = kiosks.kiosk_id " +
						"AND branches.branch_id = kiosks.parent_branch_id " +
						"AND orders.frames_id = frames.frames_id " +
						"AND orders.format_type_id = format_types.format_type_id " +
						"AND orders.printing_type_id = printing_types.printing_type_id " +
						"AND orders.development_type_id = development_types.development_type_id " +
						"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
						"AND orders.paid = 1 " +
						branchQueryString +
						kioskQueryString +
						orderTypeQueryString +
					") AS a " +
					"GROUP BY a.i, a.a, a.d ";
			
			
			switch (type) {
			case 0:
				pstmt = con.prepareStatement(queryPart1 + "UNION " + queryPart2 + "UNION " + queryPart3);
				break;
			case 1:
				pstmt = con.prepareStatement(queryPart1);
				break;
			case 2:
				pstmt = con.prepareStatement(queryPart2);
				break;
			case 3:
				pstmt = con.prepareStatement(queryPart3);
				break;
			}
			
			int param = 0;
			int c = ((type==0)?12:4);
			for (int i = 0; i < c; i++) {
				if (isBranchEnabled) {
					if ( (branchName != "")||(branchAddress != "") ) {
						if (branchName != "") {
							pstmt.setString(++param, branchName);
						}
						if (branchAddress != "") {
							pstmt.setString(++param, branchAddress);
						}
					} else {
						if (branchId != 0) {
							pstmt.setInt(++param, branchId);
						}
					}
				}
				if (isKiosksEnabled) {
					if (kioskId != 0) {
						pstmt.setInt(++param, kioskId);
					}
				}
				if (isPressing != -1) {
					pstmt.setInt(++param, isPressing);
				}
				if (orderDateSign != 0) {
					pstmt.setTimestamp(++param,new Timestamp(orderDate1.getTime()));
					if (orderDateSign >= 7) {
						pstmt.setTimestamp(++param,new Timestamp(orderDate2.getTime()));
					}
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable calculateSalesReceipts(int branchId, String branchName, String branchAddress,
			int saleDateSign, java.util.Date saleDate1, java.util.Date saleDate2) throws SQLException {
		ResponseTable table = null;
		rs = null;
		PreparedStatement pstmt = null;
		try {
			String odSign = "=";
			if (saleDateSign != 0) {
				switch (saleDateSign) {
				case 1: odSign = "="; break;
				case 2: odSign = "<>"; break;
				case 3: odSign = "<"; break;
				case 4: odSign = "<="; break;
				case 5: odSign = ">"; break;
				case 6: odSign = ">="; break;
				case 7: odSign = "BETWEEN ? AND"; break;
				case 8: odSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			String saleDateQueryString = "";
			saleDateQueryString += ((saleDateSign!=0)?"AND sales.sale_date "+odSign+" ? ":"");
			
			pstmt = con.prepareStatement("SELECT branches.name AS \"Branch Name\", " +
					"branches.address AS \"Branch Address\", " +
					"SUM(equipment.cost*sold_equipment.quantity) AS \"Sales Receipts\" " +
					"FROM branches, equipment, sales, sold_equipment " +
					"WHERE sales.branch_id = branches.branch_id " +
					"AND sold_equipment.sale_id = sales.sale_id " +
					"AND sold_equipment.equipment_id = equipment.equipment_id " +
					branchQueryString +
					saleDateQueryString +
					"GROUP BY branches.branch_id " +
					"ORDER BY `Branch Name` " +
					"");
			
			int param = 0;
			if ( (branchName != "")||(branchAddress != "") ) {
				if (branchName != "") {
					pstmt.setString(++param, branchName);
				}
				if (branchAddress != "") {
					pstmt.setString(++param, branchAddress);
				}
			} else {
				if (branchId != 0) {
					pstmt.setInt(++param, branchId);
				}
			}
			if (saleDateSign != 0) {
				pstmt.setTimestamp(++param,new Timestamp(saleDate1.getTime()));
				if (saleDateSign >= 7) {
					pstmt.setTimestamp(++param,new Timestamp(saleDate2.getTime()));
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showClients(boolean isAll, int clientId, String clientFirstname, String clientLastname, String clientAddress,
			int branchId, String branchName, String branchAddress,
			int photosNumberSign, int photosNumber1, int photosNumber2,
			int discount, int professional) throws SQLException {
		ResponseTable table = null;
		rs = null;
		PreparedStatement pstmt = null;
		try {
			String pnSign = "=";
			if (photosNumberSign != 0) {
				switch (photosNumberSign) {
				case 1: pnSign = "="; break;
				case 2: pnSign = "<>"; break;
				case 3: pnSign = "<"; break;
				case 4: pnSign = "<="; break;
				case 5: pnSign = ">"; break;
				case 6: pnSign = ">="; break;
				case 7: pnSign = "BETWEEN ? AND"; break;
				case 8: pnSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String clientsQueryString = "";
			if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
				clientsQueryString += ((clientFirstname!="")?"AND clients.firstname LIKE ? ":"");
				clientsQueryString += ((clientLastname!="")?"AND clients.lastname LIKE ? ":"");
				clientsQueryString += ((clientAddress!="")?"AND clients.address LIKE ? ":"");
			} else {
				clientsQueryString = ((clientId!=0)?"AND clients.client_id = ? ":"");
			}
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			String clientTypeQueryString = "";
			clientTypeQueryString += ((professional!=-1)?"AND clients.professional = ? ":"");
			
			if (isAll) {
				pstmt = con.prepareStatement("(SELECT clients.client_id AS \"Client ID\", " +
							"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\" , " +
							"clients.address AS \"Client's Address\", " +
							"clients.professional AS \"Is Professional\", " +
							"discounts.rate AS \"Discount Rate\", " +
							"discounts.exp_date AS \"Discount Expiration Date\" " +
							"FROM clients, discounts " +
							"WHERE EXISTS(SELECT * FROM discounts WHERE discounts.client_id = clients.client_id AND discounts.exp_date > NOW()) " +
							"AND discounts.client_id = clients.client_id " +
							clientsQueryString +
							clientTypeQueryString +
						")UNION (" +
						"SELECT clients.client_id, " +
							"CONCAT(clients.lastname, \" \", clients.firstname), " +
							"clients.address, " +
							"clients.professional, " +
							"0, " +
							"NULL " +
							"FROM clients " +
							"WHERE NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = clients.client_id AND discounts.exp_date > NOW()) " +
							clientsQueryString +
							clientTypeQueryString +
							")ORDER BY `Client's Full Name`");
				int param = 0;
				for (int i = 0; i < 2; i++) {
					if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
						if (clientFirstname != "") {
							pstmt.setString(++param, clientFirstname);
						}
						if (clientLastname != "") {
							pstmt.setString(++param, clientLastname);
						}
						if (clientAddress != "") {
							pstmt.setString(++param, clientAddress);
						}
					} else {
						if (clientId != 0) {
							pstmt.setInt(++param, clientId);
						}
					}
					if (professional != -1) {
						pstmt.setInt(++param, professional);
					}
				}
				pstmt.execute();
			} else {
				pstmt = con.prepareStatement("SELECT clients.client_id AS \"Client ID\", CONCAT(clients.firstname, \" \", clients.lastname) AS \"Client's Full Name\" , " +
							"clients.address AS \"Client's Address\" , " +
							"CONCAT(branches.name,\" (\", branches.address, \")\") AS \"Branch\", " +
							"(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) AS \"Number Of Photos\", " +
							"discounts.rate AS \"Discount Rate\", " +
							"discounts.exp_date AS \"Discount Expiration Date\", " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, discounts, frames " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id = branches.branch_id " +
							"AND orders.kiosk_id IS NULL " +
							"AND orders.frames_id = frames.frames_id " +
							"AND orders.done = 0 " +
							"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							"AND discounts.client_id = orders.client_id " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND (frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) "+pnSign+" ? ":"") + 
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"),	" +
							"(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36), " +
							"0, " +
							"NULL, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, discounts, frames " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id = branches.branch_id " +
							"AND orders.kiosk_id IS NULL " +
							"AND orders.frames_id = frames.frames_id " +
							"AND orders.done = 0 " +
							"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND (frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) "+pnSign+" ? ":"") +
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
							"(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36), " +
							"discounts.rate, " +
							"discounts.exp_date, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, kiosks, discounts, frames " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id IS NULL " +
							"AND orders.kiosk_id = kiosks.kiosk_id " +
							"AND branches.branch_id = kiosks.parent_branch_id " +
							"AND orders.frames_id = frames.frames_id " +
							"AND orders.done = 0 " +
							"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							"AND discounts.client_id = orders.client_id " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND (frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) "+pnSign+" ? ":"") +
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
							"(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36), " +
							"0, " +
							"NULL, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, kiosks, discounts, frames " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id IS NULL " +
							"AND orders.kiosk_id = kiosks.kiosk_id " +
							"AND branches.branch_id = kiosks.parent_branch_id " +
							"AND orders.frames_id = frames.frames_id " +
							"AND orders.done = 0 " +
							"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND (frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) "+pnSign+" ? ":"") +
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
							"0, " +
							"discounts.rate, " +
							"discounts.exp_date, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, discounts " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id = branches.branch_id " +
							"AND orders.kiosk_id IS NULL " +
							"AND orders.frames_id IS NULL " +
							"AND orders.done = 0 " +
							"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							"AND discounts.client_id = orders.client_id " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND 1=2 ":"") +
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
							"0, " +
							"0, " +
							"NULL, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, discounts " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id = branches.branch_id " +
							"AND orders.kiosk_id IS NULL " +
							"AND orders.frames_id IS NULL " +
							"AND orders.done = 0 " +
							"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND 1=2 ":"") +
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
							"0, " +
							"discounts.rate, " +
							"discounts.exp_date, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, kiosks, discounts " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id IS NULL " +
							"AND orders.kiosk_id = kiosks.kiosk_id " +
							"AND branches.branch_id = kiosks.parent_branch_id " +
							"AND orders.frames_id IS NULL " +
							"AND orders.done = 0 " +
							"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							"AND discounts.client_id = orders.client_id " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND 1=2 ":"") +
							clientTypeQueryString +
						"UNION " +
						"SELECT clients.client_id, " +
							"CONCAT(clients.firstname, \" \", clients.lastname), " +
							"clients.address, " +
							"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
							"0, " +
							"0, " +
							"NULL, " +
							"orders.order_date AS \"Order Date\" "+
							"FROM orders, clients, branches, kiosks, discounts " +
							"WHERE orders.client_id = clients.client_id " +
							"AND orders.branch_id IS NULL " +
							"AND orders.kiosk_id = kiosks.kiosk_id " +
							"AND branches.branch_id = kiosks.parent_branch_id " +
							"AND orders.frames_id IS NULL " +
							"AND orders.done = 0 " +
							"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
							clientsQueryString +
							branchQueryString +
							((photosNumberSign!=0)?"AND 1=2 ":"") +
							clientTypeQueryString +
							""
						);
				int param = 0;
				for (int i = 0; i < 8; i++) {
					if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
						if (clientFirstname != "") {
							pstmt.setString(++param, clientFirstname);
						}
						if (clientLastname != "") {
							pstmt.setString(++param, clientLastname);
						}
						if (clientAddress != "") {
							pstmt.setString(++param, clientAddress);
						}
					} else {
						if (clientId != 0) {
							pstmt.setInt(++param, clientId);
						}
					}
					if ( (branchName != "")||(branchAddress != "") ) {
						if (branchName != "") {
							pstmt.setString(++param, branchName);
						}
						if (branchAddress != "") {
							pstmt.setString(++param, branchAddress);
						}
					} else {
						if (branchId != 0) {
							pstmt.setInt(++param, branchId);
						}
					}
					if (i < 4) {
						if (photosNumberSign != 0) {
							pstmt.setInt(++param, photosNumber1);
							if (photosNumberSign >= 7) {
								pstmt.setInt(++param, photosNumber2);
							}
						}
					}
					if (professional != -1) {
						pstmt.setInt(++param, professional);
					}
				}
				pstmt.execute();
			}
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	/* 
	 * INFO:
	 * % 	Matches any number of characters, even zero characters
	 * _ 	Matches exactly one character
	 */
	public ResponseTable showOrders(int clientId, String clientFirstname, String clientLastname, String clientAddress,
			boolean isBranchEnabled, int branchId, String branchName, String branchAddress,
			boolean isKiosksEnabled, int kioskId, 
			int developmentTypeId, int printingTypeId, int formatTypeId, int paperTypeId, 
			int isPressing, 
			int orderDateSign, java.util.Date orderDate1, java.util.Date orderDate2,
			int isDone, int isPaid) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String odSign = "=";
			if (orderDateSign != 0) {
				switch (orderDateSign) {
				case 1: odSign = "="; break;
				case 2: odSign = "<>"; break;
				case 3: odSign = "<"; break;
				case 4: odSign = "<="; break;
				case 5: odSign = ">"; break;
				case 6: odSign = ">="; break;
				case 7: odSign = "BETWEEN ? AND"; break;
				case 8: odSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String clientsQueryString = "";
			if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
				clientsQueryString += ((clientFirstname!="")?"AND clients.firstname LIKE ? ":"");
				clientsQueryString += ((clientLastname!="")?"AND clients.lastname LIKE ? ":"");
				clientsQueryString += ((clientAddress!="")?"AND clients.address LIKE ? ":"");
			} else {
				clientsQueryString = ((clientId!=0)?"AND orders.client_id = ? ":"");
			}
			String branchQueryString = "";
			if (isBranchEnabled) {
				if ( (branchName != "")||(branchAddress != "") ) {
					branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
					branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
				} else {
					branchQueryString = ((branchId!=0)?"AND orders.branch_id = ? ":"");
				}
			}
			String kioskQueryString = "";
			if (isKiosksEnabled) {
				if (kioskId != 0) {
					kioskQueryString += "AND orders.kiosk_id = ? ";
				}
			}
			String orderTypeQueryString = "";
			orderTypeQueryString += ((developmentTypeId!=0)?"AND orders.development_type_id = ? ":"");
			orderTypeQueryString += ((printingTypeId!=0)?"AND orders.printing_type_id = ? ":"");
			orderTypeQueryString += ((formatTypeId!=0)?"AND orders.format_type_id = ? ":"");
			orderTypeQueryString += ((paperTypeId!=0)?"AND orders.paper_type_id = ? ":"");
			orderTypeQueryString += ((isPressing!=-1)?"AND orders.pressing = ? ":"");
			orderTypeQueryString += ((orderDateSign!=0)?"AND orders.order_date "+odSign+" ? ":"");
			orderTypeQueryString += ((isDone!=-1)?"AND orders.done = ? ":"");
			orderTypeQueryString += ((isPaid!=-1)?"AND orders.paid = ? ":"");
			
			
			pstmt = con.prepareStatement("(SELECT orders.order_id AS \"Order ID\", "+ 
						"CONCAT(development_types.name,\", \",printing_types.name) AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Branch / Kiosk Address\", "+
						"\"-\" AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, development_types, printing_types, frames, paper_types, format_types, discounts " +
					"WHERE orders.client_id = clients.client_id " +
					"AND branches.branch_id = orders.branch_id " +
					"AND orders.kiosk_id IS NULL " +
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					"AND discounts.client_id = orders.client_id " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+
						"development_types.name AS \"Order Type\", "+
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Branch / Kiosk Address\", "+
						"\"-\" AS \"Parent Branch\", "+
						"0 AS \"Number Of Photos\", "+
						"\"-\" AS \"Paper Type\", "+
						"\"-\" AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"development_types.cost*(orders.pressing+1)*(1-discounts.rate) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, development_types, discounts "+
					"WHERE clients.client_id = orders.client_id "+
					"AND branches.branch_id = orders.branch_id "+
					"AND orders.kiosk_id IS NULL " +
					"AND orders.printing_type_id IS NULL "+
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					"AND discounts.client_id = orders.client_id " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+ 
						"printing_types.name AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Branch / Kiosk Address\", "+
						"\"-\" AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, printing_types, frames, paper_types, format_types, discounts " +
					"WHERE orders.client_id = clients.client_id " +
					"AND branches.branch_id = orders.branch_id " +
					"AND orders.kiosk_id IS NULL " +
					"AND orders.development_type_id IS NULL " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					"AND discounts.client_id = orders.client_id " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", " + 
						"CONCAT(development_types.name,\", \",printing_types.name) AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Branch / Kiosk Address\", "+
						"\"-\" AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, development_types, printing_types, frames, paper_types, format_types " +
					"WHERE orders.client_id = clients.client_id " +
					"AND branches.branch_id = orders.branch_id " +
					"AND orders.kiosk_id IS NULL " +
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+
						"development_types.name AS \"Order Type\", "+
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Branch / Kiosk Address\", "+
						"\"-\" AS \"Parent Branch\", "+
						"0 AS \"Number Of Photos\", "+
						"\"-\" AS \"Paper Type\", "+
						"\"-\" AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"development_types.cost*(orders.pressing+1) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, development_types "+
					"WHERE clients.client_id = orders.client_id "+
					"AND branches.branch_id = orders.branch_id "+
					"AND orders.kiosk_id IS NULL " +
					"AND orders.printing_type_id IS NULL "+
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+ 
						"printing_types.name AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Branch / Kiosk Address\", "+
						"\"-\" AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, printing_types, frames, paper_types, format_types " +
					"WHERE orders.client_id = clients.client_id " +
					"AND branches.branch_id = orders.branch_id " +
					"AND orders.kiosk_id IS NULL " +
					"AND orders.development_type_id IS NULL " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" + 
					
					"SELECT orders.order_id AS \"Order ID\", "+ 
						"CONCAT(development_types.name,\", \",printing_types.name) AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(kiosks.address) AS \"Branch / Kiosk Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1)*(1-discounts.rate) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, kiosks, development_types, printing_types, frames, paper_types, format_types, discounts " +
					"WHERE orders.client_id = clients.client_id " +
					"AND orders.branch_id IS NULL "+
					"AND orders.kiosk_id = kiosks.kiosk_id "+
					"AND branches.branch_id = kiosks.parent_branch_id "+
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					"AND discounts.client_id = orders.client_id " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+
						"development_types.name AS \"Order Type\", "+
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(kiosks.address) AS \"Branch / Kiosk Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Parent Branch\", "+
						"0 AS \"Number Of Photos\", "+
						"\"-\" AS \"Paper Type\", "+
						"\"-\" AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"development_types.cost*(orders.pressing+1)*(1-discounts.rate) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					
					"FROM orders, clients, branches, kiosks, development_types, discounts "+
					"WHERE clients.client_id = orders.client_id "+
					"AND orders.branch_id IS NULL "+
					"AND orders.kiosk_id = kiosks.kiosk_id "+
					"AND branches.branch_id = kiosks.parent_branch_id "+
					"AND orders.printing_type_id IS NULL "+
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					"AND discounts.client_id = orders.client_id " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+ 
						"printing_types.name AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(kiosks.address) AS \"Branch / Kiosk Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1)*(1-discounts.rate) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, kiosks, printing_types, frames, paper_types, format_types, discounts " +
					"WHERE orders.client_id = clients.client_id " +
					"AND orders.branch_id IS NULL "+
					"AND orders.kiosk_id = kiosks.kiosk_id "+
					"AND branches.branch_id = kiosks.parent_branch_id "+
					"AND orders.development_type_id IS NULL " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					"AND discounts.client_id = orders.client_id " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", " + 
						"CONCAT(development_types.name,\", \",printing_types.name) AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(kiosks.address) AS \"Branch / Kiosk Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult + development_types.cost)*(orders.pressing+1) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, kiosks, development_types, printing_types, frames, paper_types, format_types " +
					"WHERE orders.client_id = clients.client_id " +
					"AND orders.branch_id IS NULL "+
					"AND orders.kiosk_id = kiosks.kiosk_id "+
					"AND branches.branch_id = kiosks.parent_branch_id "+
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+
						"development_types.name AS \"Order Type\", "+
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(kiosks.address) AS \"Branch / Kiosk Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Parent Branch\", "+
						"0 AS \"Number Of Photos\", "+
						"\"-\" AS \"Paper Type\", "+
						"\"-\" AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"development_types.cost*(orders.pressing+1) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					
					"FROM orders, clients, branches, kiosks, development_types "+
					"WHERE clients.client_id = orders.client_id "+
					"AND orders.branch_id IS NULL "+
					"AND orders.kiosk_id = kiosks.kiosk_id "+
					"AND branches.branch_id = kiosks.parent_branch_id "+
					"AND orders.printing_type_id IS NULL "+
					"AND development_types.development_type_id = orders.development_type_id " +
					"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					")UNION (" +
					"SELECT orders.order_id AS \"Order ID\", "+ 
						"printing_types.name AS \"Order Type\", " +
						"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
						"clients.address AS \"Client's Address\", "+
						"CONCAT(kiosks.address) AS \"Branch / Kiosk Address\", "+
						"CONCAT(branches.name,\" (\" ,branches.address, \")\") AS \"Parent Branch\", "+
						"frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36 AS \"Number Of Photos\", " +
						"paper_types.name AS \"Paper Type\", "+
						"format_types.name AS \"Format\", "+
						"orders.pressing AS \"Is Pressing\", "+
						"orders.order_date AS \"Booking Date\", "+
						"((frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36)*format_types.cost*printing_types.cost_mult)*(orders.pressing+1) AS \"Price\", "+
						"orders.done AS \"Is Done\", "+
						"orders.paid AS \"Is Paid\" "+
					"FROM orders, clients, branches, kiosks, printing_types, frames, paper_types, format_types " +
					"WHERE orders.client_id = clients.client_id " +
					"AND orders.branch_id IS NULL "+
					"AND orders.kiosk_id = kiosks.kiosk_id "+
					"AND branches.branch_id = kiosks.parent_branch_id "+
					"AND orders.development_type_id IS NULL " +
					"AND printing_types.printing_type_id = orders.printing_type_id " +
					"AND frames.frames_id = orders.frames_id " +
					"AND format_types.format_type_id = orders.format_type_id " +
					"AND paper_types.paper_type_id = orders.paper_type_id " + 
					"AND NOT EXISTS(SELECT * FROM discounts WHERE discounts.client_id = orders.client_id AND discounts.exp_date > NOW()) " +
					clientsQueryString +
					branchQueryString +
					kioskQueryString +
					orderTypeQueryString +
					") ORDER BY `Booking Date` "
					);
			int param = 0;
			for (int i = 0; i < 12; i++) {
				if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
					if (clientFirstname != "") {
						pstmt.setString(++param, clientFirstname);
					}
					if (clientLastname != "") {
						pstmt.setString(++param, clientLastname);
					}
					if (clientAddress != "") {
						pstmt.setString(++param, clientAddress);
					}
				} else {
					if (clientId != 0) {
						pstmt.setInt(++param, clientId);
					}
				}
				if (isBranchEnabled) {
					if ( (branchName != "")||(branchAddress != "") ) {
						if (branchName != "") {
							pstmt.setString(++param, branchName);
						}
						if (branchAddress != "") {
							pstmt.setString(++param, branchAddress);
						}
					} else {
						if (branchId != 0) {
							pstmt.setInt(++param, branchId);
						}
					}
				}
				if (isKiosksEnabled) {
					if (kioskId != 0) {
						pstmt.setInt(++param, kioskId);
					}
				}
				if (developmentTypeId != 0) {
					pstmt.setInt(++param, developmentTypeId);
				}
				if (printingTypeId != 0) {
					pstmt.setInt(++param, printingTypeId);
				}
				if (formatTypeId != 0) {
					pstmt.setInt(++param, formatTypeId);
				}
				if (paperTypeId != 0) {
					pstmt.setInt(++param, paperTypeId);
				}
				if (isPressing != -1) {
					pstmt.setInt(++param, isPressing);
				}
				if (orderDateSign != 0) {
					pstmt.setTimestamp(++param,new Timestamp(orderDate1.getTime()));
					if (orderDateSign >= 7) {
						pstmt.setTimestamp(++param,new Timestamp(orderDate2.getTime()));
					}
				}
				if (isDone != -1) {
					pstmt.setInt(++param, isDone);
				}
				if (isPaid != -1) {
					pstmt.setInt(++param, isPaid);
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showServices(int clientId, String clientFirstname, String clientLastname, String clientAddress,
			int branchId, String branchName, String branchAddress,
			int serviceTypeId, int equipmentId, 
			int serviceDateSign, java.util.Date serviceDate1, java.util.Date serviceDate2,
			int expDateSign, java.util.Date expDate1, java.util.Date expDate2,
			int isDone, int isPaid) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String servSign = "=";
			if (serviceDateSign != 0) {
				switch (serviceDateSign) {
				case 1: servSign = "="; break;
				case 2: servSign = "<>"; break;
				case 3: servSign = "<"; break;
				case 4: servSign = "<="; break;
				case 5: servSign = ">"; break;
				case 6: servSign = ">="; break;
				case 7: servSign = "BETWEEN ? AND"; break;
				case 8: servSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String expSign = "=";
			if (expDateSign != 0) {
				switch (expDateSign) {
				case 1: expSign = "="; break;
				case 2: expSign = "<>"; break;
				case 3: expSign = "<"; break;
				case 4: expSign = "<="; break;
				case 5: expSign = ">"; break;
				case 6: expSign = ">="; break;
				case 7: expSign = "BETWEEN ? AND"; break;
				case 8: expSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String clientsQueryString = "";
			if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
				clientsQueryString += ((clientFirstname!="")?"AND clients.firstname LIKE ? ":"");
				clientsQueryString += ((clientLastname!="")?"AND clients.lastname LIKE ? ":"");
				clientsQueryString += ((clientAddress!="")?"AND clients.address LIKE ? ":"");
			} else {
				clientsQueryString = ((clientId!=0)?"AND services.client_id = ? ":"");
			}
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND services.branch_id = ? ":"");
			}
			String serviceTypeQueryString = "";
			serviceTypeQueryString += ((serviceTypeId!=0)?"AND services.service_type_id = ? ":"");
			serviceTypeQueryString += ((equipmentId!=0)?"AND services.equipment_id = ? ":"");
			serviceTypeQueryString += ((serviceDateSign!=0)?("AND services.service_date "+servSign+" ? "):"");
			 serviceTypeQueryString += ((expDateSign!=0)?("AND services.exp_date "+expSign+" ? "):""); 
			 serviceTypeQueryString += ((isDone!=-1)?"AND services.done = ? ":"");
			 serviceTypeQueryString += ((isPaid!=-1)?"AND services.paid = ? ":"");
			
			pstmt = con.prepareStatement("(SELECT service_id AS \"Service ID\", " +
					"service_types.name AS \"Service Type\", " +
					"CONCAT(clients.lastname, \" \", clients.firstname) AS \"Client's Full Name\", "+
					"clients.address AS \"Client's Address\" , " +
					"CONCAT(branches.name,\" (\", branches.address, \")\") AS \"Branch\" , " +
					"equipment.name AS \"Equipment\", " +
					"services.service_date AS \"Booking Date\", " +
					"services.exp_date AS \"Expiration Date\", " +
					"services.done AS \"Is Done\", " +
					"services.paid AS \"Is Paid\" " +
					"FROM services, clients, branches, service_types, equipment " +
					"WHERE services.client_id = clients.client_id " +
					"AND services.branch_id = branches.branch_id " +
					"AND services.service_type_id = service_types.service_type_id " +
					"AND services.equipment_id = equipment.equipment_id " +
					clientsQueryString +
					branchQueryString +
					serviceTypeQueryString +
					")UNION( " +
					"SELECT service_id, " +
					"service_types.name, " +
					"CONCAT(clients.lastname, \" \", clients.firstname), " +
					"clients.address, " +
					"CONCAT(branches.name,\" (\", branches.address, \")\"), " +
					"\"-\", " +
					"services.service_date, " +
					"services.exp_date, " +
					"services.done, " +
					"services.paid " +
					"FROM services, clients, branches, service_types " +
					"WHERE services.client_id = clients.client_id " +
					"AND services.branch_id = branches.branch_id " +
					"AND services.service_type_id = service_types.service_type_id " +
					"AND services.equipment_id IS NULL " +
					clientsQueryString +
					branchQueryString +
					serviceTypeQueryString +
					") ORDER BY `Booking Date` "
					);
			int param = 0;
			for (int i = 0; i < 2; i++) {
				if ( (clientFirstname != "")||(clientLastname != "")||(clientAddress != "") ) {
					if (clientFirstname != "") {
						pstmt.setString(++param, clientFirstname);
					}
					if (clientLastname != "") {
						pstmt.setString(++param, clientLastname);
					}
					if (clientAddress != "") {
						pstmt.setString(++param, clientAddress);
					}
				} else {
					if (clientId != 0) {
						pstmt.setInt(++param, clientId);
					}
				}
				if ( (branchName != "")||(branchAddress != "") ) {
					if (branchName != "") {
						pstmt.setString(++param, branchName);
					}
					if (branchAddress != "") {
						pstmt.setString(++param, branchAddress);
					}
				} else {
					if (branchId != 0) {
						pstmt.setInt(++param, branchId);
					}
				}
				if (serviceTypeId != 0) {
					pstmt.setInt(++param, serviceTypeId);
				}
				if (equipmentId != 0) {
					pstmt.setInt(++param, equipmentId);
				}
				if (serviceDateSign != 0) {
					pstmt.setTimestamp(++param,new Timestamp(serviceDate1.getTime()));
					if (serviceDateSign >= 7) {
						pstmt.setTimestamp(++param,new Timestamp(serviceDate2.getTime()));
					}
				}
				if (expDateSign != 0) {
					pstmt.setTimestamp(++param,new Timestamp(expDate1.getTime()));
					if (expDateSign >= 7) {
						pstmt.setTimestamp(++param,new Timestamp(expDate2.getTime()));
					}
				}
				if (isDone != -1) {
					pstmt.setInt(++param, isDone);
				}
				if (isPaid != -1) {
					pstmt.setInt(++param, isPaid);
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showBranches(int showMethod, int branchId, String branchName, String branchAddress) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			int param = 0;
			switch (showMethod) {
			case 0:
				pstmt = con.prepareStatement("SELECT branches.name AS \"Branch Name\", " +
						"branches.address AS \"Branch Address\", " +
						"branches.main AS \"Main Office\", " +
						"(branches.cashiers + branches.photographers + branches.managers + branches.service_staff + branches.other) AS \"Workplaces\" " +
						"FROM branches " +
						"WHERE 1 " +
						branchQueryString + 
						"");
				if ( (branchName != "")||(branchAddress != "") ) {
					if (branchName != "") {
						pstmt.setString(++param, branchName);
					}
					if (branchAddress != "") {
						pstmt.setString(++param, branchAddress);
					}
				} else {
					if (branchId != 0) {
						pstmt.setInt(++param, branchId);
					}
				}
				break;
			case 1:
				pstmt = con.prepareStatement("SELECT kiosks.address AS \"Kiosk Address\", " +
						"CONCAT(branches.name,\" (\",branches.address,\")\") AS \"Parent Branch\", " +
						"kiosks.install_date AS \"Install Date\", " +
						"kiosks.exp_date AS \"Expiration Date\", " +
						"kiosks.workplaces AS \"Workplaces\" " +
						"FROM kiosks, branches " +
						"WHERE branches.branch_id = kiosks.parent_branch_id " +
						"AND (kiosks.exp_date IS NULL OR kiosks.exp_date > NOW())" +
						branchQueryString + 
						"");
				if ( (branchName != "")||(branchAddress != "") ) {
					if (branchName != "") {
						pstmt.setString(++param, branchName);
					}
					if (branchAddress != "") {
						pstmt.setString(++param, branchAddress);
					}
				} else {
					if (branchId != 0) {
						pstmt.setInt(++param, branchId);
					}
				}
				break;
			case 2:
				pstmt = con.prepareStatement("SELECT \"Branch\" AS \"Branch / Kiosk\", " +
						"branches.name AS \"Branch Name / Kiosk Address\", " +
						"branches.address AS \"Branch Address / Parent Branch\", " +
						"branches.main AS \"Main Office\", " +
						"NULL AS \"Install Date\", " +
						"NULL AS \"Expiration Date\", "+
						"(branches.cashiers+branches.photographers+branches.managers+branches.service_staff+branches.other) AS \"Workplaces\" " +
						"FROM branches " +
						"WHERE 1  " +
						branchQueryString + 
						"UNION " +
						"SELECT \"Kiosk\", " +
						"kiosks.address, " +
						"CONCAT(branches.name,\" (\",branches.address,\")\"), " +
						"NULL, " +
						"kiosks.install_date, " +
						"kiosks.exp_date, " +
						"kiosks.workplaces AS \"Workplaces\" " +
						"FROM kiosks, branches " +
						"WHERE branches.branch_id = kiosks.parent_branch_id " +
						"AND (kiosks.exp_date IS NULL OR kiosks.exp_date > NOW())" +
						branchQueryString + 
						"");
				for (int i = 0; i < 2; i++) {
					if ( (branchName != "")||(branchAddress != "") ) {
						if (branchName != "") {
							pstmt.setString(++param, branchName);
						}
						if (branchAddress != "") {
							pstmt.setString(++param, branchAddress);
						}
					} else {
						if (branchId != 0) {
							pstmt.setInt(++param, branchId);
						}
					}
				}
				break;
			}
			
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showSoldEquipment(int branchId, String branchName, String branchAddress, 
			int saleDateSign, java.util.Date saleDate1, java.util.Date saleDate2) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String sdSign = "=";
			if (saleDateSign != 0) {
				switch (saleDateSign) {
				case 1: sdSign = "="; break;
				case 2: sdSign = "<>"; break;
				case 3: sdSign = "<"; break;
				case 4: sdSign = "<="; break;
				case 5: sdSign = ">"; break;
				case 6: sdSign = ">="; break;
				case 7: sdSign = "BETWEEN ? AND"; break;
				case 8: sdSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			String saleDateQueryString = "";
			saleDateQueryString += ((saleDateSign!=0)?"AND sales.sale_date "+sdSign+" ? ":"");
			
			pstmt = con.prepareStatement("SELECT branches.name AS \"Branch Name\", " +
					"branches.address AS \"Branch Address\", " +
					"equipment.name AS \"Equipment Name\", " +
					"equipment_types.name AS \"Equipment Type\", " +
					"SUM(sold_equipment.quantity) AS \"Quantity\", " +
					"SUM(equipment.cost) AS \"Price\" " +
					"FROM branches, equipment, sales, sold_equipment, equipment_types " +
					"WHERE sales.branch_id = branches.branch_id " +
					"AND sold_equipment.sale_id = sales.sale_id " +
					"AND sold_equipment.equipment_id = equipment.equipment_id " +
					"AND equipment.equipment_type_id = equipment_types.equipment_type_id " +
					branchQueryString +
					saleDateQueryString +
					"GROUP BY equipment.equipment_id, branches.branch_id " +
					"ORDER BY `Equipment Name` " +
					"");
			int param = 0;
			if ( (branchName != "")||(branchAddress != "") ) {
				if (branchName != "") {
					pstmt.setString(++param, branchName);
				}
				if (branchAddress != "") {
					pstmt.setString(++param, branchAddress);
				}
			} else {
				if (branchId != 0) {
					pstmt.setInt(++param, branchId);
				}
			}
			if (saleDateSign != 0) {
				pstmt.setTimestamp(++param,new Timestamp(saleDate1.getTime()));
				if (saleDateSign >= 7) {
					pstmt.setTimestamp(++param,new Timestamp(saleDate2.getTime()));
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showSales(int branchId, String branchName, String branchAddress, 
			int saleDateSign, java.util.Date saleDate1, java.util.Date saleDate2) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String sdSign = "=";
			if (saleDateSign != 0) {
				switch (saleDateSign) {
				case 1: sdSign = "="; break;
				case 2: sdSign = "<>"; break;
				case 3: sdSign = "<"; break;
				case 4: sdSign = "<="; break;
				case 5: sdSign = ">"; break;
				case 6: sdSign = ">="; break;
				case 7: sdSign = "BETWEEN ? AND"; break;
				case 8: sdSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			String saleDateQueryString = "";
			saleDateQueryString += ((saleDateSign!=0)?"AND sales.sale_date "+sdSign+" ? ":"");
			
			pstmt = con.prepareStatement("SELECT branches.name AS \"Branch Name\", " +
					"branches.address AS \"Branch Address\", " +
					"SUM(equipment.cost) AS \"Sale Price\", " +
					"sales.sale_date AS \"Sale Date\" " +
					"FROM branches, equipment, sales, sold_equipment " +
					"WHERE sales.branch_id = branches.branch_id " +
					"AND sold_equipment.sale_id = sales.sale_id " +
					"AND sold_equipment.equipment_id = equipment.equipment_id " +
					branchQueryString +
					saleDateQueryString +
					"GROUP BY sales.sale_id " +
					"ORDER BY `Sale Date` " +
					"");
			int param = 0;
			if ( (branchName != "")||(branchAddress != "") ) {
				if (branchName != "") {
					pstmt.setString(++param, branchName);
				}
				if (branchAddress != "") {
					pstmt.setString(++param, branchAddress);
				}
			} else {
				if (branchId != 0) {
					pstmt.setInt(++param, branchId);
				}
			}
			if (saleDateSign != 0) {
				pstmt.setTimestamp(++param,new Timestamp(saleDate1.getTime()));
				if (saleDateSign >= 7) {
					pstmt.setTimestamp(++param,new Timestamp(saleDate2.getTime()));
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showDemandedEquipment(int branchId, String branchName, String branchAddress) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			pstmt = con.prepareStatement("SELECT equipment.name AS \"Equipment Name\", " +
					"equipment_types.name AS \"Equipment Type\", " +
					"SUM(sold_equipment.quantity) AS \"Quantity\" " +
					"FROM branches, equipment, sales, sold_equipment, equipment_types " +
					"WHERE sales.branch_id = branches.branch_id " +
					"AND sold_equipment.sale_id = sales.sale_id " +
					"AND sold_equipment.equipment_id = equipment.equipment_id " +
					"AND equipment.equipment_type_id = equipment_types.equipment_type_id " +
					branchQueryString +
					"GROUP BY equipment.equipment_id " +
					"ORDER BY `Quantity` DESC " +
					"");
			int param = 0;
			if ( (branchName != "")||(branchAddress != "") ) {
				if (branchName != "") {
					pstmt.setString(++param, branchName);
				}
				if (branchAddress != "") {
					pstmt.setString(++param, branchAddress);
				}
			} else {
				if (branchId != 0) {
					pstmt.setInt(++param, branchId);
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showSupplies(int equipmentTypeId, int equipmentQuantitySign, int equipmentQ1, int equipmentQ2, int supplierId,
			int supplyDateSign, java.util.Date supplyDate1, java.util.Date supplyDate2) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String eqSign = "=";
			if (equipmentQuantitySign != 0) {
				switch (equipmentQuantitySign) {
				case 1: eqSign = "="; break;
				case 2: eqSign = "<>"; break;
				case 3: eqSign = "<"; break;
				case 4: eqSign = "<="; break;
				case 5: eqSign = ">"; break;
				case 6: eqSign = ">="; break;
				case 7: eqSign = "BETWEEN ? AND"; break;
				case 8: eqSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String sdSign = "=";
			if (supplyDateSign != 0) {
				switch (supplyDateSign) {
				case 1: sdSign = "="; break;
				case 2: sdSign = "<>"; break;
				case 3: sdSign = "<"; break;
				case 4: sdSign = "<="; break;
				case 5: sdSign = ">"; break;
				case 6: sdSign = ">="; break;
				case 7: sdSign = "BETWEEN ? AND"; break;
				case 8: sdSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String supplyDateQueryString = "";
			supplyDateQueryString += ((supplyDateSign!=0)?"AND supplies.supply_date "+sdSign+" ? ":"");
			String supplyTypeQueryString = "";
			supplyTypeQueryString += ((equipmentTypeId!=0)?"AND equipment_types.name = ? ":"");
			supplyTypeQueryString += ((equipmentQuantitySign!=0)?"AND supplies.quantity "+eqSign+" ? ":"");
			supplyTypeQueryString += ((supplierId!=0)?"AND suppliers.supplier_id = ? ":"");
			
			pstmt = con.prepareStatement("SELECT CONCAT(suppliers.name, \" (\", suppliers.address, \")\") AS \"Supplier\", " +
					"CONCAT(equipment.name, \" (\", equipment_types.name, \")\") AS \"Equipment\", " +
					"deliverable_equipment.cost AS \"Quotation\", " +
					"supplies.quantity AS \"Quantity\", " +
					"supplies.quantity*deliverable_equipment.cost AS \"Supply Price\", " +
					"supplies.supply_date AS \"Supply Date\" " +
					"FROM equipment, equipment_types, supplies, suppliers, deliverable_equipment " +
					"WHERE deliverable_equipment.equipment_id = equipment.equipment_id " +
					"AND deliverable_equipment.supplier_id = suppliers.supplier_id " +
					"AND supplies.deliverable_equipment_id = deliverable_equipment.deliverable_equipment_id " +
					"AND equipment.equipment_type_id = equipment_types.equipment_type_id " +
					supplyTypeQueryString +
					supplyDateQueryString +
					"ORDER BY `Supply Date`, `Equipment` " +
					"");
			int param = 0;
			if (supplyDateSign != 0) {
				pstmt.setTimestamp(++param,new Timestamp(supplyDate1.getTime()));
				if (supplyDateSign >= 7) {
					pstmt.setTimestamp(++param,new Timestamp(supplyDate2.getTime()));
				}
			}
			if (equipmentTypeId != 0) {
				pstmt.setInt(++param, equipmentTypeId);
			}
			if (equipmentQuantitySign != 0) {
				pstmt.setInt(++param, equipmentQ1);
				if (supplyDateSign >= 7) {
					pstmt.setInt(++param, equipmentQ2);
				}
			}
			if (supplierId != 0) {
				pstmt.setInt(++param, supplierId);
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showSuppliers(int equipmentTypeId, int equipmentQuantitySign, int equipmentQ1, int equipmentQ2, 
			int supplyDateSign, java.util.Date supplyDate1, java.util.Date supplyDate2) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String eqSign = "=";
			if (equipmentQuantitySign != 0) {
				switch (equipmentQuantitySign) {
				case 1: eqSign = "="; break;
				case 2: eqSign = "<>"; break;
				case 3: eqSign = "<"; break;
				case 4: eqSign = "<="; break;
				case 5: eqSign = ">"; break;
				case 6: eqSign = ">="; break;
				case 7: eqSign = "BETWEEN ? AND"; break;
				case 8: eqSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String sdSign = "=";
			if (supplyDateSign != 0) {
				switch (supplyDateSign) {
				case 1: sdSign = "="; break;
				case 2: sdSign = "<>"; break;
				case 3: sdSign = "<"; break;
				case 4: sdSign = "<="; break;
				case 5: sdSign = ">"; break;
				case 6: sdSign = ">="; break;
				case 7: sdSign = "BETWEEN ? AND"; break;
				case 8: sdSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String supplyDateQueryString = "";
			if (supplyDateSign != 0) {
				supplyDateQueryString += ("AND supplies.supply_date "+sdSign+" ? ");
			}
			String supplyTypeQueryString = "";
			supplyTypeQueryString += ((equipmentTypeId!=0)?"AND a.ti = ? ":"");
			supplyTypeQueryString += ((equipmentQuantitySign!=0)?"AND a.s "+eqSign+" ? ":"");
			System.out.println(supplyTypeQueryString);
			pstmt = con.prepareStatement("SELECT DISTINCT suppliers.name AS \"Supplier's Name\", " +
					"suppliers.address AS \"Supplier's Address\", " +
					"a.s AS \"Total Quantity\", " +
					"a.t AS \"Equipment Type\" " +
					"FROM suppliers, " +
					"(SELECT suppliers.supplier_id AS i, " +
						"SUM(supplies.quantity) AS s, " +
						"equipment_types.equipment_type_id AS ti, " +
						"equipment_types.name AS t " +
						"FROM deliverable_equipment, supplies, suppliers, equipment, equipment_types " +
						"WHERE deliverable_equipment.equipment_id = equipment.equipment_id " +
						"AND deliverable_equipment.supplier_id = suppliers.supplier_id " +
						"AND supplies.deliverable_equipment_id = deliverable_equipment.deliverable_equipment_id " +
						supplyDateQueryString +
						"AND equipment_types.equipment_type_id = equipment.equipment_type_id " +
						"GROUP BY suppliers.supplier_id, equipment.equipment_type_id " +
					") AS a " +
					"WHERE a.i = suppliers.supplier_id " +
					supplyTypeQueryString +
					"ORDER BY `Supplier's Name` " +
					"");
			int param = 0;
			if (supplyDateSign != 0) {
				pstmt.setTimestamp(++param,new Timestamp(supplyDate1.getTime()));
				if (supplyDateSign >= 7) {
					pstmt.setTimestamp(++param,new Timestamp(supplyDate2.getTime()));
				}
			}
			if (equipmentTypeId != 0) {
				pstmt.setInt(++param, equipmentTypeId);
			}
			if (equipmentQuantitySign != 0) {
				pstmt.setInt(++param, equipmentQ1);
				if (equipmentQuantitySign >= 7) {
					pstmt.setInt(++param, equipmentQ2);
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showDistributions(int branchId, String branchName, String branchAddress, 
			int distributionDateSign, java.util.Date distributionDate1, java.util.Date distributionDate2) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String dbSign = "=";
			if (distributionDateSign != 0) {
				switch (distributionDateSign) {
				case 1: dbSign = "="; break;
				case 2: dbSign = "<>"; break;
				case 3: dbSign = "<"; break;
				case 4: dbSign = "<="; break;
				case 5: dbSign = ">"; break;
				case 6: dbSign = ">="; break;
				case 7: dbSign = "BETWEEN ? AND"; break;
				case 8: dbSign = "NOT BETWEEN ? AND"; break;
				}
			}
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			String saleDateQueryString = "";
			saleDateQueryString += ((distributionDateSign!=0)?"AND distributions.distribution_date "+dbSign+" ? ":"");
			
			pstmt = con.prepareStatement("SELECT branches.name AS \"Branch Name\", " +
					"branches.address AS \"Branch Address\", " +
					"equipment.name AS \"Equipment Name\", " +
					"equipment_types.name AS \"Equipment Type\", " +
					"distributions.quantity AS \"Quantity\", " +
					"distributions.distribution_date AS \"Distribution Date\" " +
					"FROM branches, equipment, distributions, equipment_types " +
					"WHERE distributions.branch_id = branches.branch_id " +
					"AND distributions.equipment_id = equipment.equipment_id " +
					"AND equipment.equipment_type_id = equipment_types.equipment_type_id " +
					branchQueryString +
					saleDateQueryString +
					"ORDER BY `Distribution Date` " +
					"");
			int param = 0;
			if ( (branchName != "")||(branchAddress != "") ) {
				if (branchName != "") {
					pstmt.setString(++param, branchName);
				}
				if (branchAddress != "") {
					pstmt.setString(++param, branchAddress);
				}
			} else {
				if (branchId != 0) {
					pstmt.setInt(++param, branchId);
				}
			}
			if (distributionDateSign != 0) {
				pstmt.setTimestamp(++param,new Timestamp(distributionDate1.getTime()));
				if (distributionDateSign >= 7) {
					pstmt.setTimestamp(++param,new Timestamp(distributionDate2.getTime()));
				}
			}
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public ResponseTable showWorkplaces(boolean isTotal, int branchId, String branchName, String branchAddress) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		ResponseTable table = null;
		try {
			String branchQueryString = "";
			if ( (branchName != "")||(branchAddress != "") ) {
				branchQueryString += ((branchName!="")?"AND branches.name LIKE ? ":"");
				branchQueryString += ((branchAddress!="")?"AND branches.address LIKE ? ":"");
			} else {
				branchQueryString = ((branchId!=0)?"AND branches.branch_id = ? ":"");
			}
			
			if (isTotal) {
				pstmt = con.prepareStatement("SELECT SUM(a.c) AS \"Total Cashiers\", " +
						"SUM(a.p) AS \"Total Photographers\", " +
						"SUM(a.m) AS \"Total Managers\", " +
						"SUM(a.ps) AS \"Total Service Staff\", " +
						"SUM(a.o) AS \"Total Other\", " +
						"SUM(a.t) AS \"Total All\" " +
						"FROM ( " +
						"SELECT " +
						"1 AS i, " +
						"branches.name AS bn, " +
						"branches.address AS ba, " +
						"branches.cashiers AS c, " +
						"branches.photographers AS p, " +
						"branches.managers AS m, " +
						"branches.service_staff AS ps, " +
						"branches.other AS o, " +
						"(branches.cashiers + branches.photographers + branches.managers + branches.service_staff + branches.other) AS t " +
						"FROM branches " +
						") AS a " +
						"WHERE 1 GROUP BY a.i " +
						"");
				
			} else {
				pstmt = con.prepareStatement("SELECT " +
						"branches.name AS \"Branch Name\", " +
						"branches.address AS \"Branch Address\", " +
						"branches.cashiers AS \"Cashiers\", " +
						"branches.photographers AS \"Photographers\", " +
						"branches.managers AS \"Managers\", " +
						"branches.service_staff AS \"Service Staff\", " +
						"branches.other AS \"Other\", " +
						"(branches.cashiers + branches.photographers + branches.managers + branches.service_staff + branches.other) AS \"Total\" " +
						"FROM branches " +
						"WHERE 1 " +
						branchQueryString +
						"");
				
				int param = 0;
				if ( (branchName != "")||(branchAddress != "") ) {
					if (branchName != "") {
						pstmt.setString(++param, branchName);
					}
					if (branchAddress != "") {
						pstmt.setString(++param, branchAddress);
					}
				} else {
					if (branchId != 0) {
						pstmt.setInt(++param, branchId);
					}
				}
			}
			
			pstmt.execute();
			rs = pstmt.getResultSet();
			table = getResponseTable(pstmt);
		}
		finally {
			closeResources();
		}
		return table;
	}
	
	public void editOrder(int orderId, int clientId, 
			boolean isBranchEnabled, int branchId,
			boolean isKiosksEnabled, int kioskId, 
			int developmentTypeId, int printingTypeId, int framesId, int formatTypeId, int paperTypeId, 
			boolean isPressing, 
			java.util.Date orderDate,
			boolean isDone, boolean isPaid) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("SELECT frames_id FROM orders WHERE order_id = ?");
			pstmt.setInt(1, orderId);
			pstmt.execute();
			rs = pstmt.getResultSet();
			int frames_id = 0;
			if (rs != null) {
				if (rs.first()) {
					frames_id = rs.getInt(1);
				}
			}
			if (frames_id != framesId) {
				pstmt = con.prepareStatement("DELETE FROM frames WHERE frames_id = ? ");
				pstmt.setInt(1, frames_id);
				pstmt.executeUpdate();
			}
			
			String parametersQueryString = "";
			if (isKiosksEnabled) {
				parametersQueryString += "branch_id = NULL, kiosk_id = ?, ";
			} else {
				parametersQueryString += "branch_id = ?, kiosk_id = NULL, ";
			}
			parametersQueryString += ((developmentTypeId!=0)?"development_type_id = ?, ":"");
			parametersQueryString += ((printingTypeId!=0)?"printing_type_id = ?, ":"");
			parametersQueryString += ((framesId!=0)?"frames_id = ?, ":"");
			parametersQueryString += ((formatTypeId!=0)?"format_type_id = ?, ":"");
			parametersQueryString += ((paperTypeId!=0)?"paper_type_id = ?, ":"");
			
			pstmt = con.prepareStatement("UPDATE orders SET client_id = ?, " +
					parametersQueryString +
					"pressing = ?, order_date = ?, done = ?, paid = ? WHERE order_id = ? ");
			int param = 0;
			pstmt.setInt(++param, clientId);
			if (isKiosksEnabled) {
				pstmt.setInt(++param, kioskId);
			} else {
				pstmt.setInt(++param, branchId);
			}
			if (developmentTypeId != 0) {
				pstmt.setInt(++param, developmentTypeId);
			}
			if (printingTypeId != 0) {
				pstmt.setInt(++param, printingTypeId);
			}
			if (framesId != 0) {
				pstmt.setInt(++param, framesId);
			}
			if (formatTypeId != 0) {
				pstmt.setInt(++param, formatTypeId);
			}
			if (paperTypeId != 0) {
				pstmt.setInt(++param, paperTypeId);
			}
			pstmt.setBoolean(++param, isPressing);
			pstmt.setTimestamp(++param, new Timestamp(orderDate.getTime()));
			pstmt.setBoolean(++param, isDone);
			pstmt.setBoolean(++param, isPaid);
			
			pstmt.setInt(++param, orderId);
			pstmt.executeUpdate();
		}
		finally {
			closeResources();
		}
	}
	
	public void editService(int serviceId, int clientId, int branchId, int serviceTypeId, int equipmentId,  
			java.util.Date serviceDate, java.util.Date extDate, boolean isDone, boolean isPaid) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		try {
			String serviceTypeQueryString = "";
			serviceTypeQueryString += ((equipmentId==0)?"equipment_id = NULL, ":"equipment_id = ?, ");
			
			pstmt = con.prepareStatement("UPDATE services SET client_id = ?, branch_id = ?, service_type_id = ?, " +
					serviceTypeQueryString +
					"service_date = ?, exp_date = ?, done = ?, paid = ? WHERE service_id = ? ");
			int param = 0;
			pstmt.setInt(++param, clientId);
			pstmt.setInt(++param, branchId);
			pstmt.setInt(++param, serviceTypeId);
			if (equipmentId != 0) {
				pstmt.setInt(++param, equipmentId);
			}
			pstmt.setTimestamp(++param, new Timestamp(serviceDate.getTime()));
			pstmt.setTimestamp(++param, new Timestamp(extDate.getTime()));
			pstmt.setBoolean(++param, isDone);
			pstmt.setBoolean(++param, isPaid);
			pstmt.setInt(++param, serviceId);
			pstmt.executeUpdate();
		}
		finally {
			closeResources();
		}
	}
	
	public void editClient(int clientId, String firstname, String lastname, String address, boolean professional, boolean discountEnable, double rate, java.util.Date exp_date) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("UPDATE clients SET firstname = ?, lastname = ?, address = ?, professional = ? WHERE client_id = ? ");
			int param = 0;
			pstmt.setString(++param, firstname);
			pstmt.setString(++param, lastname);
			pstmt.setString(++param, address);
			pstmt.setBoolean(++param, professional);
			pstmt.setInt(++param, clientId);
			pstmt.executeUpdate();
			
			if (discountEnable) {
				pstmt = con.prepareStatement("SELECT discount_id FROM discounts WHERE client_id = ?");
				pstmt.setInt(1, clientId);
				pstmt.execute();
				rs = pstmt.getResultSet();
				if (getResultSetRowCount() == 0) {
					pstmt = con.prepareStatement("INSERT INTO discounts (client_id, rate, exp_date) VALUES (?, ?, ?)");
					pstmt.setInt(1, clientId);
					pstmt.setDouble(2, rate);
					pstmt.setTimestamp(3, new Timestamp(exp_date.getTime()));
					pstmt.executeUpdate();
				} else {
					pstmt = con.prepareStatement("UPDATE discounts SET rate = ?, exp_date = ? WHERE client_id = ? ");
					pstmt.setDouble(1, rate);
					pstmt.setTimestamp(2, new Timestamp(exp_date.getTime()));
					pstmt.setInt(3, clientId);
					pstmt.executeUpdate();
				}
			} else {
				pstmt = con.prepareStatement("DELETE FROM discounts WHERE client_id = ? ");
				pstmt.setInt(1, clientId);
				pstmt.executeUpdate();
			}
			
		}
		finally {
			closeResources();
		}
	}
	
	public void deleteOrder(int id) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("SELECT frames_id FROM orders WHERE order_id = ?");
			pstmt.setInt(1, id);
			pstmt.execute();
			rs = pstmt.getResultSet();
			int frames_id = 0;
			if (rs != null) {
				if (rs.first()) {
					frames_id = rs.getInt(1);
				}
			}
			pstmt = con.prepareStatement("DELETE FROM frames WHERE frames_id = ? ");
			pstmt.setInt(1, frames_id);
			pstmt.executeUpdate();
			
			pstmt = con.prepareStatement("DELETE FROM orders WHERE order_id = ? ");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		}
		finally {
			closeResources();
		}
	}
	
	public void deleteService(int id) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("DELETE FROM services WHERE service_id = ? ");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		}
		finally {
			closeResources();
		}
	}
	
	public void deleteClient(int id) throws SQLException {
		rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("DELETE FROM clients WHERE client_id = ? ");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			pstmt = con.prepareStatement("DELETE FROM discounts WHERE client_id = ? ");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			pstmt = con.prepareStatement("DELETE FROM orders WHERE client_id = ? ");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			pstmt = con.prepareStatement("DELETE FROM services WHERE client_id = ? ");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		}
		finally {
			closeResources();
		}
	}
	
	
	
}
