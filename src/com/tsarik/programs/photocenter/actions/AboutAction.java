package com.tsarik.programs.photocenter.actions;


import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;

public class AboutAction extends MainFrameMenuAction {
	public AboutAction(MainFrame owner) {
		super(owner, "About...");
		putValue(SHORT_DESCRIPTION, "About...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		JOptionPane.showMessageDialog(owner,
				"<html><b>"
					+"<h3>"+Parameters.PROGRAM_NAME+"</h3>"
					+"<br>Version: "+Parameters.PROGRAM_VERSION+"."+Parameters.PROGRAM_BUILD+"</b></html>\n\n"
					+"<html>"
					+"&#169; "+Parameters.PROGRAM_YEAR+" by "+Parameters.PROGRAM_AUTHOR
					+"<br>&nbsp;</html>",
				"About", JOptionPane.INFORMATION_MESSAGE);
	}

}
