SELECT 
	"Branch" AS "Branch / Kiosk", 
	branches.name AS "Branch Name / Kiosk Address", 
	branches.address AS "Branch Address / Parent Branch", 
	SUM(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) AS "Printed Photos Count" 

FROM orders, branches, frames 

WHERE orders.branch_id = branches.branch_id 
	AND orders.kiosk_id IS NULL 
	AND orders.frames_id IS NOT NULL 
	AND orders.frames_id = frames.frames_id 
	
	AND orders.done = 1 

GROUP BY branches.branch_id 




UNION 




SELECT 
	"Kiosk", 
	kiosks.address, 
	CONCAT(branches.name," (",branches.address,")"), 
	SUM(frames.count_1+frames.count_2+frames.count_3+frames.count_4+frames.count_5+frames.count_6+frames.count_7+frames.count_8+frames.count_9+frames.count_10+frames.count_11+frames.count_12+frames.count_13+frames.count_14+frames.count_15+frames.count_16+frames.count_17+frames.count_18+frames.count_19+frames.count_20+frames.count_21+frames.count_22+frames.count_23+frames.count_24+frames.count_25+frames.count_26+frames.count_27+frames.count_28+frames.count_29+frames.count_30+frames.count_31+frames.count_32+frames.count_33+frames.count_34+frames.count_35+frames.count_36) 

FROM orders, branches, kiosks, frames 

WHERE orders.branch_id IS NULL 
	AND orders.kiosk_id = kiosks.kiosk_id 
	AND kiosks.parent_branch_id = branches.branch_id 
	AND orders.frames_id IS NOT NULL 
	AND orders.frames_id = frames.frames_id 
	
	
	AND orders.done = 1 

GROUP BY branches.branch_id 



