package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class EditOrderDialog extends AbstractEditDialog {
	
	public EditOrderDialog(MainFrame owner, int id) {
		super(owner, "Edit Order", 450, 400, id);
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		cbKiosks.removeAllItems();
		cbKiosks.addItem(new ComboBoxDatabaseItem());
		cbDevelopmentTypes.removeAllItems();
		cbDevelopmentTypes.addItem(new ComboBoxDatabaseItem());
		cbPrintingTypes.removeAllItems();
		cbPrintingTypes.addItem(new ComboBoxDatabaseItem());
		cbFrames.removeAllItems();
		cbFrames.addItem(new ComboBoxDatabaseItem());
		cbFormatTypes.removeAllItems();
		cbFormatTypes.addItem(new ComboBoxDatabaseItem());
		cbPaperTypes.removeAllItems();
		cbPaperTypes.addItem(new ComboBoxDatabaseItem());
		
		int client_id = 0;
		int branch_id = 0;
		int kiosk_id = 0;
		int development_type_id = 0;
		int printing_type_id = 0;
		int frames_id = 0;
		int format_type_id = 0;
		int paper_type_id = 0;
		boolean pressing = false;
		java.sql.Timestamp order_date = null;
		boolean done = false;
		boolean paid = false;
		
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,branch_id,kiosk_id,development_type_id,printing_type_id,frames_id,format_type_id,paper_type_id,pressing,order_date,done,paid FROM orders WHERE order_id = " + String.valueOf(id));
			if (table.getRecordCount() != 0) {
				if (table.getItem(0,0) != null) {
					client_id = ((Integer)table.getItem(0,0)).intValue();
				}
				if (table.getItem(0,1) != null) {
					branch_id = ((Integer)table.getItem(0,1)).intValue();
				}
				if (table.getItem(0,2) != null) {
					kiosk_id = ((Integer)table.getItem(0,2)).intValue();
				}
				if (table.getItem(0,3) != null) {
					development_type_id = ((Integer)table.getItem(0,3)).intValue();
				}
				if (table.getItem(0,4) != null) {
					printing_type_id = ((Integer)table.getItem(0,4)).intValue();
				}
				if (table.getItem(0,5) != null) {
					frames_id = ((Integer)table.getItem(0,5)).intValue();
				}
				if (table.getItem(0,6) != null) {
					format_type_id = ((Integer)table.getItem(0,6)).intValue();
				}
				if (table.getItem(0,7) != null) {
					paper_type_id = ((Integer)table.getItem(0,7)).intValue();
				}
				if (table.getItem(0,8) != null) {
					pressing = ((Boolean)table.getItem(0,8)).booleanValue();
				}
				if (table.getItem(0,9) != null) {
					order_date = (java.sql.Timestamp)table.getItem(0,9);
				}
				if (table.getItem(0,10) != null) {
					done = ((Boolean)table.getItem(0,10)).booleanValue();
				}
				if (table.getItem(0,11) != null) {
					paid = ((Boolean)table.getItem(0,11)).booleanValue();
				}
			}
			
			table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			selectComboBoxDatabaseItemById(cbClients,client_id);
			
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			selectComboBoxDatabaseItemById(cbBranches,branch_id);
			
			table = dbManager.queryToTable("SELECT kiosks.kiosk_id,kiosks.address,branches.name,branches.address FROM kiosks,branches WHERE kiosks.parent_branch_id = branches.branch_id ORDER BY kiosks.kiosk_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbKiosks.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + " - " + table.getItem(i,3) + ")"));
			}
			selectComboBoxDatabaseItemById(cbKiosks,kiosk_id);
			
			table = dbManager.queryToTable("SELECT development_type_id,name,cost FROM development_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbDevelopmentTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			selectComboBoxDatabaseItemById(cbDevelopmentTypes,development_type_id);
			
			table = dbManager.queryToTable("SELECT printing_type_id,name,cost_mult FROM printing_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbPrintingTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			selectComboBoxDatabaseItemById(cbPrintingTypes,printing_type_id);
			
			table = dbManager.queryToTable("SELECT format_type_id,name,cost FROM format_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbFormatTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			selectComboBoxDatabaseItemById(cbFormatTypes,format_type_id);
			
			table = dbManager.queryToTable("SELECT paper_type_id,name FROM paper_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbPaperTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
			selectComboBoxDatabaseItemById(cbPaperTypes,paper_type_id);
			
			table = dbManager.queryToTable("SELECT frames_id,count_1,count_2,count_3,count_4,count_5,count_6,count_7,count_8,count_9,count_10 FROM frames ORDER BY frames_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbFrames.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,0)+" ("+table.getItem(i,1)+", "+table.getItem(i,2)+", "+table.getItem(i,3)+", "+table.getItem(i,4)+", "+table.getItem(i,5)+", "+table.getItem(i,6)+", "+table.getItem(i,7)+", "+table.getItem(i,8)+", "+table.getItem(i,9)+", "+table.getItem(i,10)+", ...)"));
			}
			selectComboBoxDatabaseItemById(cbFrames,frames_id);
			
			cbBranchEnabled.setSelected(branch_id != 0);
			cbKiosksEnabled.setSelected(kiosk_id != 0);
			cbPressing.setSelected(pressing);
			cbDone.setSelected(done);
			cbPaid.setSelected(paid);
			if (order_date != null) {
				((SpinnerDateModel)spOrderDate.getModel()).setValue(new java.util.Date(order_date.getTime()));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.editOrder(id,
					((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(),
					cbBranchEnabled.isSelected(),
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					cbKiosksEnabled.isSelected(),
					((ComboBoxDatabaseItem)cbKiosks.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbDevelopmentTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbPrintingTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbFrames.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbFormatTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbPaperTypes.getSelectedItem()).getId(),
					cbPressing.isSelected(),
					((SpinnerDateModel)spOrderDate.getModel()).getDate(),
					cbDone.isSelected(), 
					cbPaid.isSelected());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		cbBranches = new JComboBox();
		cbKiosks = new JComboBox();
		cbBranchEnabled = new JRadioButton("Branch");
		cbBranchEnabled.setSelected(true);
		cbKiosksEnabled = new JRadioButton("Kiosk");
		ButtonGroup gr = new ButtonGroup();
		gr.add(cbBranchEnabled);
		gr.add(cbKiosksEnabled);
		cbPressing = new JCheckBox("Pressing");
		cbDone = new JCheckBox("Done");
		cbPaid = new JCheckBox("Paid");
		cbDevelopmentTypes = new JComboBox();
		cbPrintingTypes = new JComboBox();
		cbFrames = new JComboBox();
		cbFormatTypes = new JComboBox();
		cbPaperTypes = new JComboBox();
		spOrderDate = new JSpinner(new SpinnerDateModel());
		
		JButton btAddFrames = new JButton("Add");
		btAddFrames.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int c = cbFrames.getItemCount();
				int s = cbFrames.getSelectedIndex();
				AddFramesDialog d = new AddFramesDialog(EditOrderDialog.this);
				d.setVisible(true);
				refresh();
				if (cbFrames.getItemCount() != c) {
					cbFrames.setSelectedIndex(cbFrames.getItemCount()-1);
				} else {
					if (s < cbFrames.getItemCount()) {
						cbFrames.setSelectedIndex(s);
					}
				}
			}
		});
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spOrderDate.setValue(new Date());
			}
		});
		
		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbClients, 1, 0, 1, 1, 2, panel);
		addComponentToGrid(cbBranchEnabled, 0, 1, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 1, 1, 1, 2, panel);
		addComponentToGrid(cbKiosksEnabled, 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbKiosks, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Development Type"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(cbDevelopmentTypes, 1, 3, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Printing Type"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(cbPrintingTypes, 1, 4, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Format"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(cbFormatTypes, 1, 5, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Paper Type"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(cbPaperTypes, 1, 6, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Frames"), 0, 7, 1, 1, 1, panel);
		addComponentToGrid(cbFrames, 1, 7, 1, 1, 2, panel);
		addComponentToGrid(btAddFrames, 2, 7, 1, 1, 3, panel);
		addComponentToGrid(cbPressing, 1, 8, 1, 1, 2, panel);
		addComponentToGrid(new JLabel("Order Date"), 0, 9, 1, 1, 1, panel);
		addComponentToGrid(spOrderDate, 1, 9, 1, 1, 2, panel);
		addComponentToGrid(btNow, 2, 9, 1, 1, 3, panel);
		addComponentToGrid(cbDone, 1, 10, 1, 1, 2, panel);
		addComponentToGrid(cbPaid, 1, 11, 1, 1, 2, panel);
		return panel;
	}
	
	private JComboBox cbClients;
	private JComboBox cbBranches;
	private JComboBox cbKiosks;
	private JRadioButton cbBranchEnabled;
	private JRadioButton cbKiosksEnabled;
	private JComboBox cbDevelopmentTypes;
	private JComboBox cbPrintingTypes;
	private JComboBox cbFrames;
	private JComboBox cbFormatTypes;
	private JComboBox cbPaperTypes;
	private JCheckBox cbPressing;
	private JSpinner spOrderDate;
	private JCheckBox cbDone;
	private JCheckBox cbPaid;
	
	
}
