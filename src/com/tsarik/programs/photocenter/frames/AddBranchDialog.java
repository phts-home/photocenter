package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.sql.SQLException;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;



public class AddBranchDialog extends AbstractAddDialog {
	
	public AddBranchDialog(MainFrame owner) {
		super(owner, "Add Branch", 450, 350);
	}
	
	public AddBranchDialog(AbstractDialog owner) {
		super(owner, "Add Branch", 450, 350);
	}
	
	@Override
	protected void refresh() {
		// Nothing
	}
	
	@Override
	protected int apply() {
		if ( (tfName.getText().equals("")) || (tfAddr.getText().equals("")) ) {
			JOptionPane.showMessageDialog(owner, Parameters.ERROR_INPUT, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
			return APPLY_CHECK_FAIL;
		}
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addBranch(tfName.getText(), tfAddr.getText(), cbMain.isSelected(),
					((SpinnerNumberModel)spCashiers.getModel()).getNumber().intValue(),
					((SpinnerNumberModel)spPhotographers.getModel()).getNumber().intValue(),
					((SpinnerNumberModel)spManagers.getModel()).getNumber().intValue(),
					((SpinnerNumberModel)spServiceStaff.getModel()).getNumber().intValue(),
					((SpinnerNumberModel)spOther.getModel()).getNumber().intValue()
					);
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		tfName = new JTextField();
		tfAddr = new JTextField();
		cbMain = new JCheckBox("Main Office");
		
		spCashiers = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		spPhotographers = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		spManagers = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		spServiceStaff = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		spOther = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		
		// put components on the panel
		addComponentToGrid(new JLabel("Name"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfName, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Address"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfAddr, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(cbMain, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Cashiers"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(spCashiers, 1, 3, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Photographers"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(spPhotographers, 1, 4, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Managers"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(spManagers, 1, 5, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Service staff"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(spServiceStaff, 1, 6, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Other"), 0, 7, 1, 1, 1, panel);
		addComponentToGrid(spOther, 1, 7, 1, 1, 4, panel);
		
		return panel;
	}
	
	private JTextField tfName;
	private JTextField tfAddr;
	private JCheckBox cbMain;
	
	private JSpinner spCashiers;
	private JSpinner spPhotographers;
	private JSpinner spManagers;
	private JSpinner spServiceStaff;
	private JSpinner spOther;
	
}
