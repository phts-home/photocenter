package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;




public abstract class AbstractPhotocenterProgramDialog extends AbstractDialog {
	
	public final int APPLY_CHECK_OK = 0;
	public final int APPLY_CHECK_FAIL = 1;
	
	public AbstractPhotocenterProgramDialog(MainFrame owner, String title, int w, int h) {
		super(owner, true, title, null, w, h);
		this.owner = owner;
	}
	
	public AbstractPhotocenterProgramDialog(MainFrame owner, String title, int w, int h, String[] buttonNames) {
		super(owner, true, title, null, w, h, buttonNames);
		this.owner = owner;
	}
	
	public AbstractPhotocenterProgramDialog(AbstractDialog owner, String title, int w, int h, String[] buttonNames) {
		super(owner, true, title, null, w, h, buttonNames);
		this.owner = null;
	}
	
	protected abstract int apply();
	
	protected MainFrame owner;
	protected GridBagConstraints constraints;
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		return panel;
	}
	
	/**
	 * Adds component to the layout grid.
	 * 
	 * @param c Component
	 * @param x X-coordinate in grid
	 * @param y Y-coordinate in grid
	 * @param w The number of cells in a row in the component's display area
	 * @param h The number of cells in a column in the component's display area
	 * @param target Container
	 */
	protected void addComponentToGrid(Component c, int x, int y, int w, int h, Container target) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		((GridBagLayout)target.getLayout()).setConstraints(c, constraints);
		target.add(c);
	}
	
	protected void addComponentToGrid(Component c, int x, int y, int w, int h, int level, Container target) {
		switch (level) {
		case 0: 
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,1,2,1);
			} else {
				constraints.insets.set(1,1,2,1);
			}
			constraints.weightx = 1;
			constraints.weighty = 1;
			constraints.fill = GridBagConstraints.BOTH;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 1: 
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,25,2,5);
			} else {
				constraints.insets.set(1,25,2,5);
			}
			constraints.weightx = 0;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.NORTH;
			break;
		case 2:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,5);
			} else {
				constraints.insets.set(1,0,2,5);
			}
			constraints.weightx = 1;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 3:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,25);
			} else {
				constraints.insets.set(1,0,2,25);
			}
			constraints.weightx = 0;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 4:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,25);
			} else {
				constraints.insets.set(1,0,2,25);
			}
			constraints.weightx = 1;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		case 5:
			if (constraints.insets == null) {
				constraints.insets = new Insets(1,0,2,5);
			} else {
				constraints.insets.set(1,0,2,5);
			}
			constraints.weightx = 0;
			constraints.weighty = 0;
			constraints.fill = GridBagConstraints.NONE;
			constraints.anchor = GridBagConstraints.CENTER;
			break;
		}
		addComponentToGrid(c, x, y, w, h, target);
	}
	
}
