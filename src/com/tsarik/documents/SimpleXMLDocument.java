package com.tsarik.documents;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Simple XML document.
 * 
 * @author Phil Tsarik
 * @author Sergey Tuchina
 * @version 1.0.2
 *
 */
public class SimpleXMLDocument {
	
	/**
	 * Constructs a new instance of the <code>SimpleXMLDocument</code> class.
	 * 
	 * @param rootName Root name of this XML document
	 */
	public SimpleXMLDocument(String rootName) {
		doc = createXML(rootName);
	}
	
	/**
	 * Constructs a new instance of the <code>SimpleXMLDocument</code> class.
	 * 
	 * @param file Path of this XML document file
	 */
	public SimpleXMLDocument(File file) {
		loadXML(file);
	}
	
	/**
	 * Gets document.
	 * 
	 * @return Document
	 */
	public Document getDocument() {
		return doc;
	}
	
	/**
	 * Gets XML document's root
	 * 
	 * @return XML document's root
	 */
	public Element getRoot() {
		return root;
	}
	
	/**
	 * Reads boolean value.
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param def Default value
	 * @return Read boolean value or default value (if read error is occupied)
	 */
	public boolean readBooleanValue(Element parent, String name, boolean def){
		Element elem = findElement(parent, name);
		if (elem == null) {
			return def;
		}
		NamedNodeMap attrs = elem.getAttributes();
		if ((attrs.getLength() == 1) && (attrs.item(0).getNodeName().equals("value"))) {
			if (((Attr)attrs.item(0)).getValue().equals(String.valueOf(true)))
				return true;
			if (((Attr)attrs.item(0)).getValue().equals(String.valueOf(false)))
				return false;
		}
		return def;
	}
	
	/**
	 * Reads string value.
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param def Default value
	 * @return Read string value or default value (if read error is occupied)
	 */
	public String readStringValue(Element parent, String name, String def){
		Element elem = findElement(parent, name);
		if (elem == null) {
			return def;
		}
		NamedNodeMap attrs = elem.getAttributes();
		if ((attrs.getLength() == 1) && (attrs.item(0).getNodeName().equals("value"))) {
			return ((Attr)attrs.item(0)).getValue();
		}
		return def;
	}
	
	/**
	 * Reads character value. Also value can be '\\', '\n', '\r' or '\t'.
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param def Default value
	 * @return Read integer value or default value (if read error is occupied)
	 */
	public Character readCharacterValue(Element parent, String name, Character def){
		Element elem = findElement(parent, name);
		if (elem == null) {
			return def;
		}
		NamedNodeMap attrs = elem.getAttributes();
		if ((attrs.getLength() == 1) && (attrs.item(0).getNodeName().equals("value"))) {
			Attr attr = (Attr)attrs.item(0);
			if (attr.getValue().length() == 1) {
				return attr.getValue().charAt(0);
			} else {
				if ( (attr.getValue().length() == 2) && (attr.getValue().charAt(0) == '\\') ) {
					char ch = attr.getValue().charAt(1);
					switch (ch) {
					case '\\':
						return '\\';
					case 'n':
						return '\n';
					case 'r':
						return '\r';
					case 't':
						return '\t';
					}
				}
			}
		}
		return def;
	}
	
	/**
	 * Reads integer value.
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param def Default value
	 * @return Read integer value or default value (if read error is occupied)
	 */
	public int readIntegerValue(Element parent, String name, int def){
		Element elem = findElement(parent, name);
		if (elem == null) {
			return def;
		}
		NamedNodeMap attrs = elem.getAttributes();
		if ((attrs.getLength() == 1) && (attrs.item(0).getNodeName().equals("value"))) {
			return Integer.parseInt(((Attr)attrs.item(0)).getValue());
		}
		return def;
	}
	
	/**
	 * Reads color value.
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param def Default value
	 * @return Read color value or default value (if read error is occupied)
	 */
	public Color readColorValue(Element parent, String name, Color def){
		Element elem = findElement(parent, name);
		if (elem == null) {
			return def;
		}
		NamedNodeMap attrs = elem.getAttributes();
		if ( (attrs.getLength() == 3) && (attrs.item(0).getNodeName().equals("b")) && (attrs.item(1).getNodeName().equals("g")) && (attrs.item(2).getNodeName().equals("r")) ) {
			int b = Integer.parseInt(((Attr)attrs.item(0)).getValue());
			int g = Integer.parseInt(((Attr)attrs.item(1)).getValue());
			int r = Integer.parseInt(((Attr)attrs.item(2)).getValue());
			return new Color(r, g, b);
		}
		return def;
	}
	
	/**
	 * Reads font value.
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param def Default value
	 * @return Read font value or default value (if read error is occupied)
	 */
	public Font readFontValue(Element parent, String name, Font def){
		Element elem = findElement(parent, name);
		if (elem == null) {
			return def;
		}
		NamedNodeMap attrs = elem.getAttributes();
		Node node;
		int style = Font.PLAIN;
		node = attrs.getNamedItem("bold");
		if (node != null) {
			if ( ((Attr)node).getValue().equals(String.valueOf(true)) ) {
				style = Font.BOLD;
			}
		} else {
			if (def.isBold()) {
				style = Font.BOLD;
			}
		}
		node = attrs.getNamedItem("italic");
		if (node != null) {
			if ( ((Attr)node).getValue().equals(String.valueOf(true)) ) {
				if (style == Font.PLAIN) {
					style = Font.ITALIC;
				} else {
					style = style | Font.ITALIC;
				}
			}
		} else {
			if (def.isItalic()) {
				if (style == Font.PLAIN) {
					style = Font.ITALIC;
				} else {
					style = style | Font.ITALIC;
				}
			}
		}
		node = attrs.getNamedItem("name");
		String n = def.getName();
		if (node != null) {
			n = ((Attr)node).getValue();
		}
		node = attrs.getNamedItem("size");
		int s = def.getSize();
		if (node != null) {
			s = Integer.parseInt(((Attr)node).getValue());
		}
		return new Font(n, style, s);
		/*
		if ( (attrs.getLength() == 4) && (attrs.item(0).getNodeName().equals("bold"))
				&& (attrs.item(1).getNodeName().equals("italic"))
				&& (attrs.item(2).getNodeName().equals("name"))
				&& (attrs.item(3).getNodeName().equals("size"))) {
			int style = 0;
			if (((Attr)attrs.item(0)).getValue().equals(String.valueOf(true))) {
				if (((Attr)attrs.item(1)).getValue().equals(String.valueOf(true))) {
					style = Font.BOLD | Font.ITALIC;
				} else {
					style = Font.BOLD;
				}
			} else {
				if (((Attr)attrs.item(1)).getValue().equals(String.valueOf(true))) {
					style = Font.ITALIC;
				} else {
					style = Font.PLAIN;
				}
			}
			String n = ((Attr)attrs.item(2)).getValue();
			int s = Integer.parseInt(((Attr)attrs.item(3)).getValue());
			return new Font(n, style, s);
		}*/
		//return def;
	}
	
	/**
	 * Reads a string array, represented as:
	 * <code><pre>
	 * &lt;name&gt;
	 *   &lt;itemName0 value="stringValue"/&gt;
	 *   &lt;itemName1 value="stringValue"/&gt;
	 *   &lt;itemName2 value="stringValue"/&gt;
	 *   ...
	 *   &lt;itemNameX value="stringValue"/&gt;
	 * &lt;/name&gt;
	 * </pre></code>
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param itemName Item name of the array
	 * @param stopValue Value, which is used to check end of array
	 * @param def Default value
	 * @return Read string array or default value (if read error is occupied)
	 */
	public java.util.ArrayList<String> readStringArray(Element parent, String name, String itemName, String stopValue, java.util.ArrayList<String> def){
		Element parentElem = findElement(parent, name);
		if (parentElem == null) {
			return def;
		}
		ArrayList<String> list = new ArrayList<String>();
		String item = readStringValue(parentElem, itemName+"0", stopValue);
		for (int n = 1; !item.equals(stopValue); n++) {
			list.add(item);
			item = readStringValue(parentElem, itemName+n, stopValue);
		}
		return list;
	}
	
	/**
	 * Reads a character array, represented as:
	 * <code><pre>
	 * &lt;name&gt;
	 *   &lt;itemName0 value="charValue"/&gt;
	 *   &lt;itemName1 value="charValue"/&gt;
	 *   &lt;itemName2 value="charValue"/&gt;
	 *   ...
	 *   &lt;itemNameX value="charValue"/&gt;
	 * &lt;/name&gt;
	 * </pre></code>
	 * 
	 * @param parent Parent element which contains the required property. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required property
	 * @param itemName Item name of the array
	 * @param stopValue Value, which is used to check end of array
	 * @param def Default value
	 * @return Read string array or default value (if read error is occupied)
	 */
	public java.util.ArrayList<Character> readCharacterArray(Element parent, String name, String itemName, Character stopValue, java.util.ArrayList<Character> def){
		Element parentElem = findElement(parent, name);
		if (parentElem == null) {
			return def;
		}
		ArrayList<Character> list = new ArrayList<Character>();
		Character item = readCharacterValue(parentElem, itemName+"0", stopValue);
		for (int n = 1; !item.equals(stopValue); n++) {
			list.add(item);
			item = readCharacterValue(parentElem, itemName+n, stopValue);
		}
		return list;
	}
	
	/**
	 * Adds property.
	 * 
	 * @param parent Parent element which contains this property. If <code>null</code> then parent will be a root element.
	 * @param name Name of this property
	 * @param value Value of this property
	 * @return The created element
	 */
	public Element addProperty(Element parent, String name, boolean value) {
		if (parent == null) {
			parent = root;
		}
		Element elem = doc.createElement(name);
		elem.setAttribute("value", String.valueOf(value));
		parent.appendChild(elem);
		return elem;
	}
	
	/**
	 * Adds property.
	 * 
	 * @param parent Parent element which contains this property. If <code>null</code> then parent will be a root element.
	 * @param name Name of this property
	 * @param value Value of this property
	 * @return The created element
	 */
	public Element addProperty(Element parent, String name, String value) {
		if (parent == null) {
			parent = root;
		}
		Element elem = doc.createElement(name);
		elem.setAttribute("value", value);
		parent.appendChild(elem);
		return elem;
	}
	
	/**
	 * Adds property.
	 * 
	 * @param parent Parent element which contains this property. If <code>null</code> then parent will be a root element.
	 * @param name Name of this property
	 * @param value Value of this property
	 * @return The created element
	 */
	public Element addProperty(Element parent, String name, int value) {
		if (parent == null) {
			parent = root;
		}
		Element elem = doc.createElement(name);
		elem.setAttribute("value", Integer.toString(value));
		parent.appendChild(elem);
		return elem;
	}
	
	/**
	 * Adds property.
	 * 
	 * @param parent Parent element which contains this property. If <code>null</code> then parent will be a root element.
	 * @param name Name of this property
	 * @param value Value of this property
	 * @return The created element
	 */
	public Element addProperty(Element parent, String name, Color value) {
		if (parent == null) {
			parent = root;
		}
		Element elem = doc.createElement(name);
		elem.setAttribute("b", Integer.toString(value.getBlue()));
		elem.setAttribute("g", Integer.toString(value.getGreen()));
		elem.setAttribute("r", Integer.toString(value.getRed()));
		parent.appendChild(elem);
		return elem;
	}
	
	/**
	 * Adds property.
	 * 
	 * @param parent Parent element which contains this property. If <code>null</code> then parent will be a root element.
	 * @param name Name of this property
	 * @param value Value of this property
	 * @return The created element
	 */
	public Element addProperty(Element parent, String name, Font value) {
		if (parent == null) {
			parent = root;
		}
		Element elem = doc.createElement(name);
		elem.setAttribute("bold", String.valueOf(value.isBold()));
		elem.setAttribute("italic", String.valueOf(value.isItalic()));
		elem.setAttribute("name", value.getName());
		elem.setAttribute("size", Integer.toString(value.getSize()));
		parent.appendChild(elem);
		return elem;
	}
	
	/**
	 * Adds array, represented as:
	 * <code><pre>
	 * &lt;name&gt;
	 *   &lt;itemName0 value="stringValue"/&gt;
	 *   &lt;itemName1 value="stringValue"/&gt;
	 *   &lt;itemName2 value="stringValue"/&gt;
	 *   ...
	 *   &lt;itemNameX value="stringValue"/&gt;
	 * &lt;/name&gt;
	 * </pre></code>
	 * 
	 * @param parent Parent element which contains this property. If <code>null</code> then parent will be a root element.
	 * @param name Name of this array
	 * @param itemName Item name of the array
	 * @param value Array to add
	 */
	public void addArrayProperty(Element parent, String name, String itemName, java.util.List<String> value) {
		if (value == null) {
			return;
		}
		if (parent == null) {
			parent = root;
		}
		Element elem = getElement(parent, name);
		parent.appendChild(elem);
		for (int i = 0; i < value.size(); i++) {
			Element itemElem = doc.createElement(itemName+i);
			itemElem.setAttribute("value", value.get(i));
			elem.appendChild(itemElem);
		}
	}
	
	/**
	 * Gets an element. If the required element is not found then it will be created.
	 * 
	 * @param parent Parent element which contains the required element. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required element
	 * @return The required element
	 */
	public Element getElement(Element parent, String name) {
		Element elem = findElement(parent, name);
		if (elem == null) {
			return addElement(parent, name);
		}
		return elem;
	}
	
	/**
	 * Searches an element.
	 * 
	 * @param parent Parent element which contains the required element. If <code>null</code> then parent will be a root element.
	 * @param name Name of the required element
	 * @return The found element or <code>null</code> if nothing was found
	 */
	public Element findElement(Element parent, String name) {
		if (parent == null) {
			parent = root;
		}
		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element)node;
				if (node.getNodeName().equals(name)) {
					elem = (Element)nodes.item(i);
					return elem;
				}
			}
		}
		return null;
	}
	
	/**
	 * Loads XML file.
	 * 
	 * @param file XML file
	 */
	public void loadXML(File file) {
		if (!file.exists()) {
			doc = createXML(file.getName());
			return;
		}
		doc = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(file.getPath());
			if (doc != null) {
				root = doc.getDocumentElement();
			}
		}
		catch (Exception e) {
		}
	}
	
	/**
	 * Writes XML file.
	 * 
	 * @param file XML file
	 */
	public void writeXML(File file) {
		PrintWriter out = null;
		try {
			File dirs = new File(file.getAbsoluteFile().getParent());
			dirs.mkdirs();
			out = new PrintWriter(new FileWriter(file.getPath()));
		}
		catch (Exception e) {
		}
		if (out == null) {
			return;
		}
		writeNode(doc, out, 0);
		out.close();
	}
	
	protected Document doc;
	protected Element root;
	
	/**
	 * Adds an element.
	 * 
	 * @param parent Parent element which contains the required element
	 * @param name Name of the required element
	 * @return The added element
	 */
	protected Element addElement(Element parent, String name) {
		Element elem = doc.createElement(name);
		if (parent != null) {
			parent.appendChild(elem);
		} else {
			root.appendChild(elem);
		}
		return elem;
	}
	
	/**
	 * Writes node.
	 * 
	 * @param node Node
	 * @param out Writer
	 * @param iteration The number of an iteration
	 */
	protected void writeNode(Node node, PrintWriter out, int iteration) {
		int type = node.getNodeType();
		switch (type) {
		case Node.DOCUMENT_NODE: 
			out.println("<?xml version=\"1.0\" encoding=\"Windows-1251\"?>");
			out.println("<!-- WARNING: Do not edit this file. -->");
			writeNode(((Document)node).getDocumentElement(), out, iteration);
			break;
			
		case Node.ELEMENT_NODE:
			for (int i = 0; i < iteration; i++) {
				out.print(" ");
			}
			out.print("<");
			out.print(node.getNodeName());
			NamedNodeMap attrs = node.getAttributes();
			for (int i = 0; i < attrs.getLength(); i++)
				writeNode(attrs.item(i), out, iteration);
			if (node.hasChildNodes()) {
				out.print(">\n");
				NodeList children = node.getChildNodes();
				for (int i = 0; i < children.getLength(); i++)
					writeNode(children.item(i), out, iteration+1);
				for (int i = 0; i < iteration; i++) {
					out.print(" ");
				}
				out.print("</");
				out.print(node.getNodeName());
				out.print(">\n");
			} else {
				out.print("/>\n");
			}
			break;
			
		case Node.ATTRIBUTE_NODE:
			out.print(" " + node.getNodeName() + "=\"" +
					((Attr)node).getValue() + "\"");
			break;
			
		case Node.TEXT_NODE: 
			out.print(node.getNodeValue());
			break;
		}
	}
	
	/**
	 * Creates a new XML document
	 * 
	 * @param rootName Root name of this XML document
	 * @return The created document
	 */
	protected Document createXML(String rootName) {
		Document doc = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.newDocument();
			if (doc != null) {
				root = doc.createElement(rootName);
				doc.appendChild(root);
			}
		}
		catch (Exception e) {
			return null;
		}
		return doc;
	}
	
}
