package com.tsarik.programs.photocenter.components;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

public abstract class AbstractDatabaseValuesPanel extends JPanel {
	
	public AbstractDatabaseValuesPanel() {
		this.setLayout(new GridBagLayout());
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1;
		constraints.insets = new Insets(1,1,1,1);
	}
	
	public abstract int getSign();
	
	public abstract Object getValue1();
	
	public abstract Object getValue2();
	
	protected GridBagConstraints constraints;
	
	protected void addComponentToGrid(Component c, int x, int y, int w, int h) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		((GridBagLayout)this.getLayout()).setConstraints(c, constraints);
		this.add(c);
	}
	
}
