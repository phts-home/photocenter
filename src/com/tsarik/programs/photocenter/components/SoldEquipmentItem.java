package com.tsarik.programs.photocenter.components;


public class SoldEquipmentItem {
	
	public SoldEquipmentItem(int id, String name, int quantity) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public String toString() {
		return name + " (" + quantity + ")";
	}
	
	private int id;
	private String name;
	private int quantity;
}
