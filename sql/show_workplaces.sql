SELECT 
	SUM(a.c) AS "Total Cashiers",  
	SUM(a.p) AS "Total Photographers", 
	SUM(a.m) AS "Total Managers", 
	SUM(a.ss) AS "Total Service Staff", 
	SUM(a.o) AS "Total Other", 
	SUM(a.t) AS "Total All" 

FROM (
	SELECT 
		1 AS i, 
		branches.branch_id AS bi, 
		branches.name AS bn, 
		branches.address AS ba, 
		workplaces.cashiers AS c, 
		workplaces.photographers AS p, 
		workplaces.managers AS m, 
		workplaces.service_staff AS ss, 
		workplaces.other AS o, 
		(workplaces.cashiers + workplaces.photographers + workplaces.managers + workplaces.service_staff + workplaces.other) AS t 
	FROM branches,workplaces 
	WHERE 
		workplaces.branch_id = branches.branch_id 
	) AS a

WHERE 1


GROUP BY a.i

/*
SELECT 
		branches.name AS "Branch Name", 
		branches.address AS "Branch Address",
		SUM(workplaces.cashiers) AS "Cashiers",  
		SUM(workplaces.photographers) AS "Photographers", 
		SUM(workplaces.managers) AS "Managers", 
		SUM(workplaces.service_staff) AS "Service Staff", 
		SUM(workplaces.other) AS "Other", 
		(workplaces.cashiers + workplaces.photographers + workplaces.managers + workplaces.service_staff + workplaces.other) AS "Total" 
	FROM branches,workplaces 
	WHERE 
		workplaces.branch_id = branches.branch_id 
	GROUP BY branches.branch_id 
*/