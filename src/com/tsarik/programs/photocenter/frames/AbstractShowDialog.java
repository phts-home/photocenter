package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.tsarik.dialogs.event.AbstractDialogEvent;
import com.tsarik.dialogs.event.AbstractDialogListener;
import com.tsarik.programs.photocenter.MainFrame;




public abstract class AbstractShowDialog extends AbstractPhotocenterProgramDialog {
	
	public AbstractShowDialog(MainFrame owner, String title, int w, int h) {
		super(owner, title, w, h, new String[]{"Go", "Close"});
		init();
	}
	
	protected JTextField tfTabTitle;
	
	@Override
	protected Component createComponentPanel() {
		Component panel = super.createComponentPanel();
		return panel;
	}

	private void init() {
		buttonPanel.add(new JLabel("Tab Title: "),0);
		tfTabTitle = new JTextField("");
		tfTabTitle.setPreferredSize(new Dimension(150, (int)tfTabTitle.getPreferredSize().getHeight()));
		buttonPanel.add(tfTabTitle,1);
		addAbstractDialogListener(new AbstractDialogListener(){
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				switch (evt.getButton()) {
				case 0:
					apply();
				case 1:
					AbstractShowDialog.this.dispose();
					break;
				}
			}
		});
	}
}
