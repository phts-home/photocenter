package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowBranchesDialog;


public class ShowBranchesDialogAction extends MainFrameMenuAction {
	
	public ShowBranchesDialogAction(MainFrame owner) {
		super(owner, "Branches/Kiosks...");
		putValue(SHORT_DESCRIPTION, "Show Branches/Kiosks");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_B);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowBranchesDialog d = new ShowBranchesDialog(owner);
		d.setVisible(true);
	}
}
