package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.QueryWindow;


public class SelectQueryAction extends MainFrameMenuAction {
	
	public SelectQueryAction(MainFrame owner) {
		super(owner, "Select Query");
		putValue(SHORT_DESCRIPTION, "Select Query");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		QueryWindow qw = new QueryWindow(owner);
		qw.setVisible(true);
	}
}
