SELECT 
	equipment.name AS "Equipment Name", 
	SUM(sold_equipment.quantity) AS "Quantity", 
	CONCAT(suppliers.name, " (", suppliers.address, ")") AS "Supplier", 
	deliverable_equipment.cost AS "Quotation"
	
FROM branches, equipment, sales, sold_equipment, suppliers, deliverable_equipment 

WHERE 
	sales.branch_id = branches.branch_id 
	AND sold_equipment.sale_id = sales.sale_id 
	AND sold_equipment.equipment_id = equipment.equipment_id 
	AND deliverable_equipment.equipment_id = equipment.equipment_id 
	AND deliverable_equipment.supplier_id = suppliers.supplier_id 
	
GROUP BY equipment.equipment_id, suppliers.supplier_id 

ORDER BY `Quantity` DESC,`Equipment Name`, `Quotation` 
