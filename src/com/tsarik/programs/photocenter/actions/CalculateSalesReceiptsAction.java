package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.CalculateSalesReceiptsDialog;


public class CalculateSalesReceiptsAction extends MainFrameMenuAction {
	
	public CalculateSalesReceiptsAction(MainFrame owner) {
		super(owner, "Sales Receipts...");
		putValue(SHORT_DESCRIPTION, "Calculate Sales Receipts...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		CalculateSalesReceiptsDialog d = new CalculateSalesReceiptsDialog(owner);
		d.setVisible(true);
	}
}
