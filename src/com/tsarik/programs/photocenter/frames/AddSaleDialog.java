package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.SoldEquipmentItem;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;



public class AddSaleDialog extends AbstractAddDialog {
	
	public AddSaleDialog(MainFrame owner) {
		super(owner, "Add Sale", 450, 300);
	}
	
	@Override
	protected void refresh() {
		cbBranches.removeAllItems();
		cbEquipment.removeAllItems();
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT equipment.equipment_id,equipment.name,equipment_types.name FROM equipment,equipment_types WHERE equipment.equipment_type_id=equipment_types.equipment_type_id ORDER BY equipment.equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addSale(
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					((SpinnerDateModel)spSaleDate.getModel()).getDate(),
					seList
					); 
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		seList = new ArrayList<SoldEquipmentItem>();
		
		cbEquipment = new JComboBox();
		cbBranches = new JComboBox();
		spQuantity = new JSpinner(new SpinnerNumberModel(1,0,999,1));
		spSaleDate = new JSpinner(new SpinnerDateModel());
		lsSoldEquipment = new JList(new DefaultListModel());
		
		JScrollPane listScroller = new JScrollPane(lsSoldEquipment);
		listScroller.setMinimumSize(new Dimension(260, 100));
		
		JButton btNow = new JButton("Now");
		btNow.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				spSaleDate.setValue(new Date());
			}
		});
		
		JButton btAddToList = new JButton("Store");
		btAddToList.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				seList.add(new SoldEquipmentItem(
						((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getId(),
						(String)((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getItem(),
						((SpinnerNumberModel)spQuantity.getModel()).getNumber().intValue())
				);
				((DefaultListModel)lsSoldEquipment.getModel()).clear();
				for (int i = 0; i < seList.size(); i++) {
					((DefaultListModel)lsSoldEquipment.getModel()).addElement(seList.get(i));
				}
			}
		});
		
		// put components on the panel
		addComponentToGrid(new JLabel("Branch"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 0, 2, 1, 2, panel);
		addComponentToGrid(new JLabel("Sale Date"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(spSaleDate, 1, 1, 2, 1, 2, panel);
		addComponentToGrid(btNow, 3, 1, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Equipment"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbEquipment, 1, 2, 1, 1, 2, panel);
		addComponentToGrid(spQuantity, 2, 2, 1, 1, 2, panel);
		addComponentToGrid(btAddToList, 3, 2, 1, 1, 3, panel);
		addComponentToGrid(new JLabel("Sold Equipment"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(listScroller, 1, 3, 2, 2, 5, panel);
		
		return panel;
	}
	
	private JComboBox cbBranches;
	private JComboBox cbEquipment;
	private JSpinner spQuantity;
	private JSpinner spSaleDate;
	private JList lsSoldEquipment;
	
	private ArrayList<SoldEquipmentItem> seList;
	
}
