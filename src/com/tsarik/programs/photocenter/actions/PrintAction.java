package com.tsarik.programs.photocenter.actions;


import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;

public class PrintAction extends MainFrameMenuAction {
	
	public PrintAction(MainFrame owner) {
		super(owner, "Print...");
		putValue(SHORT_DESCRIPTION, "Print");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
		setEnabled(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.print();
	}
}
