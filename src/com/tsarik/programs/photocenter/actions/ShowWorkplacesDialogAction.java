package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowWorkplacesDialog;


public class ShowWorkplacesDialogAction extends MainFrameMenuAction {
	
	public ShowWorkplacesDialogAction(MainFrame owner) {
		super(owner, "Workplaces...");
		putValue(SHORT_DESCRIPTION, "Show Workplaces");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_W);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowWorkplacesDialog d = new ShowWorkplacesDialog(owner);
		d.setVisible(true);
	}
}
