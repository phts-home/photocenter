package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;



public class AddFramesDialog extends AbstractAddDialog {
	
	public AddFramesDialog(MainFrame owner) {
		super(owner, "Add Frames", 450, 450);
	}
	
	public AddFramesDialog(AbstractDialog owner) {
		super(owner, "Add Frames", 450, 450);
	}
	
	@Override
	protected void refresh() {
		// Nothing
	}
	
	@Override
	protected int apply() {
		try {
			int[] nums = new int[36];
			for (int i = 0; i < 36; i++) {
				nums[i] = ((SpinnerNumberModel)spFrames[i].getModel()).getNumber().intValue();
			}
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addFrames(nums);
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		spFrames = new JSpinner[36];
		for (int i = 0; i < 36; i++) {
			spFrames[i] = new JSpinner(new SpinnerNumberModel(0,0,999,1));
		}
		JLabel[] lbLabels = new JLabel[36];
		for (int i = 0; i < 36; i++) {
			lbLabels[i] = new JLabel("Frame #"+(i+1)+":");
		}
		
		// put components on the panel
		for (int i = 0; i < 12; i++) {
			addComponentToGrid(lbLabels[i], 0, i, 1, 1, 1, panel);
			addComponentToGrid(spFrames[i], 1, i, 1, 1, 2, panel);
		}
		for (int i = 12; i < 24; i++) {
			addComponentToGrid(lbLabels[i], 2, i-12, 1, 1, 1, panel);
			addComponentToGrid(spFrames[i], 3, i-12, 1, 1, 2, panel);
		}
		for (int i = 24; i < 36; i++) {
			addComponentToGrid(lbLabels[i], 4, i-24, 1, 1, 1, panel);
			addComponentToGrid(spFrames[i], 5, i-24, 1, 1, 4, panel);
		}
		
		return panel;
	}
	
	private JSpinner[] spFrames;
	
}
