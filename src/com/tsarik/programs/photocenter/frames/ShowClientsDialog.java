package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.SQLException;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.*;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class ShowClientsDialog extends AbstractShowDialog {
	
	public ShowClientsDialog(MainFrame owner) {
		super(owner, "Show Clients", 400, 430);
		tfTabTitle.setText("Clients");
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbClients.addItem(new ComboBoxDatabaseItem());
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.showClients(
					cbAll.isSelected(),
					((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(),
					(cbClientFirstnameEnabled.isSelected())?tfClientFirstname.getText():"",
					(cbClientLastnameEnabled.isSelected())?tfClientLastname.getText():"",
					(cbClientAddressEnabled.isSelected())?tfClientAddr.getText():"",
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					(cbBranchNameEnabled.isSelected())?tfBranchName.getText():"",
					(cbBranchAddrEnabled.isSelected())?tfBranchAddr.getText():"",
					dpPhotosNumber.getSign(),
					dpPhotosNumber.getValue1().intValue(),
					dpPhotosNumber.getValue2().intValue(),
					cbDiscount.getSelectedIndex()-1,
					cbProfessional.getSelectedIndex()-1
					);
			if (table != null) {
				owner.addClientsTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		tfClientFirstname = new JTextField();
		tfClientLastname = new JTextField();
		tfClientAddr = new JTextField();
		cbBranches = new JComboBox();
		tfBranchName = new JTextField();
		tfBranchAddr = new JTextField();
		dpPhotosNumber = new DatabaseIntValuesPanel();
		cbAll = new JCheckBox("All");
		
		cbClientFirstnameEnabled = new JCheckBox("First Name");
		cbClientLastnameEnabled = new JCheckBox("Last Name");
		cbClientAddressEnabled = new JCheckBox("Address");
		
		cbBranchNameEnabled = new JCheckBox("Branch Name");
		cbBranchAddrEnabled = new JCheckBox("Branch Address");
		
		cbDiscount = new JComboBox();
		cbDiscount.addItem("");
		cbDiscount.addItem("Yes");
		cbDiscount.addItem("No");
		cbProfessional = new JComboBox();
		cbProfessional.addItem("");
		cbProfessional.addItem("Amateur");
		cbProfessional.addItem("Professional");

		constraints.weightx = 1;
		constraints.insets = new Insets(1,1,1,1);
		JPanel clientPanel = new JPanel();
		clientPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbClientFirstnameEnabled, 0, 0, 1, 1, clientPanel);
		addComponentToGrid(tfClientFirstname, 1, 0, 1, 1, clientPanel);
		addComponentToGrid(cbClientLastnameEnabled, 0, 1, 1, 1, clientPanel);
		addComponentToGrid(tfClientLastname, 1, 1, 1, 1, clientPanel);
		addComponentToGrid(cbClientAddressEnabled, 0, 2, 1, 1, clientPanel);
		addComponentToGrid(tfClientAddr, 1, 2, 1, 1, clientPanel);
		
		JPanel branchPanel = new JPanel();
		branchPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbBranchNameEnabled, 0, 0, 1, 1, branchPanel);
		addComponentToGrid(tfBranchName, 1, 0, 1, 1, branchPanel);
		addComponentToGrid(cbBranchAddrEnabled, 0, 1, 1, 1, branchPanel);
		addComponentToGrid(tfBranchAddr, 1, 1, 1, 1, branchPanel);
		
		// put components on the panel
		addComponentToGrid(cbAll, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Client"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(cbClients, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(clientPanel, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Branch"), 0, 3, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 3, 1, 1, 4, panel);
		addComponentToGrid(branchPanel, 1, 4, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Photos"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(dpPhotosNumber, 1, 5, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Discount"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(cbDiscount, 1, 6, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Skill"), 0, 7, 1, 1, 1, panel);
		addComponentToGrid(cbProfessional, 1, 7, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbClients;
	private JTextField tfClientFirstname;
	private JTextField tfClientLastname;
	private JTextField tfClientAddr;
	private JCheckBox cbClientFirstnameEnabled;
	private JCheckBox cbClientLastnameEnabled;
	private JCheckBox cbClientAddressEnabled;
	private JComboBox cbBranches;
	private JTextField tfBranchName;
	private JTextField tfBranchAddr;
	private JCheckBox cbBranchNameEnabled;
	private JCheckBox cbBranchAddrEnabled;
	private DatabaseIntValuesPanel dpPhotosNumber;
	private JComboBox cbDiscount;
	private JComboBox cbProfessional;
	private JCheckBox cbAll;
	
}
