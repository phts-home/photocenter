SELECT DISTINCT 
	suppliers.name AS "Supplier's Name", 
	suppliers.address AS "Supplier's Address", 
	a.s AS "Total Quantity", 
	a.t AS "Equipment Type" 
	
FROM 
	suppliers, 
	(
		SELECT suppliers.supplier_id AS i, 
			SUM(supplies.quantity) AS s, 
			equipment_types.name AS t
		FROM deliverable_equipment, supplies, suppliers, equipment, equipment_types 
		WHERE deliverable_equipment.equipment_id = equipment.equipment_id 
			AND deliverable_equipment.supplier_id = suppliers.supplier_id 
			AND supplies.deliverable_equipment_id = deliverable_equipment.deliverable_equipment_id 
			AND supplies.supply_date BETWEEN "2009-11-01 15:30:11" AND "2009-11-07 15:30:11" 
			AND equipment_types.equipment_type_id = equipment.equipment_type_id
		GROUP BY suppliers.supplier_id, equipment.equipment_type_id
	) AS a 

WHERE a.i = suppliers.supplier_id

ORDER BY `Supplier's Name` 
