package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.AddBranchDialog;


public class AddBranchAction extends MainFrameMenuAction {
	
	public AddBranchAction(MainFrame owner) {
		super(owner, "Branch...");
		putValue(SHORT_DESCRIPTION, "Add Branch...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_B);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		AddBranchDialog d = new AddBranchDialog(owner);
		d.setVisible(true);
	}
}
