package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.sql.SQLException;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.tsarik.dialogs.AbstractDialog;
import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;



public class AddClientDialog extends AbstractAddDialog {
	
	public AddClientDialog(MainFrame owner) {
		super(owner, "Add Client", 450, 250);
	}
	
	public AddClientDialog(AbstractDialog owner) {
		super(owner, "Add Client", 450, 250);
	}
	
	@Override
	protected void refresh() {
		// Nothing
	}
	
	@Override
	protected int apply() {
		if ( (tfFirstname.getText().equals("")) || (tfLastname.getText().equals("")) || (tfAddress.getText().equals("")) ) {
			JOptionPane.showMessageDialog(owner, Parameters.ERROR_INPUT, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
			return APPLY_CHECK_FAIL;
		}
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			dbManager.addClient(tfFirstname.getText(), tfLastname.getText(), tfAddress.getText(), cbProfessional.isSelected());
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		tfLastname = new JTextField();
		tfFirstname = new JTextField();
		tfAddress = new JTextField();
		cbProfessional = new JCheckBox("Professional");
		
		// put components on the panel
		addComponentToGrid(new JLabel("Last Name"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfLastname, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("First Name"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfFirstname, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Address"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(tfAddress, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(cbProfessional, 1, 3, 1, 1, 4, panel);
		
		return panel;
	}
	
	private JTextField tfLastname;
	private JTextField tfFirstname;
	private JTextField tfAddress;
	private JCheckBox cbProfessional;
	
	
}
