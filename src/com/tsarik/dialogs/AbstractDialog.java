package com.tsarik.dialogs;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import com.tsarik.dialogs.event.AbstractDialogEvent;
import com.tsarik.dialogs.event.AbstractDialogListener;



/**
 * Abstract dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.3
 *
 */
public abstract class AbstractDialog extends JDialog {
	
	/**
	 * Constructs a dialog with "OK","Cancel" and "Apply" buttons.
	 * 
	 * @param owner The dialog's owner as Frame object
	 * @param modal Specifies whether dialog blocks user input to other top-level windows when shown
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractDialog(Frame owner, boolean modal, String title, Image imageIcon, int w, int h) {
		this(owner, modal, title, imageIcon, w, h, true, true, true, "OK", "Cancel", "Apply");
	}
	
	/**
	 * Constructs a dialog with "OK","Cancel" and "Apply" buttons.
	 * 
	 * @param owner The dialog's owner as Dialog object
	 * @param modal Specifies whether dialog blocks user input to other top-level windows when shown
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractDialog(Dialog owner, boolean modal, String title, Image imageIcon, int w, int h) {
		this(owner, modal, title, imageIcon, w, h, true, true, true, "OK", "Cancel", "Apply");
	}
	
	/**
	 * Constructs a dialog with "OK","Cancel" and/or "Apply" buttons.
	 * 
	 * @param owner The dialog's owner as Frame object
	 * @param modal Specifies whether dialog blocks user input to other top-level windows when shown
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 * @param okButton OK button visible
	 * @param cancelButton Cancel button visible
	 * @param applyButton Apply button visible
	 * @param okButtonTitle OK button title
	 * @param cancelButtonTitle Cancel button title
	 * @param applyButtonTitle Apply button title
	 */
	public AbstractDialog(Frame owner, boolean modal, String title, Image imageIcon, int w, int h, boolean okButton, boolean cancelButton, boolean applyButton, String okButtonTitle, String cancelButtonTitle, String applyButtonTitle) {
		super(owner, modal);
		abstractDialogListenerList = new EventListenerList();
		// get screen size
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension scrSize = kit.getScreenSize();
		// set window size
		setSize(w, h);
		// set window position in the center of the screen
		setLocation(scrSize.width/2 - w/2, scrSize.height/2 - h/2);
		// set icon
		setIconImage(imageIcon);
		// set window title
		setTitle(title);
		// set resizable
		setResizable(false);
		// create components
		buttonPanel = createDefaultButtonPanel(okButtonTitle, cancelButtonTitle, applyButtonTitle, okButton, cancelButton, applyButton);
		createComponents();
	}
	
	/**
	 * Constructs a dialog with "OK","Cancel" and/or "Apply" buttons.
	 * 
	 * @param owner The dialog's owner as Dialog object
	 * @param modal Specifies whether dialog blocks user input to other top-level windows when shown
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 * @param okButton OK button visible
	 * @param cancelButton Cancel button visible
	 * @param applyButton Apply button visible
	 * @param okButtonTitle OK button title
	 * @param cancelButtonTitle Cancel button title
	 * @param applyButtonTitle Apply button title
	 */
	public AbstractDialog(Dialog owner, boolean modal, String title, Image imageIcon, int w, int h, boolean okButton, boolean cancelButton, boolean applyButton, String okButtonTitle, String cancelButtonTitle, String applyButtonTitle) {
		super(owner, modal);
		abstractDialogListenerList = new EventListenerList();
		// get screen size
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension scrSize = kit.getScreenSize();
		// set window size
		setSize(w, h);
		// set window position in the center of the screen
		setLocation(scrSize.width/2 - w/2, scrSize.height/2 - h/2);
		// set icon
		setIconImage(imageIcon);
		// set window title
		setTitle(title);
		// set resizable
		setResizable(false);
		// create components
		buttonPanel = createDefaultButtonPanel(okButtonTitle, cancelButtonTitle, applyButtonTitle, okButton, cancelButton, applyButton);
		createComponents();
	}
	
	/**
	 * Constructs a dialog with custom buttons.
	 * 
	 * @param owner The dialog's owner as Frame object
	 * @param modal Specifies whether dialog blocks user input to other top-level windows when shown
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 * @param buttonNames An array with dialog button names 
	 */
	public AbstractDialog(Frame owner, boolean modal, String title, Image imageIcon, int w, int h, String[] buttonNames) {
		super(owner, modal);
		abstractDialogListenerList = new EventListenerList();
		// get screen size
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension scrSize = kit.getScreenSize();
		// set window size
		setSize(w, h);
		// set window position in the center of the screen
		setLocation(scrSize.width/2 - w/2, scrSize.height/2 - h/2);
		// set icon
		setIconImage(imageIcon);
		// set window title
		setTitle(title);
		// set resizable
		setResizable(false);
		// create components
		buttonPanel = createCustomButtonPanel(buttonNames);
		createComponents();
	}
	
	/**
	 * Constructs a dialog with custom buttons.
	 * 
	 * @param owner The dialog's owner as Dialog object
	 * @param modal Specifies whether dialog blocks user input to other top-level windows when shown
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 * @param buttonNames An array with dialog button names 
	 */
	public AbstractDialog(Dialog owner, boolean modal, String title, Image imageIcon, int w, int h, String[] buttonNames) {
		super(owner, true);
		abstractDialogListenerList = new EventListenerList();
		// get screen size
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension scrSize = kit.getScreenSize();
		// set window size
		setSize(w, h);
		// set window position in the center of the screen
		setLocation(scrSize.width/2 - w/2, scrSize.height/2 - h/2);
		// set icon
		setIconImage(imageIcon);
		// set window title
		setTitle(title);
		// set resizable
		setResizable(false);
		// create components
		buttonPanel = createCustomButtonPanel(buttonNames);
		createComponents();
	}
	
	@Override
	public void show() {
		refresh();
		super.show();
	}
	
	
	protected EventListenerList abstractDialogListenerList;
	
	protected JPanel buttonPanel;
	protected Component componentPanel;
	
	
	/**
	 * Adds the given observer to begin receiving notifications when dialog's buttons were pressed.
	 * 
	 * @param l The observer to register
	 */
	protected void addAbstractDialogListener(AbstractDialogListener l) {
		abstractDialogListenerList.add(AbstractDialogListener.class, l);
	}
	
	/**
	 * Unregisters the given observer so it will no longer receive events.
	 * 
	 * @param l The observer to unregister
	 */
	protected void removeAbstractDialogListener(AbstractDialogListener l) {
		abstractDialogListenerList.remove(AbstractDialogListener.class, l);
	}
	
	/**
	 * Gets all listeners that have registered.
	 * 
	 * @return all listeners that have registered
	 */
	protected AbstractDialogListener[] getAbstractDialogListeners() {
		return abstractDialogListenerList.getListeners(AbstractDialogListener.class);
	}
	
	/**
	 * Refreshes components on opening the dialog.
	 */
	protected abstract void refresh();
	
	/**
	 * Creates components on the frame.
	 */
	protected void createComponents() {
		// create components panel
		componentPanel = createComponentPanel();
		
		// place components on frame
		Container contentPane = getContentPane();
		contentPane.add(componentPanel, BorderLayout.CENTER);
		contentPane.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Creates button panel.
	 * 
	 * @return JPanel
	 */
	protected JPanel createDefaultButtonPanel(String okButtonTitle, String cancelButtonTitle, String applyButtonTitle, boolean okButton, boolean cancelButton, boolean applyButton) {
		JPanel buttonsPanel = new JPanel();
		
		if (okButton) {
			JButton butOK = new JButton(okButtonTitle);
			butOK.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt) {
					fireButtonPressed(AbstractDialog.this, AbstractDialogEvent.OK_BUTTON);
					AbstractDialog.this.dispose();
				}
			});
			buttonsPanel.add(butOK);
		}
		
		if (cancelButton) {
			JButton butCancel = new JButton(cancelButtonTitle);
			butCancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt) {
					fireButtonPressed(AbstractDialog.this, AbstractDialogEvent.CANCEL_BUTTON);
					AbstractDialog.this.dispose();
				}
			});
			buttonsPanel.add(butCancel);
		}
		
		if (applyButton) {
			JButton butApply = new JButton(applyButtonTitle);
			butApply.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt) {
					fireButtonPressed(AbstractDialog.this, AbstractDialogEvent.APPLY_BUTTON);
				}
			});
			buttonsPanel.add(butApply);
		}
		
		return buttonsPanel;
	}
	
	/**
	 * Creates custom button panel.
	 * 
	 * @return JPanel
	 */
	protected JPanel createCustomButtonPanel(String[] buttonNames) {
		JPanel buttonsPanel = new JPanel();
		for (int i = 0; i < buttonNames.length; i++) {
			JButton button = new JButton(buttonNames[i]);
			button.addActionListener(new AbstractDialogButtonActionListener(i));
			buttonsPanel.add(button);
		}
		
		return buttonsPanel;
	}
	
	/**
	 * Creates component panel.
	 * 
	 * @return Component
	 */
	protected abstract Component createComponentPanel();
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 */
	protected void fireButtonPressed(AbstractDialog dialog, int buttonIndex) {
		// guaranteed to return a non-null array
		Object[] listeners = abstractDialogListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof AbstractDialogListener) {
				((AbstractDialogListener)i).buttonPressed(new AbstractDialogEvent(dialog, buttonIndex));
			}
		}
	}
	
	
	
	private class AbstractDialogButtonActionListener implements ActionListener {
		public AbstractDialogButtonActionListener(int buttonIndex) {
			this.buttonIndex = buttonIndex;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			fireButtonPressed(AbstractDialog.this, buttonIndex);
		}
		private int buttonIndex;
	}
	
}
