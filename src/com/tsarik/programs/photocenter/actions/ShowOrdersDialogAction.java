package com.tsarik.programs.photocenter.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.frames.ShowOrdersDialog;


public class ShowOrdersDialogAction extends MainFrameMenuAction {
	
	public ShowOrdersDialogAction(MainFrame owner) {
		super(owner, "Orders...");
		putValue(SHORT_DESCRIPTION, "Show Orders");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK + InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ShowOrdersDialog d = new ShowOrdersDialog(owner);
		d.setVisible(true);
	}
}
