package com.tsarik.programs.photocenter.frames;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.tsarik.programs.photocenter.MainFrame;
import com.tsarik.programs.photocenter.Parameters;
import com.tsarik.programs.photocenter.components.ComboBoxDatabaseItem;
import com.tsarik.programs.photocenter.components.DatabaseDatePanel;
import com.tsarik.programs.photocenter.dbmanagement.PhotocenterDBManager;
import com.tsarik.programs.photocenter.dbmanagement.ResponseTable;




public class ShowServicesDialog extends AbstractShowDialog {
	
	public ShowServicesDialog(MainFrame owner) {
		super(owner, "Show Services", 400, 550);
		tfTabTitle.setText("Services");
	}
	
	@Override
	protected void refresh() {
		cbClients.removeAllItems();
		cbClients.addItem(new ComboBoxDatabaseItem());
		cbBranches.removeAllItems();
		cbBranches.addItem(new ComboBoxDatabaseItem());
		cbServiceTypes.removeAllItems();
		cbServiceTypes.addItem(new ComboBoxDatabaseItem());
		cbEquipment.removeAllItems();
		cbEquipment.addItem(new ComboBoxDatabaseItem());
		
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.queryToTable("SELECT client_id,firstname,lastname,address FROM clients");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbClients.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " " + table.getItem(i,2) + " (" + table.getItem(i,3) + ")"));
			}
			table = dbManager.queryToTable("SELECT branch_id,name,address FROM branches");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbBranches.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT service_type_id,name,cost FROM service_types");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbServiceTypes.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1) + " (" + table.getItem(i,2) + ")"));
			}
			table = dbManager.queryToTable("SELECT equipment.equipment_id,equipment.name FROM equipment,equipment_types WHERE equipment.equipment_type_id=equipment_types.equipment_type_id AND equipment_types.for_sale=1 ORDER BY equipment.equipment_id");
			for (int i = 0; i < table.getRecordCount(); i++) {
				cbEquipment.addItem(new ComboBoxDatabaseItem(((Integer)table.getItem(i,0)).intValue(), table.getItem(i,1)));
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
	}
	
	@Override
	protected int apply() {
		try {
			PhotocenterDBManager dbManager = PhotocenterDBManager.getInstance();
			ResponseTable table = dbManager.showServices(((ComboBoxDatabaseItem)cbClients.getSelectedItem()).getId(),
					(cbClientFirstnameEnabled.isSelected())?tfClientFirstname.getText():"",
					(cbClientLastnameEnabled.isSelected())?tfClientLastname.getText():"",
					(cbClientAddressEnabled.isSelected())?tfClientAddr.getText():"",
					((ComboBoxDatabaseItem)cbBranches.getSelectedItem()).getId(),
					(cbBranchNameEnabled.isSelected())?tfBranchName.getText():"",
					(cbBranchAddrEnabled.isSelected())?tfBranchAddr.getText():"",
					((ComboBoxDatabaseItem)cbServiceTypes.getSelectedItem()).getId(),
					((ComboBoxDatabaseItem)cbEquipment.getSelectedItem()).getId(),
					dpServiceDatePanel.getSign(),
					dpServiceDatePanel.getValue1(),
					dpServiceDatePanel.getValue2(),
					dpExpDatePanel.getSign(),
					dpExpDatePanel.getValue1(),
					dpExpDatePanel.getValue2(),
					cbIsDone.getSelectedIndex()-1, 
					cbIsPaid.getSelectedIndex()-1);
			if (table != null) {
				owner.addServicesTableTab(tfTabTitle.getText(), table);
			}
		}
		catch (SQLException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CONNECTION, e.getMessage());
		}
		catch (ClassNotFoundException e) {
			Parameters.showErrorMessage(owner, Parameters.ERROR_CLASSNOTFOUND, e.getMessage());
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		cbClients = new JComboBox();
		tfClientFirstname = new JTextField();
		tfClientLastname = new JTextField();
		tfClientAddr = new JTextField();
		cbBranches = new JComboBox();
		tfBranchName = new JTextField();
		tfBranchAddr = new JTextField();
		cbIsDone = new JComboBox(new String[]{"","Not Done","Done"});
		cbIsPaid = new JComboBox(new String[]{"","Not Paid","Paid"});
		cbServiceTypes = new JComboBox();
		cbEquipment = new JComboBox();
		dpServiceDatePanel = new DatabaseDatePanel();
		dpExpDatePanel = new DatabaseDatePanel();
		cbClientFirstnameEnabled = new JCheckBox("First Name");
		cbClientLastnameEnabled = new JCheckBox("Last Name");
		cbClientAddressEnabled = new JCheckBox("Address");
		
		cbBranchNameEnabled = new JCheckBox("Branch Name");
		cbBranchAddrEnabled = new JCheckBox("Branch Address");
		
		constraints.weightx = 1;
		constraints.insets = new Insets(1,1,1,1);
		JPanel clientPanel = new JPanel();
		clientPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbClientFirstnameEnabled, 0, 0, 1, 1, clientPanel);
		addComponentToGrid(tfClientFirstname, 1, 0, 1, 1, clientPanel);
		addComponentToGrid(cbClientLastnameEnabled, 0, 1, 1, 1, clientPanel);
		addComponentToGrid(tfClientLastname, 1, 1, 1, 1, clientPanel);
		addComponentToGrid(cbClientAddressEnabled, 0, 2, 1, 1, clientPanel);
		addComponentToGrid(tfClientAddr, 1, 2, 1, 1, clientPanel);
		
		JPanel branchPanel = new JPanel();
		branchPanel.setLayout(new GridBagLayout());
		addComponentToGrid(cbBranchNameEnabled, 0, 0, 1, 1, branchPanel);
		addComponentToGrid(tfBranchName, 1, 0, 1, 1, branchPanel);
		addComponentToGrid(cbBranchAddrEnabled, 0, 1, 1, 1, branchPanel);
		addComponentToGrid(tfBranchAddr, 1, 1, 1, 1, branchPanel);
		
		// put components on the panel
		addComponentToGrid(new JLabel("Client"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(cbClients, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(clientPanel, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Branch"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(cbBranches, 1, 2, 1, 1, 4, panel);
		addComponentToGrid(branchPanel, 1, 3, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Service Type"), 0, 4, 1, 1, 1, panel);
		addComponentToGrid(cbServiceTypes, 1, 4, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Equipment"), 0, 5, 1, 1, 1, panel);
		addComponentToGrid(cbEquipment, 1, 5, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Booking Date"), 0, 6, 1, 1, 1, panel);
		addComponentToGrid(dpServiceDatePanel, 1, 6, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Expiration Date"), 0, 8, 1, 1, 1, panel);
		addComponentToGrid(dpExpDatePanel, 1, 8, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Is Done"), 0, 10, 1, 1, 1, panel);
		addComponentToGrid(cbIsDone, 1, 10, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Is Paid"), 0, 11, 1, 1, 1, panel);
		addComponentToGrid(cbIsPaid, 1, 11, 1, 1, 4, panel);
		return panel;
	}
	
	private JComboBox cbClients;
	private JTextField tfClientFirstname;
	private JTextField tfClientLastname;
	private JTextField tfClientAddr;
	private JCheckBox cbClientFirstnameEnabled;
	private JCheckBox cbClientLastnameEnabled;
	private JCheckBox cbClientAddressEnabled;
	private JComboBox cbBranches;
	private JTextField tfBranchName;
	private JTextField tfBranchAddr;
	private JCheckBox cbBranchNameEnabled;
	private JCheckBox cbBranchAddrEnabled;
	private JComboBox cbServiceTypes;
	private JComboBox cbEquipment;
	private DatabaseDatePanel dpServiceDatePanel;
	private DatabaseDatePanel dpExpDatePanel;
	private JComboBox cbIsDone;
	private JComboBox cbIsPaid;
	
}
